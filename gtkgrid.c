/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public License
 * Version 1.0 (the "NPL"); you may not use this file except in
 * compliance with the NPL.  You may obtain a copy of the NPL at
 * http://www.mozilla.org/NPL/
 *
 * Software distributed under the NPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the NPL
 * for the specific language governing rights and limitations under the
 * NPL.
 *
 * The Initial Developer of this code under the NPL is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation.  All Rights
 * Reserved.
 */

/* 
   gtkgrid.c --- gtk fe handling of Grid windows
                  (FRAMESET's and FRAME).
*/

#include "structs.h"
#include "ntypes.h"
#include "xpassert.h"
#include "proto.h"
#include "fe_proto.h"

void
FE_LoadGridCellFromHistory(MWContext *context,
			   void *hist,
			   NET_ReloadMethod force_reload)
{
   XP_ASSERT(0);
}

void*
FE_FreeGridWindow(MWContext *context,
		  XP_Bool save_history)
{
   XP_ASSERT(0);
   return 0;
}

void
FE_RestructureGridWindow(MWContext *context,
			 int32 x,
			 int32 y,
			 int32 width,
			 int32 height)
{
   XP_ASSERT(0);
}

/* Template--
MWContext *
FE_MakeGridWindow(MWContext *old_context,
		  void *hist_list,
		  void *history,
		  int32 x,
		  int32 y,
		  int32 width,
		  int32 height,
		  char *url_str,
		  char *window_name,
		  int8 scrolling,
		  NET_ReloadMethod force_reload,
		  Bool no_edge)
{
*/

/*
This is a FIXME!!  This make our main window as far as i can tell.
This is far from complete. We need some one that know the gtk widgets
better than i, to do the rest. [MW]

It might be the window where the page is rendered, but the window
containing the menus, toolbars, and this window etc. is made elsewhere. [VH]
*/

MWContext *
#ifdef XP_UNIX
FE_MakeGridWindow (MWContext *old_context, void *hist_list, void *history,
        int32 x, int32 y,
#else
FE_MakeGridWindow (MWContext *old_context, void *history, int32 x, int32 y,
#endif /* XP_UNIX */
        int32 width, int32 height, char *url_str, char *window_name,
        int8 scrolling, NET_ReloadMethod force_reload, Bool no_edge)
{
   MWContext *context = GTKFE_CreateMWContext();

   XP_ASSERT(0); /* Want to know if/when we get here... [VH] */
    
#if CTXBUILT
  GtkWidget parent = CONTEXT_DATA (old_context)->drawing_area;
  
  struct fe_MWContext_cons *cons = (struct fe_MWContext_cons *)
    GTKFE_ALLOC (sizeof (struct fe_MWContext_cons));

  fe_ContextData *fec 
    = (fe_ContextData *) calloc (sizeof (GTKFE_ContextData), 1);
  History_entry *he = (History_entry *)history;
  URL_Struct *url = NULL;

  CONTEXT_DATA (context) = fec;

  /* add the layout function pointers
   */
  context->funcs = GTKFE_BuildDisplayFunctionTable();
  context->convertPixX = context->convertPixY = 1;
  context->is_grid_cell = TRUE;
  context->grid_parent = old_context;

  /* New field added by putterman for increase/decrease font */
  context->fontScalingPercentage = old_context->fontScalingPercentage;

  cons->context = context;
  cons->next = GTKFE_all_MWContexts;
  GTKFE_all_MWContexts = cons;

  /* pixelsPerPoint:  display-specific information needed by the back end
   * when converting style sheet length units between points and pixels.
   */
  context->XpixelsPerPoint = old_context->XpixelsPerPoint;
  context->YpixelsPerPoint = old_context->YpixelsPerPoint;

  SHIST_InitSession (context);          /* Initialize the history library. */
#ifdef XP_UNIX
  if (hist_list != NULL)
  {
    context->hist.list_ptr = hist_list;
  }
  else
  {
    SHIST_AddDocument(context, he);
  }
#else
  SHIST_AddDocument(context, he);
#endif /* XP_UNIX */

  if (he)
    url = SHIST_CreateURLStructFromHistoryEntry (context, he);
  else
    url = NET_CreateURLStruct (url_str, NET_DONT_RELOAD);

  if (url) {
    MWContext *top = XP_GetNonGridContext(old_context);
    History_entry *h = SHIST_GetCurrent (&top->hist);

    url->force_reload = force_reload;

    /* Set the referer field in the url to the document that refered to the
     * grid parent.  New function fe_GetURLForReferral() might be used
     * here, brendan says this is probably Ok. -mcafee
     */
    if (h && h->referer)
      url->referer = strdup(h->referer);
  }

  if (window_name)
    {
      context->name = strdup (window_name);
    }
  XP_AddContextToList (context);
  if (old_context)
    {
      CONTEXT_DATA (context)->autoload_images_p =
        CONTEXT_DATA (old_context)->autoload_images_p;
      CONTEXT_DATA (context)->loading_images_p = False;
      CONTEXT_DATA (context)->looping_images_p = False;
      CONTEXT_DATA (context)->delayed_images_p =
        CONTEXT_DATA (old_context)->delayed_images_p;
      CONTEXT_DATA (context)->force_load_images = 0;
      CONTEXT_DATA (context)->fancy_ftp_p =
        CONTEXT_DATA (old_context)->fancy_ftp_p;
      CONTEXT_DATA (context)->xfe_doc_csid =
        CONTEXT_DATA (old_context)->xfe_doc_csid;
    }
  CONTEXT_WIDGET (context) = CONTEXT_WIDGET (old_context);
  CONTEXT_DATA (context)->backdrop_pixmap = (Pixmap) ~0;
  CONTEXT_DATA (context)->grid_scrolling = scrolling;
    
  /* FRAMES_HAVE_THEIR_OWN_COLORMAP was an unfinished
         experiment by kevina. */
#ifdef FRAMES_HAVE_THEIR_OWN_COLORMAP
  /* We have to go through this to get the toplevel widget */
  { 
      MWContext *top_context = XP_GetNonGridContext(context);
    
      /* We need some way to pick out color map.
      fe_pick_visual_and_colormap (XtParent(CONTEXT_WIDGET (top_context)),
                                   context);
      */
  } 
#else
  /* Inherit colormap from our parent */
  CONTEXT_DATA(context)->colormap = CONTEXT_DATA(old_context)->colormap;
#endif
    
#ifdef FRAMES_HAVE_THEIR_OWN_COLORMAP
        /* XXXM12N Create and initialize the Image Library JMC callback
           interface.  Also create a new IL_GroupContext for this window.*/
        if (!fe_init_image_callbacks(context))
            {
                return NULL;
            }
  fe_InitColormap(context);
#endif

  XtGetApplicationResources (CONTEXT_WIDGET (old_context),
                             (XtPointer) CONTEXT_DATA (context),
                             fe_Resources, fe_ResourcesSize,
                             0, 0);
    
/* CONTEXT_DATA (context)->main_pane = parent; */
    
  /*
   * set the default coloring correctly into the new context.
   */
  { 
    Pixel unused_select_pixel;
    XmGetColors (XtScreen (parent),
                 fe_cmap(context),
                 CONTEXT_DATA (context)->default_bg_pixel,
                 &(CONTEXT_DATA (context)->fg_pixel),
                 &(CONTEXT_DATA (context)->top_shadow_pixel),
                 &(CONTEXT_DATA (context)->bottom_shadow_pixel),
                 &unused_select_pixel);
  }
  
  /* ### Create a form widget to parent the scroller.
   *
   * This might keep the scroller from becoming smaller than
   * the cell size when there are no scrollbars.
   */
    
  {
    Arg av [50];
    int ac;
    Widget pane, mainw, scroller;
    int border_width = 0;
    
    if (no_edge)
      border_width = 0;
    else
      border_width = 2;
    
    ac = 0;
    XtSetArg (av[ac], XmNx, (Position)x); ac++;
    XtSetArg (av[ac], XmNy, (Position)y); ac++;
    XtSetArg (av[ac], XmNwidth, (Dimension)width - 2*border_width); ac++;
    XtSetArg (av[ac], XmNheight, (Dimension)height - 2*border_width); ac++;
    XtSetArg (av[ac], XmNborderWidth, border_width); ac++;
    mainw = XmCreateForm (parent, "form", av, ac);
    
    ac = 0;
    XtSetArg (av[ac], XmNborderWidth, 0); ac++;
    XtSetArg (av[ac], XmNmarginWidth, 0); ac++;
    XtSetArg (av[ac], XmNmarginHeight, 0); ac++;
    XtSetArg (av[ac], XmNborderColor,
                      CONTEXT_DATA (context)->default_bg_pixel); ac++;
    pane = XmCreatePanedWindow (mainw, "pane", av, ac);
    
    XtVaSetValues (pane,
                   XmNtopAttachment, XmATTACH_FORM,
                   XmNbottomAttachment, XmATTACH_FORM,
                   XmNleftAttachment, XmATTACH_FORM,
                   XmNrightAttachment, XmATTACH_FORM,
                   0);

    /* The actual work area */
    scroller = fe_MakeScrolledWindow (context, pane, "scroller");
    XtVaSetValues (CONTEXT_DATA (context)->scrolled,
                   XmNborderWidth, 0, 0);

    XtManageChild (scroller);
    XtManageChild (pane);
    XtManageChild (mainw);
    
    CONTEXT_DATA (context)->main_pane = mainw;
  } 
  
  fe_load_default_font (context);
  fe_get_context_resources (context);   /* Do other resource db hackery. */
    
  /* FIXME - This is flagrantly wasteful of backing store memory.
     Contexts which are not leaves in the FRAMESET hierarchy don't
     need any backing store or compositor. */
  context->compositor = gtkfe_CreateCompositor(context);
    
  /* Figure out how much space the horizontal and vertical scrollbars take up.
     It's basically impossible to determine this before creating them...
   */
  { 
    Dimension w1 = 0, w2 = 0, h1 = 0, h2 = 0;
    
    XtManageChild (CONTEXT_DATA (context)->hscroll);
    XtManageChild (CONTEXT_DATA (context)->vscroll);
    XtVaGetValues (CONTEXT_DATA (context)->drawing_area,
                                 XmNwidth, &w1,
                                 XmNheight, &h1,
                                 0);
    
    XtUnmanageChild (CONTEXT_DATA (context)->hscroll);
    XtUnmanageChild (CONTEXT_DATA (context)->vscroll);
    XtVaGetValues (CONTEXT_DATA (context)->drawing_area,
                                 XmNwidth, &w2,
                                 XmNheight, &h2,
                                 0);
    
    CONTEXT_DATA (context)->sb_w = w2 - w1;
    CONTEXT_DATA (context)->sb_h = h2 - h1;
    
    /* Now that we know, we don't need to leave them managed. */
  } 
  
  XtVaSetValues (CONTEXT_DATA (context)->scrolled, XmNinitialFocus,
                 CONTEXT_DATA (context)->drawing_area, 0);
    
  fe_SetGridFocus (context);  /* Give this grid focus */
  fe_InitScrolling (context); /* big voodoo */
    
  /* XXXM12N Create and initialize the Image Library JMC callback
     interface.  Also create a new IL_GroupContext for this window.*/
  if (!context->img_cx)
      if (!fe_init_image_callbacks(context))
          {
              return NULL;
          }
    
  fe_InitColormap (context);
    
  if (url)
    {
      /* #### This might not be right, or there might be more that needs
         to be done...   Note that url->history_num is bogus for the new
         context.  url->position_tag might also be context-specific. */
#ifdef XP_UNIX
      /*
       * I believe that if we are restoring from a history entry,
       * we don't want to clear this saved data.
       */
      if (!he)
      {
        XP_MEMSET (&url->savedData, 0, sizeof (SHIST_SavedData));
      }
#else
      XP_MEMSET (&url->savedData, 0, sizeof (SHIST_SavedData));
#endif /* XP_UNIX */
      fe_GetURL (context, url, FALSE);
    }
    
  XFE_SetDocTitle (context, 0);
  CONTEXT_DATA (context)->are_scrollbars_active = True;
#endif /*CTXBUILT*/


  XP_ASSERT(context);
  return(context);
}

void
FE_GetFullWindowSize(MWContext *context,
		     int32 *width,
		     int32 *height)
{
   XP_ASSERT(0);
}

void
FE_GetEdgeMinSize(MWContext *context,
		  int32 *size)
{
   XP_ASSERT(0);
}
