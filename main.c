/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public License
 * Version 1.0 (the "NPL"); you may not use this file except in
 * compliance with the NPL.  You may obtain a copy of the NPL at
 * http://www.mozilla.org/NPL/
 *
 * Software distributed under the NPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the NPL
 * for the specific language governing rights and limitations under the
 * NPL.
 *
 * The Initial Developer of this code under the NPL is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation.  All Rights
 * Reserved.
 */

/*
 * Modified:
 *     1998/04/04    Vidar Hokstad
 *         Started adding stuff from XFE. Seems like much of the mozilla.c stuff from XFE
 *         ought to move into posixfe.
 *
 *     1998/04/06    Vidar Hokstad
 *         Now calls FE_Confirm() to put up the first Gnome/GTK window... :) Just a
 *         test - doesn't do anything useful yet. Annoying that the confirm button
 *         defaults to "No". Guess I'll have to read some of the GTK docs (no,
 *         I haven't even looked at them, which is kind of embarrasing..)
 *
 *         Added some declarations of a couple of jpeg functions gdk_imlib seems
 *         to be using that isn't defined in Netscapes jpeg library.
 * 
 *         Did a very feeble effort to support GTK without Gnome as well... (Not even
 *         checked if it will compile without HAVE_GNOME set).
 *
 *     1998/04/07    Vidar Hokstad
 *         Fixed menu code to use gnome_app_create_menus(). We have to make a choice here...
 *         If we proceed with all the Gnome code, it will be very hard to make a GTK only
 *         version. I vote for using Gnome all the way.
 * 
 *         Moved menu code to browser.c, where the rest of the browser window initialization
 *         now takes place.
 * 
 *         Gotten Init code closer to what it should be - now manages to get the environment
 *         to a state where Mozilla actually retrieve a page from http://www.netscape.com/
 * 
 *     1998/04/08    Vidar Hokstad
 * 
 *         Even more init code added. The entire init code is one big kludge. Lot of interdependencies
 *         that must be sorted out. Added libfont init code to get one step further.
 *
 *     1998/04/08 matt wimer
 *          Looked through main, adding comments to #endif's.
 * 
 *     1998/04/09   Vidar Hokstad
 *          Added some more resource initialization stuff. Un #if 0'ed lots of code, and added
 *          a few more stubs.
 */

#include <signal.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <netinet/in.h>

/*#include <X11/Xlib.h> Do we need this in here. It makes files
                        included from gnome.h whine about "Bool."[MW]*/

#include "structs.h"
#include "ntypes.h"
#include "proto.h"
#include "net.h"
#include "gui.h"
#include "plevent.h"
#include "gtkfe.h"
#include "gtkfe_err.h"
#include "fonts.h"
#include "libmocha.h"
#include "libimg.h"
#include "private/pprthred.h"
#include "seccomon.h"
#include "secstubt.h"
#include "secstubn.h"
#include "secstubs.h"
#include "NSReg.h"
#include "np.h"
#include "nf.h"

#ifdef DO_CHECKMEM
#include "checkmem.h"
#else
#define DMC()
#endif

#include <gdk/gdkx.h>
#ifdef HAVE_GNOME
#include <gnome.h>
#else
#include <gtk/gtk.h>
#endif

#include "gtklayout.h"

#ifndef HAVE_GNOME
/* If Gnome is installed, we use the Gnome DNS functions else we use the 
   supplied DNS functions.*/

#ifdef UNIX_ASYNC_DNS
void PFE_InitDNS_Early(int argc, char **argv);
void fe_check_use_async_dns(void);
XP_Bool fe_UseAsyncDNS(void);
#endif /*UNIX_ASYNC_DNS*/

#endif /*HAVE_GNOME*/

/* Global variables */
PRThread *mozilla_thread;
PREventQueue* mozilla_event_queue;
PRMonitor *fdset_lock;

/* Routines */
void fe_RegisterConverters(void);


extern char **fe_encoding_extensions;

/*** Some macros to shorten the messy splash screen #ifdef's we stole from XFE... [VH] ***/

#undef NSPR_SPLASH  /*Don't load the splash page.[MW]*/

#ifdef NSPR_SPLASH
# define IFSPLASH(__X__) if (fe_globalData.show_splash) { __X__ ; }
# define SPLASHMSG(__X__) if (fe_globalData.show_splash) {     fe_splashUpdateText(XP_GetString(__X__)); }
#else
# define IFSPLASH(__X__)
# define SPLASHMSG(__X__)
#endif  /*NSPR_SPLASH*/ 


#if defined(XP_UNIX) && defined(DEBUG)
    extern PRLogModuleInfo* NETLIB;
#endif


/******** Misc prototypes *****************************/

static void fe_hack_uid(void); /* Defined at end... Should move to posixfe. */

/* Defined at end... Should move to posixfe */
#if 0
static void build_simple_user_agent_string(char *versionLocale);
#endif

void fe_sigchild_handler(int signo);


#include "xp_debug.h"

extern int MKLib_trace_flag;
extern int cl_trace_level;
extern int WWW_TraceFlag;
extern int wf_trace_flag;

#ifdef HAVE_GNOME
char *cmdline_URL = NULL;

error_t
parsethecrap(int key, char *arg, struct argp_state *state)
{
  if(!cmdline_URL)
    cmdline_URL = arg;
  return 0;
}

struct argp_option app_options[] = {
  {NULL, ARGP_KEY_ARG, "blah", OPTION_ARG_OPTIONAL,
   "Whatever loser invented argp should be shot. Half an hour to get argv[1] is NOT sane.", 1},
  {NULL, 0, NULL, 0, NULL, 0 }
};

/* Crap for gnome */
struct argp app_parser = {
app_options,
parsethecrap,
"[dieargp]",
NULL,
NULL,
NULL,
NULL
};
#endif

const char *fe_version = "0.0";

int
main(int argc,
     char *argv[])
{
  struct sigaction act;
  URL_Struct *url;
  MWContext *context;
   
  MKLib_trace_flag = 9999;
  cl_trace_level = 9999;
  WWW_TraceFlag = 9999;
  wf_trace_flag = 9999;

  fe_hack_uid();	/* Do this real early */
  g_print("=============================== Start run ======================\n");

  XP_AppName="mozilla";
  XP_AppCodeName="mozilla";
  XP_AppVersion="CVS-5.0";
  XP_AppLanguage="EN";
  XP_AppPlatform="Linux";

  /* 
    * Check the environment for MOZILLA_NO_ASYNC_DNS.  It would be nice to
    * make this either a pref or a resource/option.  But, at this point in
    * the game neither prefs nor xt resources have been initialized and read.
    */

   
   /*Init the DNS.*/
#ifdef UNIX_ASYNC_DNS
#ifdef HAVE_GNOME
   gnome_init("mozilla", &app_parser, argc, argv, 0, NULL);
   gnome_dns_init(2);  /* What number is best? Needs some testing... */
#else
   if (fe_UseAsyncDNS())
     {
       PFE_InitDNS_Early(argc,argv);/* Do this early (before Java, NSPR.) */
     }
#endif /*HAVE_GNOME*/
#endif /*UNIX_ASYNC_DNS*/


#ifdef MOZILLA_GPROF
  /*
    *    Do this early, but after the async DNS process has spawned.
    */
  gmon_init(); /* initialize, maybe start, profiling */
#endif /*MOZILLA_GPROF*/

  /*
    ** Initialize the runtime, preparing for multi-threading. Make mozilla
    ** a higher priority than any java applet. Java's priority range is
    ** 1-10, and we're mapping that to 11-20 (in sysThreadSetPriority).
    */
#if defined(XP_UNIX) && defined(DEBUG)
  /* Must be initialized, or we seg. fault. Should fix the debug code... [VH] */
  NETLIB = PR_NewLogModule("netlib");
#endif

  PR_SetThreadGCAble();
  PR_SetThreadPriority(PR_GetCurrentThread(), PR_PRIORITY_LAST);
  PR_BlockClockInterrupts();

  mozilla_thread = PR_CurrentThread();
  fdset_lock = PR_NewNamedMonitor("mozilla-fdset-lock");

  /*
    ** Create a pipe used to wakeup mozilla from select. A problem we had
    ** to solve is the case where a non-mozilla thread uses the netlib to
    ** fetch a file. Because the netlib operates by updating mozilla's
    ** select set, and because mozilla might be in the middle of select
    ** when the update occurs, sometimes mozilla never wakes up (i.e. it
    ** looks hung). Because of this problem we create a pipe and when a
    ** non-mozilla thread wants to wakeup mozilla we write a byte to the
    ** pipe.
    */
  mozilla_event_queue = PR_CreateEventQueue("mozilla-event-queue", mozilla_thread);
    
  if (mozilla_event_queue == NULL) {
    XP_ASSERT(0); /* Should use XP_GetString.... */
    fprintf(stderr,"Failed to initialize event queue\n");
    exit(-1);
  }

#ifdef HAVE_GNOME
  /* Seems like Gnome is a really fast moving target... The gnome_init() from
 * gnome_hello in CVS use only gnome_init(name,&argc,&argv). But this compile and
 * run, so I leave it in for now [VH]
 */
#ifndef UNIX_ASYNC_DNS /* If doing async DNS, should init
			  before dns_init() - see above */
  gnome_init("mozilla", &app_parser, argc, argv, 0, NULL);
#endif
#else
  gtk_init(&argc, &argv);
#endif /*HAVE_GNOME*/
   
   
  /** BEGINNING OF HUGE HACK (copied from /ns/cmd/xfe/mozilla_main.c) ****/
   
   /* FIXME -- Should be cleaned up, and moved into separate function 
   like most of this junk [VH] */

  DMC();
   /* Must be called before XFE_ReadPrefs(). */
  fe_InitFonts(GDK_DISPLAY());

  /* FIXME: Does this interfere with any GTK/Gnome signal handlers? [VH] */
     
     /* Install the default signal handlers */
  act.sa_handler = fe_sigchild_handler;
  act.sa_flags = 0;
  sigfillset(&act.sa_mask);
  sigaction (SIGCHLD, &act, NULL);
   
  NR_StartupRegistry();
  fe_RegisterConverters ();  /* this must be before InstallPreferences(),
				and after fe_InitializeGlobalResources(). */
   
  SECNAV_Init();
  DMC();
   
  /* The unit for tcp buffer size is changed from ktypes to bytes */

   /* FIXME --- We hardcode the buffer size, since prefs isn't in yet [VH] */
#if 0
  NET_InitNetLib (fe_globalPrefs.network_buffer_size, 50);
#else
  NET_InitNetLib (32768, 50);
#endif /*0*/


  /* Initialize the message library. */

#ifdef MOZ_MAIL_NEWS
  MSG_InitMsgLib(); 
#endif

  DMC();
  /* Initialize the Image Library */
  g_print("Doing IL_Init()\n");
  IL_Init();

   /* Initialize libmocha after netlib and before plugins. */

  g_print("Doing LM_InitMocha()\n");

  LM_InitMocha ();

  g_print("Doing NPL_Init()\n");
  DMC();

  NPL_Init();

#ifdef XFE_RDF
  /* Initialize RDF */
  {
    RDF_InitParamsStruct rdf_params;

    /* we need some initial context, so create bookmarks */
    fe_createBookmarks(toplevel, NULL, NULL);

    rdf_params.profileURL 
      = XP_PlatformFileToURL(fe_config_dir);
    rdf_params.bookmarksURL
      = XP_PlatformFileToURL(fe_globalPrefs.bookmark_file);
    rdf_params.globalHistoryURL
      = XP_PlatformFileToURL(fe_globalPrefs.history_file);

    RDF_Init(&rdf_params);

    XP_FREEIF(rdf_params.profileURL);
    XP_FREEIF(rdf_params.bookmarksURL);
    XP_FREEIF(rdf_params.globalHistoryURL);
  }
#endif /*XFE_RDF*/


  DMC();

  g_print("Doing URL creation\n");

#ifdef HAVE_GNOME
  if(cmdline_URL)
    url = NET_CreateURLStruct(cmdline_URL, NET_NORMAL_RELOAD);
  else
#endif
#ifdef FINISHED_TESTING
    url = NET_CreateURLStruct("http://www.netscape.com", NET_NORMAL_RELOAD);
#else
    url = NET_CreateURLStruct("file:///usr/doc/HTML/index.html", NET_NORMAL_RELOAD);
#endif

  g_print("gtkfe_MakeNewWindow\n");
  context = GTKFE_MakeNewWindow(url,"Mozilla/GTK",0,0);

  XP_ASSERT(context);

  g_print("Running gtk_main()\n");
  gtk_main();
   
  return 0;
}



/*********** Callback functions *********************************/

gboolean gtkfe_callback_relay(GtkObject *object,
                              struct gtkfe_SigRelayInfo *data,
                              guint n_args,
                              GtkArg *args)
{
  data->callback(data->arg);
  return TRUE;
}

  
gboolean gtkfe_QuitCallback (GtkWidget *widget, void *data) {
    gtk_main_quit ();
    return TRUE;
}
      
void
GTKFE_about_callback (GtkWidget *widget, void *data)
{
   GtkWidget *about;
   gchar *authors[] = {
      /* Here should be your names */
      "The Mozilla/GTK team:",
      "Elliot Lee",
      NULL
     };
   
#ifdef HAVE_GNOME
   about = gnome_about_new ( _("Mozilla/GTK"), "5.0/gtkfe0.01",
			    /* copyrigth notice */
			    "(C) 1998 the Mozilla GTK team and NetScape Communications Inc.",
			    authors,
			    /* another comments */
			    _("This software is licenced under the NetScape Public License"),
			    NULL);
#else
   about = NULL;
#endif /* HAVE_GNOME */
   gtk_widget_show (about);
   
     return;
}                 


/* navigation callbacks */
void gtkfe_BackCallback(GtkWidget *widget) {
  MWContext *context=gtkfe_GetContext(widget);
  FE_BackCommand(context);
}

void gtkfe_ForwardCallback(GtkWidget *widget) {
  MWContext *context=gtkfe_GetContext(widget);
  FE_ForwardCommand(context);
}

void gtkfe_HomeCallback(GtkWidget *widget) {
  MWContext *context=gtkfe_GetContext(widget);
  FE_HomeCommand(context);
}

void gtkfe_EditCallback(GtkWidget *widget) {
  XP_ASSERT(0);  
}

void gtkfe_ReloadCallback(GtkWidget *widget) {
  MWContext *context = gtkfe_GetContext(widget);
  LO_Element *e = LO_XYToNearestElement (context,
                                         GTK_LAYOUT(CONTEXT_DATA (context)->layout)->xoffset,
                                         GTK_LAYOUT(CONTEXT_DATA (context)->layout)->yoffset,
                                         NULL);
  History_entry *he = SHIST_GetCurrent (&context->hist);
  URL_Struct *url;
  gboolean force_reload = FALSE;
  /* We must store the position into the History_entry before making
     a URL_Struct from it. */
  if (e && he)
    SHIST_SetPositionOfCurrentDoc (&context->hist, e->lo_any.ele_id);

  if (he)
    url = (force_reload == NET_RESIZE_RELOAD)
        ? SHIST_CreateWysiwygURLStruct (context, he)
        : SHIST_CreateURLStructFromHistoryEntry (context, he);
  else
    url = 0;

  if (url)
    {
      if (force_reload != NET_DONT_RELOAD)
        url->force_reload = force_reload;

      /* warn plugins that the page relayout is not disasterous so that
         it can fake caching their instances */
      /* XXX Only need to do this if you're eventually going to call
         NPL_EmbedDelete(), which doesn't appear to be the case?
      if (force_reload == NET_RESIZE_RELOAD || force_reload == NET_DONT_RELOAD)
        NPL_SamePage (context);
      */

      FE_GetURL (context, url);
    }
} 

void gtkfe_FindCallback(GtkWidget *widget) {
  XP_ASSERT(0);  
} 
void gtkfe_StopCallback(GtkWidget *widget) {
  XP_InterruptContext(gtkfe_GetContext(widget));
} 

/* file menu callbacks */
void gtkfe_NewWindowCallback(GtkWidget *widget, gpointer data) {
  URL_Struct *url = NET_CreateURLStruct( ((MWContext *)data)->url, NET_NORMAL_RELOAD);

  g_print("gtkfe_MakeNewWindow\n");

  XP_ASSERT( GTKFE_MakeNewWindow(url,"Mozilla/GTK",0,0) );
} 
void gtkfe_OpenPageCallback(GtkWidget *widget, gpointer data) {
  gtkfe_OpenPageDialog( (MWContext *) data, "");
} 
void gtkfe_SaveAsCallback(GtkWidget *widget) {
  XP_ASSERT(0);  
} 
void gtkfe_SaveFrameAsCallback(GtkWidget *widget) {
  XP_ASSERT(0);  
} 
void gtkfe_UploadFileCallback(GtkWidget *widget) {
  XP_ASSERT(0);  
} 
void gtkfe_PrintDocumentCallback(GtkWidget *widget) {
  XP_ASSERT(0);  
} 
void gtkfe_CloseCallback(GtkWidget *widget) {
  XP_ASSERT(0);  
} 
/* edit menu callbacks */
void gtkfe_UndoCallback(GtkWidget *widget) {
  XP_ASSERT(0);  
} 
void gtkfe_RedoCallback(GtkWidget *widget) {
  XP_ASSERT(0);  
} 
void gtkfe_CutCallback(GtkWidget *widget) {
  XP_ASSERT(0);  
} 
void gtkfe_CopyCallback(GtkWidget *widget) {
  XP_ASSERT(0);  
} 
void gtkfe_PasteCallback(GtkWidget *widget) {
  XP_ASSERT(0);  
} 
void gtkfe_FindInDocumentCallback(GtkWidget *widget) {
  XP_ASSERT(0);  
} 
void gtkfe_EditPreferencesCallback(GtkWidget *widget) {
  XP_ASSERT(0);  
} 
/* view menu callbacks */
void gtkfe_ShowNavibarCallback(GtkWidget *widget) {
  XP_ASSERT(0);  
} 
void gtkfe_ShowPersonalToolbarCallback(GtkWidget *widget) {
  XP_ASSERT(0);  
} 
void gtkfe_ShowLocationbarCallback(GtkWidget *widget) {
  XP_ASSERT(0);  
} 
void gtkfe_HideMenubarCallback(GtkWidget *widget) {
  XP_ASSERT(0);  
} 


/************* Misc. hacks ***********************************************/

  
#if 1
/* This one is now in posixfe/fileutils.c, but I get some weird linking problem.
 * Guess the Makefile is messed up, or I'm just to sleepy to see some obvious
 * mistake ;) [VH]
 */
void fe_GetProgramDirectory(char *buf, int len) {
  snprintf(buf, len, "%s", getenv("HOME"));
}
#endif


/* stubs because I'm lazy */

char *XmStringCreateLtoR(void) {    XP_ASSERT(0);
   return strdup("foo"); }
void XFE_AskStreamQuestion(void){
   XP_ASSERT(0);
}
void MimeGuessURLContentName(void){
   XP_ASSERT(0);
}
void FE_ShowMinibuffer(void){
   XP_ASSERT(0);
}
char *_XmStrings(void){    XP_ASSERT(0);
   return strdup("astring");
}

void fe_MakeSaveAsStream () {
   XP_ASSERT(0);
}

void fe_MakeSaveAsStreamNoPrompt () {
   XP_ASSERT(0);
}

void fe_MakeViewSourceStream () {
   XP_ASSERT(0);
}


/*******************
 * Signal handlers *
 *******************/
void fe_sigchild_handler(int signo)
{
  pid_t pid;
  int status = 0;
  while ((pid = waitpid(-1, &status, WNOHANG)) > 0) {
#ifdef DEBUG_dp
    fprintf(stderr, "fe_sigchild_handler: Reaped pid %d.\n", pid);
#endif
  }
}



/***************** FUNCTIONS TO MOVE TO POSIXFE ****************************/

/* Should move this to posixfe. Taken from XFE. [VH19980404] */

static void
fe_hack_uid(void)
{
  /* If we've been run as setuid or setgid to someone else (most likely root)
     turn off the extra permissions.  Nobody ought to be installing Mozilla
     as setuid in the first place, but let's be extra special careful...
     Someone might get stupid because of movemail or something.
  */
  setgid (getgid ());
  setuid (getuid ());

  /* Is there anything special we should do if running as root?
     Features we should disable...?
     if (getuid () == 0) ...
   */
}




/*
 * build_simple_user_agent_string
 * 
 * From XFE [VH]
 */
#if 0
static void
build_simple_user_agent_string(char *versionLocale)
{
    char buf [1024];
    int totlen;

    if (XP_AppVersion) {
	GTKFE_FREE((void *)XP_AppVersion);
	XP_AppVersion = NULL;
    }

    /* SECNAV_Init must have a language string in XP_AppVersion.
     * If missing, it is supplied here.  This string is very short lived.
     * After SECNAV_Init returns, build_user_agent_string() will build the 
     * string that is expected by existing servers.  
     */
    if (!versionLocale || !*versionLocale) {	
	versionLocale = " [en]";   /* default is english for SECNAV_Init() */
    }

    /* be safe about running past end of buf */
    totlen = sizeof buf;
    memset(buf, 0, totlen);
    strncpy(buf, fe_version, totlen - 1);
    totlen -= strlen(buf);
    strncat(buf, versionLocale, totlen - 1);

    XP_AppVersion = strdup (buf);
    /* if it fails, leave XP_AppVersion NULL */
}
#endif

void
fe_RegisterConverters (void)
{
#ifdef NEW_DECODERS
  NET_ClearAllConverters ();
#endif /* NEW_DECODERS */
   
   if (fe_encoding_extensions)
    {
      int i = 0;
      while (fe_encoding_extensions [i])
	GTKFE_FREE (fe_encoding_extensions [i++]);
      GTKFE_FREE (fe_encoding_extensions);
      fe_encoding_extensions = 0;
    }

#if 0   
  /* register X specific decoders
   */
  if (fe_globalData.encoding_filters)
    {
      char *copy = strdup (fe_globalData.encoding_filters);
      char *rest = copy;
      char *end = rest + strlen (rest);

      int exts_count = 0;
      int exts_size = 10;
      char **all_exts = (char **) GTKFE_ALLOC (sizeof (char *) * exts_size);

      while (rest < end)
	{
	  char *start;
	  char *eol, *colon;
	  char *input, *output, *extensions, *command;
	  eol = strchr (rest, '\n');
	  if (eol) *eol = 0;

	  rest = fe_StringTrim (rest);
	  if (! *rest)
	    /* blank lines are ok */
	    continue;

	  start = rest;

	  colon = strchr (rest, ':');
	  if (! colon) goto LOSER;
	  *colon = 0;
	  input = fe_StringTrim (rest);
	  rest = colon + 1;

	  colon = strchr (rest, ':');
	  if (! colon) goto LOSER;
	  *colon = 0;
	  output = fe_StringTrim (rest);
	  rest = colon + 1;

	  colon = strchr (rest, ':');
	  if (! colon) goto LOSER;
	  *colon = 0;
	  extensions = fe_StringTrim (rest);
	  rest = colon + 1;

	  command = fe_StringTrim (rest);
	  rest = colon + 1;
	  
	  if (*command)
	    {
	      /* First save away the extensions. */
	      char *rest = extensions;
	      while (*rest)
		{
		  char *start;
		  char *comma, *end;
		  while (isspace (*rest))
		    rest++;
		  start = rest;
		  comma = XP_STRCHR (start, ',');
		  end = (comma ? comma - 1 : start + strlen (start));
		  while (end >= start && isspace (*end))
		    end--;
		  if (comma) end++;
		  if (start < end)
		    {
		      all_exts [exts_count] =
			(char *) GTKFE_ALLOC (end - start + 1);
		      strncpy (all_exts [exts_count], start, end - start);
		      all_exts [exts_count][end - start] = 0;
		      if (++exts_count == exts_size)
			all_exts = (char **)
			  GTKFE_REALLOC (all_exts,
				   sizeof (char *) * (exts_size += 10));
		    }
		  rest = (comma ? comma + 1 : end);
		}
	      all_exts [exts_count] = 0;
	      fe_encoding_extensions = all_exts;

	      /* Now register the converter. */
	      NET_RegisterExternalDecoderCommand (input, output, command);
	    }
	  else
	    {
	  LOSER:
	      fprintf (stderr,
				   XP_GetString(GTKFE_COMMANDS_UNPARSABLE_ENCODING_FILTER_SPEC),
				   fe_progname, start);
	    }
	  rest = (eol ? eol + 1 : end);
	}
      GTKFE_FREE (copy);
    }
#endif
   
  /* Register standard decoders
     This must come AFTER all calls to NET_RegisterExternalDecoderCommand(),
     (at least in the `NEW_DECODERS' world.)
   */
  NET_RegisterMIMEDecoders ();

  /* How to save to disk. */
  NET_RegisterContentTypeConverter ("*", FO_SAVE_AS, NULL,
				    (NET_Converter *)fe_MakeSaveAsStream);
   
  /* Saving any binary format as type `text' should save as `source' instead.
   */
  NET_RegisterContentTypeConverter ("*", FO_SAVE_AS_TEXT, NULL,
				    (NET_Converter *)fe_MakeSaveAsStreamNoPrompt);
  NET_RegisterContentTypeConverter ("*", FO_QUOTE_MESSAGE, NULL,
				    (NET_Converter *)fe_MakeSaveAsStreamNoPrompt);

  /* default presentation converter - offer to save unknown types. */
  NET_RegisterContentTypeConverter ("*", FO_PRESENT, NULL,
				    (NET_Converter *)fe_MakeSaveAsStream);

  NET_RegisterContentTypeConverter ("*", FO_VIEW_SOURCE, NULL,
				    (NET_Converter *)fe_MakeViewSourceStream);

#ifndef NO_MOCHA_CONVERTER_HACK
  /* libmocha:LM_InitMocha() installs this convert. We blow away all
   * converters that were installed and hence these mocha default converters
   * dont get recreated. And mocha has no call to re-register them.
   * So this hack. - dp/brendan
   */
  NET_RegisterContentTypeConverter(APPLICATION_JAVASCRIPT, FO_PRESENT, 0,
				   NET_CreateMochaConverter);
#endif /* NO_MOCHA_CONVERTER_HACK */

  /* Parse stuff out of the .mime.types and .mailcap files.
   * We dont have to check dates of files for modified because all that
   * would have been done by the caller. The only place time checking
   * happens is
   * (1) Helperapp page is created
   * (2) Helpers are being saved (OK button pressed on the General Prefs).
   */

#if 0   
   NET_InitFileFormatTypes (fe_globalPrefs.private_mime_types_file,
			   fe_globalPrefs.global_mime_types_file);
  fe_isFileChanged(fe_globalPrefs.private_mime_types_file, 0,
		   &fe_globalData.privateMimetypeFileModifiedTime);

  NET_RegisterConverters (fe_globalPrefs.private_mailcap_file,
			  fe_globalPrefs.global_mailcap_file);
  fe_isFileChanged(fe_globalPrefs.private_mailcap_file, 0,
		   &fe_globalData.privateMailcapFileModifiedTime);
   
#endif
   
#ifndef NO_WEB_FONTS
  /* Register webfont converters */
  NF_RegisterConverters();
#endif /* NO_WEB_FONTS */

#if 0
  /* Plugins go on top of all this */
  fe_RegisterPluginConverters();
#endif
}
