/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public License
 * Version 1.0 (the "NPL"); you may not use this file except in
 * compliance with the NPL.  You may obtain a copy of the NPL at
 * http://www.mozilla.org/NPL/
 *
 * Software distributed under the NPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the NPL
 * for the specific language governing rights and limitations under the
 * NPL.
 *
 * The Initial Developer of this code under the NPL is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation.  All Rights
 * Reserved.
 */

/* 
   gtkimg.c --- gtk functions for fe
                 specific images stuff.
*/

#define ROUNDUP(x,y)    ((((x)+(y)-1)/(y))*(y))

#define JMC_INIT_IMGCB_ID

#include "xp_core.h"
#include "structs.h"
#include "ntypes.h"

#include "libimg.h"
#include "il_util.h"
#include "prtypes.h"
#include "gtkfe.h"
#ifdef HAVE_GNOME
#include <gnome.h>
#include <gdk_imlib.h>
#endif
#include <gtk/gtk.h>

typedef struct fe_PixmapClientData {
    GdkPixmap *pixmap;
} fe_PixmapClientData;

JMC_PUBLIC_API(void)
_IMGCB_init(struct IMGCB* self, JMCException* *exception)
{
   /* FIXME: Get's called now... Time to implement... */
#ifdef HAVE_GNOME
  gdk_imlib_init();
#else
   XP_ASSERT(0);
#endif
}

JMC_PUBLIC_API(void*)
_IMGCB_getBackwardCompatibleInterface(struct IMGCB* self,
				      const JMCInterfaceID* iid,
				      JMCException* *exception)
{
  return NULL;
}

JMC_PUBLIC_API(void)
_IMGCB_NewPixmap(IMGCB* img_cb, jint op, void *dpy_cx, jint width, jint height,
		 IL_Pixmap *image, IL_Pixmap *mask) 
{
  /* Copied from cmd/xfe/images.c and then Gdk-ized */
    uint8 img_depth;
    NI_PixmapHeader *img_header = &image->header;
    NI_PixmapHeader *mask_header = mask ? &mask->header : NULL;
    MWContext *context = (MWContext *)dpy_cx; /* XXXM12N This should be the
                                                 FE's display context. */
    GtkWidget *widget = ((fe_ContextData *)context->fe.data)->main;
    GdkWindow *window;
    GdkPixmap *img_x_pixmap, *mask_x_pixmap = NULL;
    gint visual_depth;
    fe_PixmapClientData *img_client_data, *mask_client_data = NULL;
    GdkVisual *v;

    if(!GTK_WIDGET_REALIZED(widget)) gtk_widget_realize(widget);
    window = widget->window;

    /* Allocate the client data structures for the IL_Pixmaps. */
    img_client_data = XP_NEW_ZAP(fe_PixmapClientData);
    if (!img_client_data) {
        image->bits = NULL;
        mask->bits = NULL;
        return;
    }
    if (mask) {
        mask_client_data = XP_NEW_ZAP(fe_PixmapClientData);
        if (!mask_client_data) {
            image->bits = NULL;
            mask->bits = NULL;
            return;
        }
    }

    v = gdk_imlib_get_visual();
    visual_depth = v->depth;
    g_warning("visual_depth = %d\n", visual_depth);

    /* Override the image and mask dimensions with the requested target
       dimensions.  This instructs the image library to do any necessary
       scaling. */
    img_header->width = width;
    img_header->height = height;
    if (mask) {
        mask_header->width = width;
        mask_header->height = height;
    }

    /* Override the image colorspace with the display colorspace.  This
       instructs the image library to decode to the display colorspace
       instead of decoding to the image's source colorspace. */
    IL_ReleaseColorSpace(img_header->color_space);
    img_header->color_space = context->color_space;
    IL_AddRefToColorSpace(img_header->color_space);

    /* Compute the number of bytes per scan line for the image and mask,
       and make sure it is quadlet aligned. */
    img_depth = img_header->color_space->pixmap_depth;
    img_header->widthBytes = (img_header->width * img_depth + 7) / 8;
    img_header->widthBytes = ROUNDUP(img_header->widthBytes, 4);
    if (mask) {
        mask_header->widthBytes = (mask_header->width + 7) / 8;
        mask_header->widthBytes = ROUNDUP(mask_header->widthBytes, 4);
    }

    /* Allocate memory for the image bits, and for the mask bits (if
       required.) */
    image->bits = calloc(img_header->widthBytes * img_header->height, 1);
    if (!image->bits)
        return;
    if (mask) {
        mask->bits = calloc(mask_header->widthBytes * mask_header->height, 1);
        if (!mask->bits) {
            free(image->bits);
            image->bits = NULL;
            return;
        }
    }

    /* Create an X pixmap for the image, and for the mask (if required.) */
    img_x_pixmap = gdk_pixmap_new(window, img_header->width,
                                  img_header->height, visual_depth);

    if (mask)
      mask_x_pixmap = gdk_pixmap_new(window, mask_header->width,
                                     mask_header->height, 1);

    /* Fill in the pixmap client_data.  We store the Display pointer for use
       in DestroyPixmap, which can be called after the FE's display context
       (MWContext) has been destroyed. */
    img_client_data->pixmap = img_x_pixmap;
    image->client_data = (void *)img_client_data;
    if (mask) {
        mask_client_data->pixmap = mask_x_pixmap;
        mask->client_data = (void *)mask_client_data;
    }
}

JMC_PUBLIC_API(void)
_IMGCB_UpdatePixmap(IMGCB* img_cb, jint op, void* dpy_cx, IL_Pixmap* pixmap,
		    jint x_offset, jint y_offset, jint width, jint height)
{
   XP_ASSERT(0);
}

JMC_PUBLIC_API(void)
_IMGCB_ControlPixmapBits(IMGCB* img_cb, jint op, void* dpy_cx,
                         IL_Pixmap* pixmap, IL_PixmapControl message)
{
   XP_ASSERT(0);
}

JMC_PUBLIC_API(void)
_IMGCB_DestroyPixmap(IMGCB* img_cb, jint op, void* dpy_cx, IL_Pixmap* pixmap)
{
   XP_ASSERT(0);
}

JMC_PUBLIC_API(void)
_IMGCB_DisplayPixmap(IMGCB* img_cb, jint op, void* dpy_cx, IL_Pixmap* image, 
                     IL_Pixmap* mask, jint x, jint y, jint x_offset,
                     jint y_offset, jint width, jint height, jint req_w, jint req_h)
{
   XP_ASSERT(0);
}

JMC_PUBLIC_API(void)
_IMGCB_GetIconDimensions(IMGCB* img_cb, jint op, void* dpy_cx, int* width,
                         int* height, jint icon_number)
{
   XP_ASSERT(0);
}

JMC_PUBLIC_API(void)
_IMGCB_DisplayIcon(IMGCB* img_cb, jint op, void* dpy_cx, jint x, jint y,
                   jint icon_number)
{
   XP_ASSERT(0);
}

/* Mocha image group observer callback. */
void
FE_MochaImageGroupObserver(XP_Observable observable, XP_ObservableMsg message,
                           void *message_data, void *closure)
{   
   XP_ASSERT(0);
}   
