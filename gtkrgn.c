/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public License
 * Version 1.0 (the "NPL"); you may not use this file except in
 * compliance with the NPL.  You may obtain a copy of the NPL at
 * http://www.mozilla.org/NPL/
 *
 * Software distributed under the NPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the NPL
 * for the specific language governing rights and limitations under the
 * NPL.
 *
 * The Initial Developer of this code under the NPL is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation.  All Rights
 * Reserved.
 */

/*   gtkrgn.c --- gtk functions dealing with front-end */

#include "xp.h"
#include "fe_rgn.h"
#include "gtkfe.h"

#include <gdk/gdkx.h>
#include <gdk/gdk.h>

#define ASSERT_NYI	XP_ASSERT(0 && "NYI!");

#define GTK_REGION(reg) ((GTKFE_Region)(reg))

#define XP_RECT_TO_GDK(xp, gdk) \
(gdk)->x = (guint16)((xp)->left); \
(gdk)->y = (guint16)((xp)->top); \
(gdk)->width = (guint16)((xp)->right - (xp)->left); \
(gdk)->height = (guint16)((xp)->bottom - (xp)->top);

static GdkRegion *empty_region = NULL;

void FE_PrintRegion(FE_Region region);

/* #define DEBUG_RGN 1 */

FE_Region
FE_CreateRegion()
{
  GTKFE_Region retval;
  retval = GTKFE_NEW(GdkRegion*);
  *retval = gdk_region_new();

#ifdef DEBUG_RGN
  g_print(__FUNCTION__ " "); FE_PrintRegion(retval);
#endif

  return retval;
}

/*
 * Oh dear, this is a nasty hack.  Replace with gdk_ call when it
 * appears in the gtk+ distribution. [shaver]
 */
#include <gdk/gdkprivate.h>

FE_Region
FE_CreateRectRegion(XP_Rect *rect)
{
  GTKFE_Region retval;
  GdkRectangle r;

  if(!empty_region)
    empty_region = gdk_region_new();

  retval = GTKFE_NEW(GdkRegion*);

  XP_RECT_TO_GDK(rect, &r);
  *retval = gdk_region_union_with_rect(empty_region, &r);

#ifdef DEBUG_RGN
  g_print(__FUNCTION__ " ");
  FE_PrintRegion(retval);
#endif

  return retval;
}

void
FE_DestroyRegion(FE_Region region)
{
#ifdef DEBUG_RGN
  g_print(__FUNCTION__ " "); FE_PrintRegion(region);
#endif
  gdk_region_destroy(GDK_REGION(region));
  GTKFE_FREE(region);
}

FE_Region
FE_CopyRegion(FE_Region src,
	      FE_Region dst)
{
  GTKFE_Region retval;

  if(!empty_region)
    empty_region = gdk_region_new();

  if(dst)
    {
      if(GDK_REGION(dst))
        gdk_region_destroy(GDK_REGION(dst));
      retval = dst;
    }
  else
    retval = GTKFE_NEW(GdkRegion*);
  GDK_REGION(retval) = gdk_regions_union(GDK_REGION(src), dst?GDK_REGION(dst):empty_region);

#ifdef DEBUG_RGN
  g_print(__FUNCTION__ "(to %p[%p]) ", retval, GDK_REGION(retval));
  FE_PrintRegion(src);
  FE_PrintRegion(retval);
#endif

  return retval;
}

FE_Region 
FE_SetRectRegion(FE_Region region, 
		 XP_Rect *rect)
{
  GTKFE_Region retval;
  GdkRectangle r;
  XP_RECT_TO_GDK(rect, &r);
  retval = GTKFE_NEW(GdkRegion*);
  *retval = gdk_region_union_with_rect(GDK_REGION(region), &r);
#ifdef DEBUG_RGN
  g_print(__FUNCTION__ "(to %p[%p]) ", retval, GDK_REGION(retval)); FE_PrintRegion(region);
#endif

  return retval;
}

#define XREGION(feregion) ((GdkRegionPrivate *)feregion)->xregion

void
FE_IntersectRegion(FE_Region src1, FE_Region src2, FE_Region dst)
{
  if(dst && GDK_REGION(dst) && (dst != src1 && dst != src2))
    gdk_region_destroy(GDK_REGION(dst));
  GDK_REGION(dst) = gdk_regions_intersect(GDK_REGION(src1), GDK_REGION(src2));
}

void
FE_UnionRegion(FE_Region src1, FE_Region src2, FE_Region dst)
{
  if(dst && GDK_REGION(dst) && (dst != src1 && dst != src2))
    gdk_region_destroy(GDK_REGION(dst));
  GDK_REGION(dst) = gdk_regions_union(GDK_REGION(src1), GDK_REGION(src2));
}

void
FE_SubtractRegion(FE_Region src1, FE_Region src2, FE_Region dst)
{
  if(dst && GDK_REGION(dst) && (dst != src1 && dst != src2))
    gdk_region_destroy(GDK_REGION(dst));
  GDK_REGION(dst) = gdk_regions_subtract(GDK_REGION(src1), GDK_REGION(src2));
}

XP_Bool 
FE_IsEmptyRegion(FE_Region region)
{
  XP_Bool retval;
  retval = gdk_region_empty(GDK_REGION(region));
#ifdef DEBUG_RGN
  g_print(__FUNCTION__ " = %d: ", retval); FE_PrintRegion(region);
#endif
  return retval;
}

void 
FE_GetRegionBoundingBox(FE_Region region, 
			XP_Rect *bbox)
{
  GdkRectangle rect;

  XP_ASSERT(region);
  XP_ASSERT(bbox);

  gdk_region_get_clipbox(GDK_REGION(region),
                         &rect);

  bbox->left = (int32)rect.x;
  bbox->top = (int32)rect.y;
  bbox->right = (int32)(rect.x + rect.width);
  bbox->bottom = (int32)(rect.y + rect.height);
#ifdef DEBUG_RGN
  g_print(__FUNCTION__ " = (%d,%d)-(%d,%d): ", bbox->left,
          bbox->top, bbox->right, bbox->bottom); FE_PrintRegion(region);
#endif
}

XP_Bool 
FE_IsEqualRegion(FE_Region rgn1, 
		 FE_Region rgn2)
{
  return gdk_region_equal(GDK_REGION(rgn1), GDK_REGION(rgn2));
}

void 
FE_OffsetRegion(FE_Region region, 
		int32 xOffset, 
		int32 yOffset)
{
  gdk_region_offset(GDK_REGION(region), xOffset, yOffset);
}

XP_Bool 
FE_RectInRegion(FE_Region region, 
		XP_Rect *rect)
{
   GdkRectangle gdkRect;
   XP_RECT_TO_GDK(rect, &gdkRect);
   return gdk_region_rect_in(GDK_REGION(region), &gdkRect);
}

/* Don't ask, just believe the magic, the magic of the private data structures,
 the magic that only Xlib can know */
typedef struct {
  short x1, x2, y1, y2;
} BOX;
typedef struct _XRegion {
    long size;
    long numRects;
    BOX *rects;
    BOX extents;
} REGION;

void printrect(gpointer closure, XP_Rect *rect)
{
  g_print("     (%d, %d) +%d +%d\n",
          rect->left, rect->top,
          rect->right - rect->left,
          rect->bottom - rect->top);
}

void
FE_PrintRegion(FE_Region region)
{
  g_print("Region %p[%p]:\n", region, GDK_REGION(region));
  FE_ForEachRectInRegion(region, printrect, NULL);
  fflush(stdout);
}

/* For each rectangle that makes up this region, call the func */
void 
FE_ForEachRectInRegion(FE_Region region, 
		       FE_RectInRegionFunc func,
		       void * closure)
{
  GdkRegionPrivate *rp;
  Region pRegion;
  register int nbox;
  register BOX *pbox;
  XP_Rect rect;

  XP_ASSERT(region);
  XP_ASSERT(func);

  rp = (GdkRegionPrivate *)GDK_REGION(region);
  pRegion = rp->xregion;
  pbox = pRegion->rects;
  nbox = pRegion->numRects;

  while(nbox--)
    {
      rect.left = pbox->x1;
      rect.right = pbox->x2;
      rect.top = pbox->y1;
      rect.bottom = pbox->y2;
      (*func)(closure, &rect);
      pbox++;
    }
}

#ifdef DEBUG
void 
FE_HighlightRect(void *context, 
		 XP_Rect *rect, 
		 int how_much)
{
   ASSERT_NYI;
}

void 
FE_HighlightRegion(void *context, 
		   FE_Region region, 
		   int how_much)
{
   ASSERT_NYI;
}
#endif /* DEBUG */
