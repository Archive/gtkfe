/* Copyright Owen Taylor, 1998
 * 
 * This file may be distributed under either the terms of the
 * Netscape Public License, or the GNU Library General Public License
 *
 * Note: No GTK+ or Mozilla code should be added to this file.
 * The coding style should be that of the the GTK core.
 */

#include <stdio.h>
#include <gtk/gtk.h>
#include "gtklayout.h"

char *_dl_library_path = "";

void 
expose_handler (GtkWidget *widget, GdkEventExpose *event)
{
  gdk_draw_rectangle (GTK_LAYOUT (widget)->bin_window, 
		      widget->style->base_gc[GTK_STATE_NORMAL],
		      TRUE,
		      event->area.x, event->area.y,
		      event->area.width, event->area.height);
}

int main (int argc, char **argv)
{
  GtkWidget *window;
  GtkWidget *table;
  GtkWidget *layout;
  GtkWidget *hscroll;
  GtkWidget *vscroll;
  GtkWidget *button;
  
  gchar buf[16];

  gint i, j;
  
  gtk_init (&argc, &argv);

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (gtk_main_quit), NULL);

  table = gtk_table_new (2, 2, FALSE);
  gtk_container_add (GTK_CONTAINER (window), table);
  gtk_widget_show (table);
  
  layout = gtk_layout_new (NULL, NULL);
  gtk_table_attach (GTK_TABLE (table), layout,
		    0, 1, 0, 1, 
		    GTK_FILL | GTK_EXPAND,
		    GTK_FILL | GTK_EXPAND,
		    0, 0);

  gtk_widget_set_events (layout, GDK_EXPOSURE_MASK);
  gtk_signal_connect (GTK_OBJECT (layout), "expose_event",
		      GTK_SIGNAL_FUNC (expose_handler), NULL);

  gtk_layout_set_size (GTK_LAYOUT (layout), 1600, 1600);
  gtk_widget_show (layout);

  hscroll = gtk_hscrollbar_new (gtk_layout_get_hadjustment (GTK_LAYOUT (layout)));
  gtk_table_attach (GTK_TABLE (table), hscroll,
		    0, 1, 1, 2, 
		    GTK_FILL | GTK_EXPAND,
		    0,
		    0, 0);
  gtk_widget_show (hscroll);
							    
  vscroll = gtk_vscrollbar_new (gtk_layout_get_vadjustment (GTK_LAYOUT (layout)));
  gtk_table_attach (GTK_TABLE (table), vscroll,
		    1, 2, 0, 1, 
		    0,
		    GTK_FILL | GTK_EXPAND,
		    0, 0);
  gtk_widget_show (vscroll);

  for (i=0 ; i < 16 ; i++)
    for (j=0 ; j < 16 ; j++)
      {
	sprintf(buf, "Button %d, %d", i, j);
	button = gtk_button_new_with_label (buf);
	gtk_layout_put (GTK_LAYOUT (layout), button,
			j*100, i*100);
	gtk_widget_show (button);
      }
  
  gtk_widget_show (window);

  gtk_main();

  return 0;
}
