/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public License
 * Version 1.0 (the "NPL"); you may not use this file except in
 * compliance with the NPL.  You may obtain a copy of the NPL at
 * http://www.mozilla.org/NPL/
 *
 * Software distributed under the NPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the NPL
 * for the specific language governing rights and limitations under the
 * NPL.
 *
 * The Initial Developer of this code under the NPL is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation.  All Rights
 * Reserved.
 */

/*   gtksec.c --- gtk fe handling of FE security related stuff. */

#include "structs.h"
#include "ntypes.h"
#include "xpassert.h"
#include "proto.h"
#include "fe_proto.h"
#ifdef HAVE_GNOME
#include <gnome.h>
#else
#include <gtk/gtk.h>
#endif

#define SD_INSECURE_POST_FROM_SECURE_DOC              1
#define SD_INSECURE_POST_FROM_INSECURE_DOC            2
#define SD_ENTERING_SECURE_SPACE                      3
#define SD_LEAVING_SECURE_SPACE                       4
#define SD_INSECURE_DOCS_WITHIN_SECURE_DOCS_NOT_SHOWN 5
#define SD_REDIRECTION_TO_INSECURE_DOC                6
#define SD_REDIRECTION_TO_SECURE_SITE                 7

static void
gtkfe_SecurityDialogExit(
#ifdef HAVE_GNOME
                         GnomeMessageBox *mb,
#else
                         GtkWidget *mb,
#endif
                         gint button)
{
  gtk_object_set_data(GTK_OBJECT(mb), "button_clicked", (gpointer)button);
  gtk_widget_hide(GTK_WIDGET(mb));
  gtk_main_quit();
}

Bool
FE_SecurityDialog(MWContext* context,
		  int message,
		  XP_Bool* prefs_toggle)
{
#ifdef HAVE_GNOME
  GtkWidget *mb, *toggle = NULL;

  if (prefs_toggle && !*prefs_toggle)
    return TRUE;

  mb = gnome_message_box_new("SecurityDialog", GNOME_MESSAGE_BOX_QUESTION,
                             "Yes", "No", NULL);
  gnome_message_box_set_default(GNOME_MESSAGE_BOX(mb), 0);

  if(prefs_toggle)
    {
      toggle = gtk_check_button_new_with_label("Warn me about this every time");
      gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(toggle), prefs_toggle?(*prefs_toggle):TRUE);
      gtk_widget_show(toggle);
      gtk_container_add(GTK_CONTAINER(GTK_DIALOG(mb)->vbox), toggle);
    }

  gtk_signal_connect(GTK_OBJECT(mb), "clicked",
                     GTK_SIGNAL_FUNC(gtkfe_SecurityDialogExit), NULL);
  gnome_dialog_set_modal(GNOME_DIALOG(mb));
  gtk_widget_show(mb);
  gtk_main();

  if(prefs_toggle)
    {
      *prefs_toggle = (GTK_TOGGLE_BUTTON(toggle)->active)?TRUE:FALSE;
    }

  gtk_widget_destroy(mb);

#else
   XP_ASSERT(0);
#endif

   return TRUE;
}

void
FE_SetPasswordEnabled(MWContext *context, PRBool usePW)
{
  /* XFE doesn't care, so I don't think we need to either */
}
