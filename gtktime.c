/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public License
 * Version 1.0 (the "NPL"); you may not use this file except in
 * compliance with the NPL.  You may obtain a copy of the NPL at
 * http://www.mozilla.org/NPL/
 *
 * Software distributed under the NPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the NPL
 * for the specific language governing rights and limitations under the
 * NPL.
 *
 * The Initial Developer of this code under the NPL is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation.  All Rights
 * Reserved.
 */
/*   gtktime.c --- gtk functions dealing with front-end
                  timers and timeouts.

     [MW] = Matthew Wimer  <matt@cgibuilder.com>
     [VH] = Vidar Hokstad  <vidarh@ncg.net>

 Modified:
    1998/04/05   Metthew Wimer
    1998/04/06   Vidar Hokstad
        Added some XP_ASSERT's ;) Take a look at README-DEBUG for
        updated explanation.
  
*/

#include "xp_core.h"
#include "structs.h"
#include "ntypes.h"
#include "fe_proto.h"
/* GTK header stuff */
#include <gtk/gtk.h>
#include <glib.h>

/*
** FE_SetTimeout - Do whatever needs to be done to register a timeout to happen
** after msecs milliseconds.
**
** This function should return some unique ID for the timeout, or NULL
** if some operation fails.
**
** once the timeout has fired, it should not be fired again until
** re-registered.  That is, if the FE maintains a list of timeouts, it
** should remove the timeout after it's fired.  
*/


/*
  Thougths on this file. [MW]

  This looks to be rather easy for someone to implement.


  FE_SetTimeout:
  1. Convert uint32s to guint32s
  2. Pass on a struct our_gtk_timeout_st with 
     1.TimeoutCallbackFunction func, 
     2. void * closure, and
     3. gint timer_id
     to GtkFunction our_gtk_times_up_function as gpointer data.

  FE_ClearTimeout:
  1. (void *timer_id) is equal to the return of FE_SetTimeout so this is
      done for us.
  2. Cast (void *timer_id) to be gint, and use it in 
     void gtk_timeout_remove (gint tag).

  
  Note:  I don't know what the XP_ASSERTs do so i will just leave them in. :) 
 
*/

/* This should go somewhere else, but im not sure where. :) [MW] */
struct _GTKFE_timeout_handler_data {
  void *closure;
  TimeoutCallbackFunction func;
  gint timer_id;
};
typedef struct _GTKFE_timeout_handler_data GTKFE_timeout_handler_data;

static gint GTKFE_timeout_handler (gpointer data);

void*
FE_SetTimeout(TimeoutCallbackFunction func,
	      void *closure,
	      uint32 msecs)
{
  /*
      http://www.gtk.org/tutorial/gtk_tut-15.html#ss15.1  
      This looks like a good place to start building the gtk
      FE_SetTimeout function. [MW]

      gint gtk_timeout_add (guint32 interval,
         GtkFunction function,
         gpointer data);

  */
   GTKFE_timeout_handler_data *new_gtk_timeout;

   XP_ASSERT(func);
   
#if 0   
   XP_ASSERT(closure); /* FIXME: Is this assert right? Probably not... [VH] */
#endif
   
   new_gtk_timeout = XP_NEW(GTKFE_timeout_handler_data);
   /* our_gtk_times_up_function needs to free this memory. */
   
   /*Init the timeout struct */
   new_gtk_timeout->closure = closure;
   new_gtk_timeout->func = func;

   /* Register this timeout with gtk and init the timer_id. */
   new_gtk_timeout->timer_id = 
     gtk_timeout_add ((guint32) msecs,
                      GTKFE_timeout_handler,
                      (gpointer)new_gtk_timeout);

   
   return (void *)new_gtk_timeout->timer_id; 
   /* ...so the caller can stop this 
     timer before it runs out if need be. */

}

/*
** FE_ClearTimeout - Do whatever needs to happen to unregister a
** timeout, given it's ID.  
*/
void 
FE_ClearTimeout(void *timer_id)
{
   /*
     http://www.gtk.org/tutorial/gtk_tut-15.html#ss15.1
     see above.  [MW]
   
     void gtk_timeout_remove (gint tag);
   */

  gint tag = (gint)timer_id;  /* This is ok i guess. 
                                 The timer_id is of gint originally.*/

  gtk_timeout_remove(tag);  /* done. */

}



/*This may also belong somewhere else. */
gint GTKFE_timeout_handler (gpointer data)
{
   /* Extract calldata. */
   GTKFE_timeout_handler_data *this_gtk_timeout = data;

   XP_ASSERT(data);
  
   (*this_gtk_timeout->func)(this_gtk_timeout->closure);

   /* Need to get rid of this timer. */
   gtk_timeout_remove(this_gtk_timeout->timer_id);
   
   return FALSE; /* Lest be safe. */
}
