
#include <glib.h>

/* #define DEBUG_MALLOC */

void *GTKFE_ALLOC( int size )
{
  void *pointer = g_malloc( size );
#ifdef DEBUG_MALLOC
  g_print( "Allocating %d bytes at %p.\n", size, pointer );
#endif
  return pointer;
}

void *GTKFE_ALLOC_ZAP( int size )
{
  void *pointer = g_malloc0( size );
#ifdef DEBUG_MALLOC
  g_print( "Allocating %d bytes at %p.\n", size, pointer );
#endif
  return pointer;
}

void GTKFE_FREE( void *pointer )
{
#ifdef DEBUG_MALLOC
  g_print( "Freeing at %p.\n", pointer );
#endif
  g_free( pointer );
}

void *GTKFE_REALLOC( int *pointer1, int size )
{
  void *pointer2 = g_realloc( pointer1, size );
#ifdef DEBUG_MALLOC
  g_print( "Reallocing %p at %p with size %d.\n", pointer1, pointer2, size );
#endif
  return pointer2;
}

void *GTKFE_REALLOC_ZAP( int *pointer1, int size )
{
  void *pointer2 = g_malloc0( size );
#ifdef DEBUG_MALLOC
  g_print( "Reallocing %p at %p with size %d.\n", pointer1, pointer2, size );
#endif
  g_free( pointer1 );
  return pointer2;
}

