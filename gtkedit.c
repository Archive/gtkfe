/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public License
 * Version 1.0 (the "NPL"); you may not use this file except in
 * compliance with the NPL.  You may obtain a copy of the NPL at
 * http://www.mozilla.org/NPL/
 *
 * Software distributed under the NPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the NPL
 * for the specific language governing rights and limitations under the
 * NPL.
 *
 * The Initial Developer of this code under the NPL is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation.  All Rights
 * Reserved.
 */

/* 
   gtkedit.c --- gtk functions for fe
                  specific editor stuff.
*/

#include "xp_core.h"
#include "structs.h"
#include "ntypes.h"
#include "edttypes.h"
#include "edt.h"

void
FE_DisplayTextCaret(MWContext* context,
		    int loc,
		    LO_TextStruct* text_data,
		    int char_offset)
{
   XP_ASSERT(0);
}

void
FE_DisplayImageCaret(MWContext* context,
		     LO_ImageStruct* pImageData,
		     ED_CaretObjectPosition caretPos)
{
   XP_ASSERT(0);
}

void
FE_DisplayGenericCaret(MWContext* context,
		       LO_Any* pLoAny,
		       ED_CaretObjectPosition caretPos)
{
   XP_ASSERT(0);
}

Bool
FE_GetCaretPosition(MWContext* context,
		    LO_Position* where,
		    int32* caretX,
		    int32* caretYLow,
		    int32* caretYHigh)
{
   XP_ASSERT(0);
   return 0;
}

void
FE_DestroyCaret(MWContext* pContext)
{
   XP_ASSERT(0);
}

void
FE_ShowCaret(MWContext* pContext)
{
   XP_ASSERT(0);
}

void
FE_DocumentChanged(MWContext* context,
		   int32 iStartY,
		   int32 iHeight)
{
   XP_ASSERT(0);
}

MWContext*
FE_CreateNewEditWindow(MWContext* pContext,
		       URL_Struct* pURL)
{
   XP_ASSERT(0);
   return NULL;
}

char*
FE_URLToLocalName(char* url)
{
   XP_ASSERT(0);
   return NULL;
}

void
FE_EditorDocumentLoaded(MWContext* context)
{
   XP_ASSERT(0);
}

void
FE_GetDocAndWindowPosition(MWContext * context,
			   int32 *pX,
			   int32 *pY,
			   int32 *pWidth,
			   int32 *pHeight)
{
   XP_ASSERT(0);
}

void
FE_SetNewDocumentProperties(MWContext* context)
{
   XP_ASSERT(0);
}

Bool
FE_CheckAndSaveDocument(MWContext* context)
{
   XP_ASSERT(0);
   return 0;
}

Bool
FE_CheckAndAutoSaveDocument(MWContext *context)
{
   XP_ASSERT(0);
   return 0;
}

void 
FE_FinishedSave(MWContext* context,
		int status,
		char *pDestURL,
		int iFileNumber)
{
   XP_ASSERT(0);
}

char *
XP_BackupFileName (const char *url)
{
   XP_ASSERT(0);
   return 0;
}

Bool
XP_ConvertUrlToLocalFile (const char *url,
			  char **localName)
{
   XP_ASSERT(0);
   return 0;
}

void
FE_ImageLoadDialog(MWContext* context)
{
   XP_ASSERT(0);
}

void
FE_ImageLoadDialogDestroy(MWContext* context)
{
   XP_ASSERT(0);
}

void
FE_DisplayAddRowOrColBorder(MWContext * pMWContext,
			    XP_Rect *pRect,
			    XP_Bool bErase)
{
   XP_ASSERT(0);
}

void
FE_DisplayEntireTableOrCell(MWContext * pMWContext,
			    LO_Element * pLoElement)
{
   XP_ASSERT(0);
}
