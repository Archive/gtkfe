/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public License
 * Version 1.0 (the "NPL"); you may not use this file except in
 * compliance with the NPL.  You may obtain a copy of the NPL at
 * http://www.mozilla.org/NPL/
 *
 * Software distributed under the NPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the NPL
 * for the specific language governing rights and limitations under the
 * NPL.
 *
 * The Initial Developer of this code under the NPL is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation.  All Rights
 * Reserved.
 */

/*
   gtkfe.h --- gtk-specific headers for the front end.
   Created: Matt Wimer <matt@cgibuilder.com>

   This file is copylefted, details at: http://www.mozilla.org/NPL/
 */

/* 
   This file needs major help!
   We need the stuctures in this file to use only the pieces we use
   in the gtkfe. [MW]
  
 Modified:
   1998/04/06   Vidar Hokstad
      Removed lots of evil Motif junk... #if 0'ed out most of the
      fe_ContextData structure...
  
   1998/04/09   Vidar Hokstad
      Added fe_globalData (from XFE)
*/


#include <sys/errno.h>
#include "fe_proto.h"
#include "gtkmalloc.h"
#include <gtk/gtk.h>
#ifdef HAVE_GNOME
#include "gnome.h"
#endif

#include "structs.h"
#include "ntypes.h"
#include "xpassert.h"
#include "proto.h"

/*
 *    X11/Xlib.h "define"s Bool to be int. 
 *    So.. Undef it here, so that the XP type
 *    gets used...djw
 */
#ifdef Bool
#undef Bool
#endif

#ifndef Boolean
#define Boolean Bool
#endif

#define CONTEXT_FOR_WIDGET(widget) \
        gtk_object_get_data(GTK_OBJECT(widget), "MWContext")
#define EDITOR_CONTEXT_DATA(context)	((&CONTEXT_DATA(context)->editor))
#define CONTEXT_WIDGET(context)	((CONTEXT_DATA (context))->main)
#define CONTEXT_DATA(context)	((context)->fe.data)
#define MAILNEWS_CONTEXT_DATA(context) ((fe_ContextData *)CONTEXT_DATA(context))->mailnews_part
#define MSG_CONTEXT_DATA(context) ((fe_ContextData *)CONTEXT_DATA(context))->msg_part
#define ARTICLE_CONTEXT_DATA(context) ((fe_ContextData *)CONTEXT_DATA(context))->article_part
#define MAILTAB(context,tab) MAILNEWS_CONTEXT_DATA(context)->mailtabs[tab]
#define ACTIVE_MAILTAB(context) MAILTAB(context, MAILNEWS_CONTEXT_DATA(context)->active_tab)


/* Sanity check on the context.  Throughout the progress stuff below,
 * the context (and fe_data) needs to be checked for validity before
 * dereferencing the members.
 * 
 * (Yeah, I finally got around to stealing it from XFE [VH] )
 */
#define CHECK_CONTEXT_AND_DATA(c) \
((c) && CONTEXT_DATA(c) && !CONTEXT_DATA(context)->being_destroyed)


/* Platform-specific part of a compositor drawable, which consists of
   a drawing target, an XY offset and a clipping region. */
typedef struct fe_Drawable_struct
{
/* FIXME: Need something here ;) [VH] */
   
  GdkDrawable *gdkdrawable;    /* X11 drawable */

  int gdk_drawable_serial_num;   /* Serial number for offscreen pixmap */
  int32 x_origin;
  int32 y_origin;
  FE_Region clip_region;
} fe_Drawable; 

gint GTKFE_refresh_url_timer(gpointer data);

extern const char *fe_version;
extern const char *fe_progname_long;
extern const char *fe_progname;          

/*
 * Search Callback
 */

typedef void (*fe_searchFinishedFunction)(
                  MWContext* context
                  );
typedef void (*fe_searchOutlineChangeFunction)(
                  MWContext* context,
	          int index,
		  int32 num,
		  int totallines	
                  );




/*This structure seems to be owned by the FE mozilla is linked against. [MW]*/
typedef struct fe_ContextData
{

   GtkWidget *main;            /* The main window for this context */

   GtkWidget *progressbar;     /* What the name says */
   GtkWidget *statusbar;
   guint last_status_ctx, last_status_msg;
   
   GtkWidget *toolbar;		/* toolbar containing back, forward etc buttons */
#ifndef HAVE_GNOME
   GtkWidget *toolbarbox;         /* Vbox for toolbar, navibar and logo [TP]*/
   GtkWidget *toolnavibox; /* vbox for navibar + toolbar [TP] */
#endif
   
   GtkWidget *locationentry;     /* entry for asking location */
   GtkWidget *locationlabel;     /* text with word "location:" */
   GtkWidget *locationhbox;

   GtkWidget *separator;

   GtkWidget *logohbox;
   GtkWidget *logoimage;


   GdkBitmap *logo_mask;	/* all stuff for the gtk logo */
   GtkStyle *logo_style;
   GdkPixmap *logo_pixmap;

   GtkWidget *contenttable;    /* Table for scrolledwindow, statusbar and progressbar */

   
   /*We need to fix this hack up. [MW]*/
  

  void *view;  /* a pointer to the view associated with this context. */   

  /* are all the following unnecessary?? [TP] (toolbar is used) */

  GtkWidget *back_button;	/* Toolbar button to go back in history. */
  GtkWidget *forward_button;	/* Toolbar button to go forward in history. */
  GtkWidget *home_button;	/* Toolbar button to load home page. */

  GtkWidget *back_menuitem;	/* Menuitem to go back in history. */
  GtkWidget *forward_menuitem;	/* Menuitem to go foreward in history. */
  GtkWidget *home_menuitem;	/* Menuitem to load home page. */
  GtkWidget *delete_menuitem;	/* Menuitem to delete this window. */

  GtkWidget *back_popupitem;	/* Popup menuitem to go back in history. */
  GtkWidget *forward_popupitem;	/* Popup menuitem to go foreward in history. */

  GtkWidget *cut_menuitem, *copy_menuitem, *paste_menuitem,
    *paste_quoted_menuitem;
				/* Menuitems to cut/copy/paste. */

  GtkWidget *findAgain_menuitem;	/* Menuitems to findAgain. */
  GtkWidget *reloadFrame_menuitem;	/* Menuitems to reload selected Frame. */
  GtkWidget *frameSource_menuitem;	/* Menuitem to view source of selected Frame. */
  GtkWidget *frameInfo_menuitem;	/* Menuitem to view info of selected Frame. */

  GtkWidget *mailto_menuitem;	/* Menuitem to mail this URL to someone. */
  GtkWidget *saveAs_menuitem;	/* Menuitem to save this URL as */
  GtkWidget *uploadFile_menuitem;   /* Menuitem to upload a file to an FTP site */
  GtkWidget *print_menuitem;	/* Menuitem to print this URL. */
  GtkWidget *refresh_menuitem;	/* Menuitem to refresh this URL. */
  GtkWidget *print_button;		/* Toolbar button to print this URL. */

  GtkWidget *bookmark_menu;		/* Menu containing the bookmark entries. */
  GtkWidget *windows_menu;		/* Menu containing the list of windows. */
  GtkWidget *history_menu;		/* Menu containing the URL history. */
  GtkWidget *delayed_menuitem;	/* Menuitem to load in delayed images. */
  GtkWidget *delayed_button;	/* Toolbar button to load in delayed images. */
  GtkWidget *abort_menuitem;	/* Menuitem to abort downloads. */
  GtkWidget *abort_button;		/* Toolbar button to abort downloads. */
  GtkWidget *top_area;		/* Form containing the toolbar, current URL */
				/* info, directory buttons, and logo. */
  GtkWidget *menubar;		/* Menubar, containing all menu buttons. */
  GtkWidget *character_toolbar;	/* RowColumn containing editor char buttons. */
  GtkWidget *paragraph_toolbar;	/* RowColumn containing editor para buttons. */
  GtkWidget *dashboard;		/* Form containing the security/thermometer */
				/* info at bottom of window.*/
#ifdef LEDGES
  GtkWidget *top_ledge, *bottom_ledge; /* Half-implemented fixed areas that */
				  /* display other URLs. */
#endif
#ifndef NO_SECURITY
  GtkWidget *security_bar;		/* Drawing area for colored security bar. */
  GtkWidget *security_logo;		/* Label gadget containing security "key" */
				/* icon.*/
#endif
#ifdef JAVA
  GtkWidget *show_java;
#endif
  GtkWidget *scrolltable;       /* Table holding scrollbars and
                                   layout widget */
  GtkWidget *layout;            /* GtkLayout widget */
  GtkWidget *hscroll, *vscroll;	/* Scrollbars for the main drawining area. */

  GtkWidget *wholine;		/* Label at bottom of window displaying */
				/* info about current pointing URL, etc. */
  GtkWidget *bifficon;		/* Button indicating whether we have new
				   mail. */

  gint delete_event_close_id; /* This is used to coordinate the
                                 allow_close etc. stuff in Chrome*
                              */
  gint close_callback_id; /* ditto, but it's what the main body
                             of Netscape code wants */

  MSG_Pane* comppane;	/* If this is a composition, the libmsg data
			   structure. */

  int hysteresis;		/* "stickiness" of mouse-selection. */

  /* Widgets for the mail folder menu */
  GtkWidget *foldermenu;
  GtkWidget *move_selected_to_folder;
  GtkWidget *copy_selected_to_folder;
  gboolean doingMove;			/* move and copy share the same
					   foldermenu */
  /* Widgets for mail popup menus */
  GtkWidget *mailPopupBody;
  GtkWidget *mailPopupMessage;
  GtkWidget *mailPopupFolder;

  /* Widgets and things that appear only in MessageComposition windows: */
  GtkWidget *mcFrom;
  GtkWidget *mcReplyTo;
  GtkWidget *mcTo;
  GtkWidget *mcCc;
  GtkWidget *mcBcc;
  GtkWidget *mcFcc;
  GtkWidget *mcNewsgroups;
  GtkWidget *mcFollowupTo;
  GtkWidget *mcSubject;
  GtkWidget *mcAttachments;
  GtkWidget *mcBodyText;
  GtkWidget *deliverNow_menuitem;
  GtkWidget *deliverLater_menuitem;
  XP_Bool mcCitedAndUnedited;
  XP_Bool mcEdited;
  gboolean compose_wrap_lines_p;
  struct fe_mail_attach_data *mad;


  /* Pixels allocated per window. */
  GdkColor *color_data;
  int color_size;
  int color_fp;

  /* Dialogs of which there is only one per context. */
  GtkWidget *history_dialog, *file_dialog;
  struct fe_file_type_data *ftd;
  struct fe_source_data *sd;
  struct fe_docinfo_data *did;

  /* The dialog used when doing something synchronously per context,
     like downloading a file or delivering mail. */
  GtkWidget *synchronous_url_dialog;
  int synchronous_url_exit_status;

  /* XXX should we use a gdkpoint (with its 'guint16' x/y) here? */
  GdkPoint sb;		/* Width & height of the main scrollbars. */

  gulong fg_pixel;
  gulong bg_pixel;
  gulong top_shadow_pixel;
  gulong bottom_shadow_pixel;
  gulong text_bg_pixel;          /* enabled text background color */

  int bg_red;                           /* Nominal background color */
  int bg_green;
  int bg_blue;

  gboolean icon_colors_initialized;
  struct fe_colormap *colormap;

  GdkPixmap *backdrop_pixmap;

  int active_url_count;			/* Number of transfers in progress. */
  gboolean clicking_blocked;		/* Save me from myself! */
  gboolean save_next_mode_p;		/* "Save Next" prompt in progress */

  LO_Element *current_edge;		/* The grid edge being moved */
  gboolean focus_grid;			/* The grid with focus */
  int8 grid_scrolling;			/* Grid scrolling policy */

  fe_Drawable *drawable;                /* Target for drawing.  Either
                                           a window or offscreen pixmap */
  gboolean relayout_required;		/* Set when size change occurs before
					   document has been completely layed
					   out the first time. */
  const char *force_load_images;	/* Hack for "Load Images" command. */

  unsigned long line_height;
  time_t doc_size_last_update_time;	/* So FE_SetDocSize() can be lazy. */

  int16 xfe_doc_csid;

  gboolean bookmark_menu_up_to_date_p;
  gboolean windows_menu_up_to_date_p;

  int expose_x1;
  int expose_y1;
  int expose_x2;
  int expose_y2;
  gboolean held_expose;
  int expose_x_offset;
  int expose_y_offset;
  unsigned long expose_serial;

  /* Data used by thermo.c for status notification:
   */
  gint thermo_timer_id;		/* timer running the animation */
  time_t thermo_start_time;		/* when transfer started (requested) */
  time_t thermo_data_start_time;	/* when transfer REALLY started */
  time_t thermo_last_update_time;	/* time we last printed text message */

  gboolean thermo_size_unknown_count;	/* The number of transfers in progress
					   whose content-length is not known */
  int thermo_current;			/* total bytes-so-far */
  int thermo_total;			/* total content-length of all docs
					   whose sizes are known. */
  int thermo_lo_percent;		/* percent of layout complete */

  int thermo_cylon;			/* if !thermo_size_known_p, this is the
					   last pixel position of the cylon
					   thingy.  It's negative if it is in
					   motion backwards. */

  gboolean logo_animation_running;	/* logo animation running ?  */

  gint blink_timer_id;		/* timer for blinking (gag) */
  struct fe_blinker *blinkers;
  gboolean blinking_enabled_p;

  gboolean loading_images_p;     /* TRUE if images are loading. */
  gboolean looping_images_p;     /* TRUE if images are looping. */
  gboolean delayed_images_p;

  /* Data for that FE_SetRefreshURLTimer() repulsive kludge. */
  gint refresh_url_timer;
  guint32 refresh_url_timer_secs;
  char *refresh_url_timer_url;

  /* X Selection data
   */
  char *selection;
  char *clipboard;
  guint selection_time;
  guint clipboard_time;

  /* Things initialized from the resource database:
   */
  GdkColor link_pixel;
  GdkColor vlink_pixel;
  GdkColor alink_pixel;
  GdkColor select_fg_pixel;
  GdkColor select_bg_pixel;
  GdkColor default_fg_pixel;
  GdkColor default_bg_pixel;
#ifndef NO_SECURITY
  GdkColor secure_document_pixel;
  GdkColor insecure_document_pixel;
#endif

  char *default_background_image;

  /* XXX should these be char * or some other special type? */
  gchar *unedited_label_string;
  gchar *edited_label_string;
  gchar *netsite_label_string;

  gboolean confirm_exit_p;
  gboolean show_url_p;
  gboolean show_toolbar_p;
  gboolean show_toolbar_icons_p;
  gboolean show_toolbar_text_p;
  gboolean show_directory_buttons_p;
  gboolean show_menubar_p;
  gboolean show_bottom_status_bar_p;
  gboolean show_character_toolbar_p;
  gboolean show_paragraph_toolbar_p;
  gboolean autoload_images_p;
  gboolean fancy_ftp_p;
#ifndef NO_SECURITY
  gboolean show_security_bar_p;
#endif
#ifdef JAVA
  gboolean show_java_console_p;
#endif

  int progress_interval;	/* How quickly %-done text updates */
  int busy_blink_rate;		/* How quickly light blinks (microseconds) */

  GdkCursor *normal_cursor;
  GdkCursor *link_cursor;
  GdkCursor *busy_cursor;

  GdkCursor *save_next_link_cursor;
  GdkCursor *save_next_nonlink_cursor;
  GdkCursor *editable_text_cursor;     /* maybe others want to use this */

  GdkCursor *tab_sel_cursor;    /* new table cursors... */
  GdkCursor *row_sel_cursor;
  GdkCursor *col_sel_cursor;
  GdkCursor *cel_sel_cursor;
  GdkCursor *resize_col_cursor;
  GdkCursor *resize_tab_cursor;
  GdkCursor *add_col_cursor;
  GdkCursor *add_row_cursor;

  struct fe_bookmark_data* bmdata; /* Bookmark data (used only by hot.c) */
  struct fe_MailComposeContextData* mailcomposer;  /*mail compose data*/
  struct fe_addrbk_data* abdata; 
				   /* Address book data 
                                                 (used only by addrbk.c) */
  struct fe_search_data* searchdata; /* Search data (used by search.c) */
  struct fe_ldapsearch_data* ldapsearchdata; 
  struct fe_mailfilt_data* filtdata; /* Mail filter data (used only by 
					mailfilter.c) */

  fe_searchOutlineChangeFunction  searchOutlineChangeFunc;
  fe_searchFinishedFunction   	  searchFinishedFunc;
  gboolean hasCustomChrome;
  Chrome chrome;

  /* a handle to the current pref dialog being displayed. this
     is useful if you need to create a modal html dialog as it's
     child, as in the case of the security prefs.
     */
  GtkWidget *currentPrefDialog;
  struct fe_prefs_data *fep;

  /* If this context is a full page plugin, then this flag gets set.
     Because of this, resize events are passed on to the plugin.
  */
  gboolean is_fullpage_plugin;

  gboolean being_destroyed;

  /* If dont_free_context_memory  is non-zero on the context,
   * fe_DestroyContext() will not release the
   * memory for the context although it will go ahead and do all other
   * destroy stuff like destroying the widgets etc. Just the memory for
   * both the context and the context-data is retained.
   *
   * delete_response is used to store the XmNdeleteResponse of the context
   * while the context is in a protected state. During protected state,
   * the context's XmNdeleteResponse will be forced to XmDO_NOTHING, indicating
   * that the user cannot destroy this context using the window manager
   * delete menu item. Once the context is completely unprotected, the
   * XmNdeleteResponse will be restored to the stored delete_response value.
   *
   *********** DO NOT ACCESS THESE VARAIBLES DIRECTLY.
   * WARNING * Use this via fe_ProtectContext(), fe_UnProtectContext() and
   *********** fe_IsContextProtected().
   */
  int dont_free_context_memory;
  unsigned char delete_response;

  /* If a context is destroyed and dont_free_context_memory is set,
   * then this will be set indicating that the context was destroyed but
   * the context and context-data memory was not reclaimed. It will be the
   * responsibility of the module that set dont_free_context_memory to
   * reclaim the context memory if this is set on the context.
   *
   * The situation this is happen is
   * 	1. when a synchronous_url_dialog is up
   *	2. when a popup on a say frame-context is up and before we can
   *	   bring the popup down, the frame-context gets destroyed with a
   *	   JavaScript timer.
   */
  gboolean destroyed;

#if 0
  fe_FindData *find_data;

,  fe_MailNewsContextData *mailnews_part;
  fe_MsgWindowContextData *msg_part;
  fe_ArticleWindowContextData *article_part;
   
  fe_EditorContextData editor;     /* editor stuff */
  char *save_file_url;
  int  file_save_status;
  XP_Bool is_file_saving;

  fe_DependentList* dependents;    /* dependency list */
  GtkWidget            *posted_msg_box;
  XP_Bool are_scrollbars_active;
  XP_Bool stealth_cmd;
#endif

  XP_Bool is_resizing; /* Don't change URLbar for a resize. */
                       /* You may have needed more typing room. */

  struct {
    int32 x,y;
  } cachedPos;
   
} fe_ContextData;







typedef struct
{
  /* Stuff from the resource database that is not per-window. */

#if 0   
  XtIntervalId save_history_id;		/* timer for periodically saving
					   the history and bookmark files. */
#endif
   int save_history_interval;		/* Every N seconds. */

#if 0   
   Visual *default_visual;
#endif   
   struct fe_colormap *common_colormap; /* If private colormap, colormap for
                                          "simple" contexts, else common
                                          colormap for all contexts. */

  struct fe_colormap *default_colormap; /* fe wrapper around X default
                                           colormap */

  Boolean always_install_cmap;
#ifdef USE_NONSHARED_COLORMAPS
  Boolean windows_share_cmap;
#endif
  Boolean force_mono_p;
  char*   wm_icon_policy;

  Boolean document_beats_user_p;	/* #### move to prefs */
  const char *language_region_list;	/* #### move to prefs */
  const char *invalid_lang_tag_format_msg;/* #### move to prefs */
  const char *invalid_lang_tag_format_dialog_title;/* #### move to prefs */

#ifdef __sgi
  Boolean sgi_mode_p;
#endif /* __sgi */

  int max_image_colors;		/* Max color cells to gobble up. */

  int fe_guffaw_scroll;		/* Brokenness of server WindowGravity */

  const char *user_prefs_file;

  Boolean stderr_dialog_p;
  Boolean stdout_dialog_p;

  const char *encoding_filters;
#if 0
  XtTranslations terminal_text_translations;
  XtTranslations nonterminal_text_translations;
  XtTranslations global_translations;
  XtTranslations global_text_field_translations;
  XtTranslations global_nontext_translations;

  XtTranslations editing_translations;
  XtTranslations single_line_editing_translations;
  XtTranslations multi_line_editing_translations;
  XtTranslations form_elem_editing_translations;

  XtTranslations browser_global_translations;
  XtTranslations bm_global_translations;
  XtTranslations ab_global_translations;
  XtTranslations gh_global_translations;
  XtTranslations mailnews_global_translations;
  XtTranslations mnsearch_global_translations;
  XtTranslations messagewin_global_translations;
  XtTranslations mailcompose_global_translations;
  XtTranslations address_outliner_traverse_translations;
  XtTranslations address_outliner_key_translations;
  XtTranslations dialog_global_translations;
  XtTranslations editor_global_translations;
#endif
   
  /* Random error messages and things.
   */
  const char *options_saved_message;
  const char *click_to_save_message;
  const char *click_to_save_cancelled_message;
  const char *no_url_loaded_message;
  const char *no_next_url_message;
  const char *no_previous_url_message;
  const char *no_home_url_message;
  const char *not_over_image_message;
  const char *not_over_link_message;
  const char *no_search_string_message;
  const char *wrap_search_message;
  const char *wrap_search_backward_message;
  const char *wrap_search_not_found_message;
  const char *no_addresses_message;
  const char *no_file_message;
  const char *no_print_command_message;
  const char *bookmarks_changed_message;
  const char *bookmark_conflict_message;
  const char *bookmarks_no_forms_message;
  const char *create_cache_dir_message;
  const char *created_cache_dir_message;
  const char *cache_not_dir_message;
  const char *cache_suffix_message;
  const char *cube_too_small_message;
  const char *really_quit_message;
  const char *double_inclusion_message;
  const char *expire_now_message;
  const char *clear_mem_cache_message;
  const char *clear_disk_cache_message;
  const char *rename_files_message;
  const char *overwrite_file_message;
  const char *unsent_mail_message;
  const char *binary_document_message;
  const char *empty_message_message;
  const char *default_mailto_text;
  const char *default_url_charset;
  const char *helper_app_delete_message; /* For Helper App Delete */

  /*
   * Enterprise Kit stuff
   */
  #define MAX_DIRECTORY_BUTTONS    6
  #define MAX_DIRECTORY_MENU_ITEMS 25
  #define MAX_HELP_MENU_ITEMS      25

  char* homePage;
  int numDirectoryButtons;
  int numDirectoryMenuItems;
  int numHelpMenuItems;
  char* directoryButtonUrls[MAX_DIRECTORY_BUTTONS];
  char* directoryMenuUrls[MAX_DIRECTORY_MENU_ITEMS];
  char* helpMenuUrls[MAX_HELP_MENU_ITEMS];

#ifdef NSPR_SPLASH
  /* this is not the normal splash screen shown in the html window.  It is
     the new, nspr-threads-and-X
     splash screen. */
  Boolean show_splash;
#endif
 
  /* Startup component flags */
#ifdef MOZ_TASKBAR
  Boolean startup_component_bar;
#endif
#ifdef MOZ_MAIL_NEWS
  Boolean startup_composer;
  Boolean startup_mail;
  Boolean startup_news;
#endif

  Boolean startup_history;
  Boolean startup_bookmarks;
  Boolean startup_nethelp;
  Boolean startup_netcaster;

  /* Session Management on/off */
  Boolean session_management;

  /* IRIX Session Management */
  Boolean irix_session_management;

  /* Dont do about:splash on startup */
  Boolean startup_no_about_splash;

  /* Dont force window stacking (ie chrome topmost & bottommost) */
  Boolean dont_force_window_stacking;

  /* Startup iconic */
  Boolean startup_iconic;

#if 0   
  /* Startup geometry */
  String startup_geometry;
#endif
   
  /* Dont save geometry prefs */
  Boolean dont_save_geom_prefs;

  /* Ignore geometry prefs */
  Boolean ignore_geom_prefs;

  /*
   * Enterprise Kit Proxy information is put in fe_globalPrefs
   */

  /* More global data that aren't resource related */
  time_t privateMimetypeFileModifiedTime;
  time_t privateMailcapFileModifiedTime;

  Boolean editor_im_input_enabled;

  /* We need to keep track of this for the Global History. */
  /* If the databases are locked, we need to disable the window. */
  Boolean all_databases_locked;

#if 0
  Cardinal editor_update_delay;
#endif
   
} fe_GlobalData;

extern fe_GlobalData   fe_globalData;


/* gtkfe_* -methods */
MWContext *gtkfe_GetContext(GtkWidget *widget);
fe_ContextData *gtkfe_GetContextData(GtkWidget *widget);
gboolean gtkfe_QuitCallback (GtkWidget *widget, void *data);

struct gtkfe_SigRelayInfo {
  void (* callback)(void *arg);
  void *arg;
};

gboolean gtkfe_callback_relay(GtkObject *object,
                              struct gtkfe_SigRelayInfo *data,
                              guint n_args,
                              GtkArg *args);

MWContext* GTKFE_CreateMWContext(void);

typedef GdkRegion **GTKFE_Region;
#define GDK_REGION(reg) (*((GdkRegion **)(reg)))

MWContext *GTKFE_MakeNewWindow(URL_Struct *url,
                               char       *window_name,
                               Chrome     *chrome,
                               MWContext  *parent);
MWContext *GTKFE_CreateMWContext(void);

void gtkfe_OpenPageDialog(MWContext *context, char *defaulttext);
