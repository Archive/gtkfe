/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public License
 * Version 1.0 (the "NPL"); you may not use this file except in
 * compliance with the NPL.  You may obtain a copy of the NPL at
 * http://www.mozilla.org/NPL/
 *
 * Software distributed under the NPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the NPL
 * for the specific language governing rights and limitations under the
 * NPL.
 *
 * The Initial Developer of this code under the NPL is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation.  All Rights
 * Reserved.
 */
/*
 * Modified:
 *       1998/04/06    Matt Wilmer
 *           Created.
 * 
 */



#include "gtkfe.h"
#include "structs.h"
#include "ntypes.h"
#include "xpassert.h"
#include "proto.h"
#include "fe_proto.h"

#include "gtkform.h"
#include "gtk/gtk.h"
#include "glib.h"

/*Our very own header file. */

/*FD_SETSIZE needs to be the max number of file descriptors gtk_input_add can 
setup for us. [MW]*/

/* FD_SETSIZE should be defined in <sys/select.h>... Only define it if we don't
 * get it for free.. [VH] */

#ifndef FD_SETSIZE
#define FD_SETSIZE 256
#endif

#define countof(x) (sizeof(x) / sizeof (*x))

static gint GTKFE_fds_to_gtkio_ids [FD_SETSIZE] = { 0, };

static void GTKFE_stream_callback (gpointer data, gint source, 
				   GdkInputCondition condition);




/* Where are these defined? Maybe I need to get newer versions of some lib...
 * Define a dummy version if they're not found... [VH] */

#ifndef LOCK_FDSET
#define LOCK_FDSET()
#endif
#ifndef UNLOCK_FDSET
#define UNLOCK_FDSET()
#endif

static void
GTKFE_add_input (int fd, int mask)
{
  if (fd < 0 || fd >= countof (GTKFE_fds_to_gtkio_ids))
    abort (); /*Maybe we should do a return here[MW]*/

   LOCK_FDSET();
   
#if defined(UNIX_ASYNC_DNS) && !defined(HAVE_GNOME)
  if ( fe_UseAsyncDNS() && GTKFE_fds_to_gtkio_ids [fd]) {
          /* If we're already selecting input on this fd, don't select it again
                 and lose our pointer to the XtInput object..  This shouldn't happen,
                 but netlib does this when async DNS lookups are happening.  (This
                 will lose if the `mask' arg has changed on two consecutive calls
                 without an intervening call to `fe_remove_input', but that doesn't
                 happen.)  -- jwz, 9-Jan-97.
                 */
          goto DONE;
  }
#endif

  GTKFE_fds_to_gtkio_ids[fd] = 
    gdk_input_add (fd,
		   mask,
		   GTKFE_stream_callback,
		   NULL);

#ifdef JAVA
  if (PR_CurrentThread() != mozilla_thread) {
      /*
      ** Sometimes a non-mozilla thread will be using the netlib to fetch
      ** data. Because mozilla is the only thread that runs the netlib
      ** "select" code, we need to be able to kick mozilla and wake it up
      ** when the select set has changed.
      **
      ** A way to do this would be to have mozilla stop calling select
      ** and instead manually manipulate the idle' thread's select set,
      ** but there is yet to be an NSPR interface at that level.
      */
      PR_PostEvent(mozilla_event_queue, NULL);
  }
#endif /* JAVA */

   UNLOCK_FDSET();
}

static void
GTKFE_remove_input (int fd)
{
  if (fd < 0 || fd >= countof (GTKFE_fds_to_gtkio_ids))
    return;     /* was abort() --  */

  LOCK_FDSET();

  if (GTKFE_fds_to_gtkio_ids [fd] != 0) {
    gdk_input_remove (GTKFE_fds_to_gtkio_ids [fd]);
    GTKFE_fds_to_gtkio_ids [fd] = 0;
  }


  UNLOCK_FDSET();
}




void
FE_SetReadSelect (MWContext *context, int fd)
{
  GTKFE_add_input (fd, GDK_INPUT_READ);
}

void
FE_SetConnectSelect (MWContext *context, int fd)
{
  GTKFE_add_input (fd, GDK_INPUT_WRITE);
}

void
FE_ClearReadSelect (MWContext *context, int fd)
{
  GTKFE_remove_input (fd);
}

void
FE_ClearConnectSelect (MWContext *context, int fd)
{
  FE_ClearReadSelect (context, fd);
}

void
FE_ClearFileReadSelect (MWContext *context, int fd)
{
  FE_ClearReadSelect (context, fd);
}

void
FE_SetFileReadSelect (MWContext *context, int fd)
{ 
  FE_SetReadSelect (context, fd);
}


static void 
GTKFE_stream_callback (gpointer data, gint source, GdkInputCondition condition)
{
#ifdef QUANTIFY
  quantify_start_recording_data();
#endif /* QUANTIFY */

#ifdef NSPR20_DISABLED
  NET_ProcessNet (source, NET_UNKNOWN_FD);
#endif 

#ifdef QUANTIFY
  quantify_stop_recording_data();
#endif /* QUANTIFY */
}
