/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public License
 * Version 1.0 (the "NPL"); you may not use this file except in
 * compliance with the NPL.  You may obtain a copy of the NPL at
 * http://www.mozilla.org/NPL/
 *
 * Software distributed under the NPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the NPL
 * for the specific language governing rights and limitations under the
 * NPL.
 *
 * The Initial Developer of this code under the NPL is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation.  All Rights
 * Reserved.
 */

/* 
   gtkctxt.c --- gtk fe handling of MWContext initialization.

  Modified:
   1998/04/02    Vidar Hokstad <vidarh@telepost.net>
     Added some reasonable temporary return values to several functions,
     and some XP_ASSERT(0)'s, to track calls to unimplemented functions.

   1998/04/05    matt wimer <matt@cgibuilder.com>
     Added the inclusion of a gtkfe.h.  
     Built GTKFE_refresh_url_timer.
     Moved FE_SetRefreshURLTimer to gtk.

   1998/04/06   Vidar Hokstad <vidarh@ncg.net>
     Added some argument checking, and removed a couple of XP_ASSERT(0)'s
     from the functions Matt has started implementing. Fixed lack of
     return value in one of the timer functions.

   1998/04/07   Vidar Hokstad <vidarh@ncg.net>
     Started implementing FE_GetURL(). Taken lots from qtfe. Identified
     several high priority functions (search for "FIXME").

   1998/04/08   Vidar Hokstad <vidarh@ncg.net>
     First stab at implementing the url_exit() callback. Partly
     implemented GTKFE_GetTextInfo() (it's a wonder it doesn't crash).
  
     Implemented most of the progressbar stuff... Doesn't work perfectly yet
     though. If you want to hack on it, be aware that it differentiates between
     "Cylon" mode (when the total size of the doc. is unknown), and "Thermometer"
     mode (when the size of the doc. is known).
 
   1998/04/09  Vidar Hokstad <vidarh@ncg.net>
     I don't have the faintest idea about how many FIXME's I just blasted away
     just by finally defining the CHECK_CONTEXT_AND_DATA() macro... :)
     Also implemented partts of GTKFE_LayoutNewDocument(), and fixed SetDocumentTitle.

   1998/04/10 Larry Ewing <lewing@isc.tamu.edu>
     filled out some of the enable/disable sensitivity funtions.
*/

#include "structs.h"
#include "ntypes.h"
#include "xpassert.h"
#include "proto.h"
#include "fe_proto.h"
#include "merrors.h"
#include "prefapi.h"
#include "net.h"
#include "shist.h"
#include "xp_thrmo.h"
#include "np.h"
#include "layers.h"
#include "gtkform.h"
#include <gdk/gdkx.h>
#include "gtk/gtk.h"
#include "glib.h"
#include "gtklayout.h"

/*Our very own header file. */
#include "gtkfe.h"
#include "fonts.h"

#ifdef DO_CHECKMEM
#include "checkmem.h"
#else
#define DMC()
#endif

/*
 * Utility functions
 */

#define LO_COLOR_TO_GDK_COLOR(locolor, gdkcolor) \
(gdkcolor).red = (locolor)->red; \
(gdkcolor).green = (locolor)->green; \
(gdkcolor).blue = (locolor)->blue; \
gdk_imlib_best_color_get(&(gdkcolor));


GdkGC *fe_GetGCfromDW (fe_Drawable *fe_drawable, GdkGCValuesMask flags, GdkGCValues *gcv);

static void
SetURLString(MWContext * context, URL_Struct *url)
{
    GtkWidget *entry = CONTEXT_DATA(context)->locationentry;
#ifdef HAVE_GNOME
    entry = GTK_COMBO(entry)->entry;
#endif
    gtk_entry_set_text(GTK_ENTRY(entry), (gchar *)url->address);
}

static void
UpdateStatusText(MWContext * context,
                 char * text)
{
   char *message;
   fe_ContextData *cd;

   if (!CHECK_CONTEXT_AND_DATA(context)) return;

   cd = CONTEXT_DATA(context);
   
   message = text;
   if (message == 0 || *message == '\0') {
      message = context->defaultStatus;
   }

   if(cd->last_status_ctx == -1)
     cd->last_status_ctx = gtk_statusbar_get_context_id(GTK_STATUSBAR(cd->statusbar),
                                                        "statusmsgs");
   else
     gtk_statusbar_remove(GTK_STATUSBAR(cd->statusbar), cd->last_status_ctx,
                          cd->last_status_msg);
   cd->last_status_msg = gtk_statusbar_push(GTK_STATUSBAR(cd->statusbar),
                                            cd->last_status_ctx, message);
}  

static void
UpdatePercent(MWContext * context,int percent, int32 content_length)
{
  if (!CHECK_CONTEXT_AND_DATA(context)) return;
  if (!CONTEXT_DATA(context)->progressbar) return;
  DMC();
  if(percent <= 0)
    {
      if(GTK_PROGRESS_BAR
         (CONTEXT_DATA(context)->progressbar)->percentage > 1)
        {
          GTK_PROGRESS_BAR
            (CONTEXT_DATA(context)->progressbar)->percentage = 0;
        }
      gtk_progress_bar_update
        (GTK_PROGRESS_BAR
         (CONTEXT_DATA(context)->progressbar),
         GTK_PROGRESS_BAR
         (CONTEXT_DATA(context)->progressbar)->percentage+.01);
    }
  else
    {
      gtk_progress_bar_update
        (GTK_PROGRESS_BAR(CONTEXT_DATA(context)->progressbar),
         percent);
    }
}

#if 0
static void
UpdateCylon(MWContext * context)
{
   if (!CHECK_CONTEXT_AND_DATA(context)) return;
   if (!CONTEXT_DATA(context)->progressbar) return;

   /* This is supposed to update the progressbar with a preset number of pixels... We just add one for now.
    */
#if 0   
	// Notify the frame to tick cylon mode
	frame->notifyInterested(XFE_Frame::progressBarCylonTick);
#endif
   gtk_progress_bar_update
     (GTK_PROGRESS_BAR(CONTEXT_DATA(context)->progressbar),
      GTK_PROGRESS_BAR (CONTEXT_DATA(context)->progressbar)->percentage + .1);
}
#endif

static void GTKFE_Progress(MWContext *context, const char *message);

/* Redraw the graph.  If text_too_p, then regenerate the textual message
   and/or move the cylon one tick if appropriate.  (We don't want to do
   this every time FE_GraphProgress() is called, just once a second or so.)
 */
static void 
UpdateGraph(MWContext * context,XP_Bool text_too_p, int32 content_length)
{
   double ratio_done;
   time_t now;
   int total_bytes, bytes_received;
   XP_Bool size_known_p;

   if (!CHECK_CONTEXT_AND_DATA(context)) return;
   
   now = time ((time_t *) 0);
	total_bytes     = CONTEXT_DATA (context)->thermo_total;
   bytes_received  = CONTEXT_DATA (context)->thermo_current;
   size_known_p    = CONTEXT_DATA (context)->thermo_size_unknown_count <= 0;

   if (size_known_p && total_bytes > 0 && bytes_received > total_bytes)
    {
#if 0
       /* Netlib doesn't take into account the size of the headers, so this
	can actually go a bit over 100% (not by much, though.)  Prevent
	the user from seeing this bug...   But DO print a warning if we're
	way, way over the limit - more than 1k probably means the server
	is messed up. */
       if (bytes_received > total_bytes + 1024)
	 fprintf (stderr, "%s: received %d bytes but only expected %d??\n",
		  fe_progname, bytes_received, total_bytes);
#endif
       bytes_received = total_bytes;
    }
   
   ratio_done = (size_known_p && total_bytes > 0
		 ? (((double) bytes_received) / ((double) total_bytes))
		 : 0);
   
#ifdef DEBUG_PROGRESS
   printf(" UpdateGraph: %d/%d=%f (%d/%d) %s  -or-  %d%%\n",
	  bytes_received, total_bytes,
	  ratio_done,
	  CONTEXT_DATA (context)->thermo_size_unknown_count,
	  CONTEXT_DATA (context)->active_url_count,
	  text_too_p ? "text" : "notext",
	  CONTEXT_DATA (context)->thermo_lo_percent);
#endif
   
   /* Update the thermo each time we're called. */
   
   if (CONTEXT_DATA (context)->thermo_lo_percent >= 0)
     {
	int percent = CONTEXT_DATA(context)->thermo_lo_percent;
	
	/* Update the progress percent */
	UpdatePercent(context,percent, content_length);
     }
   /* This is rather unneeded. [MW]
   else
     {
	UpdateCylon(context);
     }
   */

   
   /* Only update text if a second or more has elapsed since last time.
      Unlike the cylon, which we update each time we are called with
      text_too_p == True (which is 4x a second or so.)
   */
   if (text_too_p && now >= (CONTEXT_DATA (context)->thermo_last_update_time +
			     CONTEXT_DATA (context)->progress_interval))
     {
	const char *msg = XP_ProgressText ((size_known_p ? total_bytes : 0),
                          bytes_received, 
                          CONTEXT_DATA (context)->thermo_start_time,now);
	
	CONTEXT_DATA (context)->thermo_last_update_time = now;
	
	if (msg && *msg)
	  GTKFE_Progress (context, msg);
	
#ifdef DEBUG_PROGRESS
	fprintf (stderr, "====== %s\n", (msg ? msg : ""));
#endif
     }
}

/* Start blinking the light and drawing the thermometer.
   This is done before the first call to FE_GraphProgressInit()
   to make sure that we indicate that we are busy before the
   first connection has been established.
 */
static void
StartProgressGraph(MWContext * context)
{
   time_t now = time ((time_t *) 0);

   if (!CHECK_CONTEXT_AND_DATA(context)) return;

   /* FIXME [VH] */
#if 0
   fe_frameNotifyLogoStartAnimation(context);
#endif
   CONTEXT_DATA (context)->thermo_start_time = now;
   CONTEXT_DATA (context)->thermo_last_update_time = now;
   CONTEXT_DATA (context)->thermo_data_start_time = 0;
   
   CONTEXT_DATA (context)->thermo_size_unknown_count = 0;
   CONTEXT_DATA (context)->thermo_total = 0;
   CONTEXT_DATA (context)->thermo_current = 0;
   CONTEXT_DATA (context)->thermo_lo_percent = 0;
   
   if (CONTEXT_DATA (context)->thermo_timer_id != -1) {
     /* FIXME [VH] */
   }
}

static void
fe_StopProgressGraph(MWContext * context)
{
  if(CONTEXT_DATA(context)->thermo_timer_id != -1)
    {
      gtk_timeout_remove(CONTEXT_DATA(context)->thermo_timer_id);
    }
}

/* Recurses over children of context, returning True if any context is
   stoppable. */
static Boolean
IsContextStoppableRecursive(MWContext *context)
{
    int i = 1;
    MWContext *child;
    
    if (!context)
        return False;
    
    if ((CONTEXT_DATA(context)->loading_images_p &&
		 CONTEXT_DATA(context)->autoload_images_p) ||
        CONTEXT_DATA(context)->looping_images_p) 
	  return True;

    while ((child = (MWContext*)XP_ListGetObjectNum (context->grid_children,
													 i++)))
        if (IsContextStoppableRecursive(child))
            return True;
    
    return False;
}


/* Returns True if this is a context whose activity can be stopped. */
static Boolean
IsContextStoppable(MWContext *context)
{
      /* XP_IsContextStoppable checks for mocha threads, too. */
      return (IsContextStoppableRecursive(context) ||
              XP_IsContextStoppable(context)); 
}

/*
 * MWContext functions.
 */

/* Stubs just to get it to compile */
static void
GTKFE_CreateEmbedWindow(MWContext *context, NPEmbeddedApp *app)
{
}

static void
GTKFE_SaveEmbedWindow(MWContext *context, NPEmbeddedApp *app)
{
}

static void
GTKFE_RestoreEmbedWindow(MWContext *context, NPEmbeddedApp *app)
{
}

static void
GTKFE_DestroyEmbedWindow(MWContext *context, NPEmbeddedApp *app)
{
}

static MWContext*
GTKFE_CreateNewDocWindow(MWContext *calling_context,
			  URL_Struct *URL)
{
  if (calling_context) 
    {
      GtkWidget *widget = CONTEXT_WIDGET (calling_context);
		
      if (widget)
        {
#if 0          
          /* XXX TODO */
          GtkWidget *app_shell = gtk_widget_get_toplevel(widget);

          return GTKFE_showBrowser(app_shell, URL);
#else
          g_print("GTKFE_showBrowser to %s\n",
                  URL->address);
#endif
        }
    }/* if */

  return NULL;
}

static void 
GTKFE_SetDocTitle (MWContext * context,
		    char * title)
{
   gtk_window_set_title(GTK_WINDOW(CONTEXT_WIDGET(context)),
                        title);
}

static void 
GTKFE_FinishedLayout (MWContext *context)
{
  /* Copied from XFE */
  /* Since our processing of XFE_SetDocDimension() may have been lazy,
     do it for real this time. */
  CONTEXT_DATA (context)->doc_size_last_update_time = 0;
}

static char* 
GTKFE_TranslateISOText (MWContext * context,
			 int charset,
			 char *ISO_Text)
{
   return ISO_Text;
}

/*
 * more ugly hackery!
 * we need to wait for gtk+ 1.1.x for a clean API to get [lr]bearing.
 * [shaver]
 */
#include <gdk/gdkx.h>

static int 
GTKFE_GetTextInfo (MWContext * context,
		    LO_TextStruct *text,
		    LO_TextInfo *text_info)
{
   /* FIXME -- Get's called now... Time to implement it [VH]
    * Copied it from XFE and started working on it.
    */

   
  GdkFont * font;
  char *str = (char *) text->text;
  int length = text->text_len;
  int remaining = length;

  font = fe_LoadFontFromFace (context, text->text_attr,
			      &text->text_attr->charset,
			      text->text_attr->font_face,
			      text->text_attr->size,
			      text->text_attr->fontmask);
   
  /* X is such a winner, it uses 16 bit quantities to represent all pixel
     widths.  This is really swell, because it means that if you've got
     a large font, you can't correctly compute the size of strings which
     are only a few thousand characters long.  So, when the string is more
     than N characters long, we divide up our calls to XTextExtents to
     keep the size down so that the library doesn't run out of fingers
     and toes.
   */
#define SUCKY_X_MAX_LENGTH 600

  text_info->ascent = 14;
  text_info->descent = 3;
  text_info->max_width = 0;
  text_info->lbearing = 0;
  text_info->rbearing = 0;
  if (!font) {
      fprintf(stderr, "couldn't find font for face %s! bailing!\n",
             text->text_attr->font_face);
      return 0;
  }

  do
    {
      int L = (remaining > SUCKY_X_MAX_LENGTH ? SUCKY_X_MAX_LENGTH :
	       remaining);
      int ascent, descent;
      int direction;
      gint strwidth;
      XFontStruct *fontStruct = (XFontStruct *)GDK_FONT_XFONT(font);
      XCharStruct overall;

      XTextExtents(fontStruct, str, L, &direction, &ascent, &descent,
                   &overall);

#if 0
       /* FIXME: This is one hell of a nasty macro defined in fonts.h [VH] */
       FE_TEXT_EXTENTS (text->text_attr->charset, font, str, L,
		    &ascent, &descent, &overall);
#endif
       /* ascent and descent are per the font, not per this text. */
       text_info->ascent = font->ascent;
       text_info->descent = font->descent;
       strwidth = gdk_string_width(font, (gchar *)str);

       text_info->max_width += strwidth;
       
#define FOO(x,y) if (y > x) x = y
      FOO (text_info->lbearing,   overall.lbearing);
      FOO (text_info->rbearing,   overall.rbearing);
      /*
       * If font metrics were set right, overall.descent should never exceed
       * descent, but since there are broken fonts in the world.
       */
      FOO (text_info->descent,   overall.descent);
#undef FOO

      str += L;
      remaining -= L;
    }
  while (remaining > 0);

  /* What is the return value expected to be?
     layout/layout.c doesn't seem to use it. */
  
  /*
  fprintf(stderr, "context->type == %i\n", context->type);
  */
  
  return 0;
}

static void 
GTKFE_GetEmbedSize (MWContext * context,
                    LO_EmbedStruct *embed_struct,
                    NET_ReloadMethod force_reload)
{
  LO_CommonPluginStruct *embed_struct_super = (LO_CommonPluginStruct *) embed_struct;
  /* Copied from XFE */
  NPEmbeddedApp *eApp = (NPEmbeddedApp *)embed_struct_super->FE_Data;
  int32 doc_id;

    /* here we need only decrement the number of embeds expected to load */
  doc_id = XP_DOCID(context);

  if(!eApp)
    {
      /* Determine if this is a fullpage plugin. Do this _now_ so
         that it'll be available when NPL_EmbedCreate() calls back
         to XFE_CreateEmbedWindow() */
      if((embed_struct_super->width == 1) &&
         (embed_struct_super->height == 1) &&
         (embed_struct->attribute_cnt > 0) &&
         (!strcmp(embed_struct->attribute_list[0], "src")) &&
         (!strcmp(embed_struct->value_list[0], "internal-external-plugin"))) {
        CONTEXT_DATA(context)->is_fullpage_plugin = 1;
      }

      /* attempt to make a plugin */
#ifdef UNIX_EMBED
      if(!(eApp = NPL_EmbedCreate(context, embed_struct)))
#else
	if(1)  /* disable unix plugin's */
#endif
          {
	    /* hmm, that failed which is unusual */
	    embed_struct_super->width = embed_struct_super->height=1;
	    return;
          }
      eApp->type = NP_Plugin;

      if (embed_struct_super->ele_attrmask & LO_ELE_HIDDEN) {
        /* Hidden plugin. Dont create window for it. */
        eApp->fe_data = 0;
        eApp->wdata = 0;
        embed_struct_super->width = embed_struct_super->height=0;
        /* --- begin fix for bug# 35087 --- */
        embed_struct_super->FE_Data = (void *)eApp;

        if (NPL_EmbedStart(context, embed_struct, eApp) != NPERR_NO_ERROR) {
          /* Spoil sport! */
          /* XXX This used to be a call to fe_destroyEmbed,
             which has now been massaged into a front-end
             callback. However, it doesn't (and didn't!) _do_
             anything unless eApp->fe_data or eApp->wdata
             contain something, and we've just hard-coded them
             to zero!

             XFE_DestroyEmbedWindow(context, eApp) */
          embed_struct_super->FE_Data = NULL;
          return;
        }
        /* --- end fix for bug# 35087 --- */

        /* XXX NPL_EmbedSize does nothing if eApp->wdata == NULL;
           makes sense because this thing is _hidden_.

           (void)NPL_EmbedSize(eApp); */
        return;
      }

      if (NPL_EmbedStart(context, embed_struct, eApp) != NPERR_NO_ERROR) {
        /* Spoil sport! */
        GTKFE_DestroyEmbedWindow(context, eApp);
        embed_struct_super->FE_Data = NULL;
        return;
      }
    }

  /* always inform plugins of size changes */
  (void)NPL_EmbedSize(eApp);
}

static void 
GTKFE_GetJavaAppSize (MWContext * context,
                      LO_JavaAppStruct *java_struct,
                      NET_ReloadMethod force_reload)
{
   XP_ASSERT(0);
}

static void 
GTKFE_FreeEmbedElement (MWContext *context,
			 LO_EmbedStruct *embed)
{
   XP_ASSERT(0);
}

static void 
GTKFE_FreeJavaAppElement (MWContext *context,
			   struct LJAppletData *appletData)
{
   XP_ASSERT(0);
}

static void 
GTKFE_HideJavaAppElement (MWContext *context,
			   struct LJAppletData *java_app)
{
   XP_ASSERT(0);
}

static void 
GTKFE_FreeEdgeElement (MWContext *context,
			LO_EdgeStruct *edge)
{
   XP_ASSERT(0);
}

static void 
GTKFE_FormTextIsSubmit (MWContext * context,
                        LO_FormElementStruct * form_element)
{
   XP_ASSERT(0);
}


#define FE_NORMALIZE_SIZE(sizeNum) \
    do { (sizeNum) &= 0x7; if ((sizeNum) < 1) { (sizeNum) = 1; } } while (0)

GdkGC *
fe_GetGCfromDW (fe_Drawable *fe_drawable,
                GdkGCValuesMask flags,
                GdkGCValues *gcv)
{
  GdkGC *retval;

  if(fe_drawable->clip_region)
    {
      gcv->clip_x_origin = fe_drawable->x_origin;
      gcv->clip_y_origin = fe_drawable->y_origin;
      flags |= GDK_GC_CLIP_X_ORIGIN | GDK_GC_CLIP_Y_ORIGIN;
    }

  retval = gdk_gc_new_with_values(fe_drawable->gdkdrawable,
                                  gcv, flags);

  if(fe_drawable->clip_region)
    gdk_gc_set_clip_region(retval, GDK_REGION(fe_drawable->clip_region));

  return retval;
}

static GdkGC *
fe_get_text_gc (MWContext *context, LO_TextAttr *text, fe_Font *font_ret,
		Boolean *selected_p, Boolean blunk)
{
  GdkGCValuesMask flags;
  GdkGCValues gcv;
  fe_Font font;
  fe_Drawable *fe_drawable = CONTEXT_DATA (context)->drawable;
  GdkColor fg, bg;
  GString *fontname = g_string_new(NULL);

  if (selected_p && *selected_p)
    {
      fg = CONTEXT_DATA (context)->select_fg_pixel;
      bg = CONTEXT_DATA (context)->select_bg_pixel;
    }
  else if(text->attrmask & LO_ATTR_ANCHOR)
    {
      g_print("Doing the anchor color thing\n");
      fg.red = LO_UNVISITED_ANCHOR_RED;
      fg.green = LO_UNVISITED_ANCHOR_GREEN;
      fg.blue = LO_UNVISITED_ANCHOR_BLUE;
      gdk_imlib_best_color_get(&fg);
      LO_COLOR_TO_GDK_COLOR(&text->bg, bg);
    }
  else
    {
      LO_COLOR_TO_GDK_COLOR(&text->fg, fg);
      LO_COLOR_TO_GDK_COLOR(&text->bg, bg);
    }
  if (blunk)
    fg = bg;

  /* foundry-face-weight-slant-sWdth-adstyl-pxlsz-ptSz-resx-resy-spc-avgWdth-rgstr-encoding */
  g_string_sprintf(fontname, "*-%s-%s-%s-*-*-*-%d-*",
                   text->font_face?text->font_face:"helvetica",
                   (text->fontmask & LO_FONT_BOLD)?"bold":"medium",
                   (text->fontmask & LO_FONT_ITALIC)?"i":"r",
                   text->point_size?((int)(text->point_size * 10)):120);
                   
  font = gdk_font_load(fontname->str);

  if (! font)
    {
      g_warning("gdk_font_load(%s) returned NULL\n",
                fontname->str);
      g_string_free(fontname, TRUE);
      return NULL;
    }

  g_string_free(fontname, TRUE);
  

  flags = 0;
  gcv.font = font;
  gcv.foreground = fg;
  gcv.background = bg;
  flags = GDK_GC_FOREGROUND | GDK_GC_BACKGROUND | GDK_GC_FONT;

  if (font_ret) *font_ret = font;

  return fe_GetGCfromDW (fe_drawable, flags, &gcv);
}

static void 
GTKFE_DisplaySubtext (MWContext * context,
		       int iLocation,
		       LO_TextStruct *text,
		       int32 start_pos,
		       int32 end_pos,
		       XP_Bool need_bg)
{
  GdkGC *gc;
  GdkFont *fret;
  gint x, y, ptsize;
  fe_Drawable *fe_drawable = CONTEXT_DATA(context)->drawable;

  gc = fe_get_text_gc (context, text->text_attr, &fret,
                       NULL, FALSE);

  x = (text->x + text->x_offset
       - GTK_LAYOUT(CONTEXT_DATA (context)->layout)->xoffset);
  y = (text->y + text->y_offset
       - GTK_LAYOUT(CONTEXT_DATA (context)->layout)->yoffset);
  x += fe_drawable->x_origin;
  y += fe_drawable->y_origin;
  if(text->text_attr->point_size != 0.0)
    ptsize = ((int)text->text_attr->point_size) >> 1;
  else
    ptsize = 12; /* Assume point size of 12 */

  y += ptsize;

#ifdef DEBUG_DRAW
  g_print("Displaying subtext %10s at (%d, %d)\n",
          text->text, x, y);
#endif
  gdk_draw_text(fe_drawable->gdkdrawable, fret, gc, x, y, (char *)text->text + start_pos,
                end_pos - start_pos);

  if(text->text_attr->attrmask & LO_ATTR_STRIKEOUT)
    ptsize >>= 1;

  if((text->text_attr->attrmask & LO_ATTR_ANCHOR)
     || (text->text_attr->attrmask & LO_ATTR_UNDERLINE)
     || (text->text_attr->attrmask & LO_ATTR_STRIKEOUT))
    {
      g_print("subDrawing line below\n");
      gdk_draw_line(fe_drawable->gdkdrawable, gc,
                    x, y + ptsize, x + (end_pos - start_pos)*ptsize,
                    y+ptsize);
    }

  gdk_gc_destroy(gc);
}

static void 
GTKFE_DisplayText (MWContext * context,
                   int iLocation,
                   LO_TextStruct *text,
                   XP_Bool need_bg)
{
  GdkGC *gc;
  GdkFont *fret;
  gint x, y;
  fe_Drawable *fe_drawable = CONTEXT_DATA(context)->drawable;
  int ptsize;

  gc = fe_get_text_gc (context, text->text_attr, &fret,
                       NULL, FALSE);

  x = (text->x + text->x_offset
       - GTK_LAYOUT(CONTEXT_DATA (context)->layout)->xoffset);
  y = (text->y + text->y_offset
       - GTK_LAYOUT(CONTEXT_DATA (context)->layout)->yoffset);
  x += fe_drawable->x_origin;
  y += fe_drawable->y_origin;
  if(text->text_attr->point_size != 0.0)
    ptsize = ((int)text->text_attr->point_size) >> 1;
  else
    ptsize = 12; /* Assume point size of 12 */
  y += ptsize;

#ifdef DEBUG_DRAW
  g_print("Displaying text %.*s at (%d, %d)\n",
          text->text_len, text->text, x, y);
#endif

  gdk_draw_text(fe_drawable->gdkdrawable, fret, gc, x, y, (char *)text->text,
                text->text_len);

  if(text->text_attr->attrmask & LO_ATTR_STRIKEOUT)
    ptsize >>= 1;

  if((text->text_attr->attrmask & LO_ATTR_ANCHOR)
     || (text->text_attr->attrmask & LO_ATTR_UNDERLINE)
     || (text->text_attr->attrmask & LO_ATTR_STRIKEOUT))
    {
      g_print("Drawing line below\n");
      gdk_draw_line(fe_drawable->gdkdrawable, gc,
                    x, y + ptsize, x + strlen((char *)text->text)*ptsize,
                    y+ptsize);
    }

  gdk_gc_destroy(gc);
}

static void 
GTKFE_DisplayEmbed (MWContext * context,
		     int iLocation,
		     LO_EmbedStruct *embed_struct)
{
   XP_ASSERT(0);
}


static void 
GTKFE_DisplayJavaApp (MWContext * context,
		       int iLocation,
		       LO_JavaAppStruct *java_struct)
{
   XP_ASSERT(0);
}

static void 
GTKFE_DisplayEdge (MWContext * context,
                   int iLocation,
                   LO_EdgeStruct *edge_struct)
{
   XP_ASSERT(0);
}

static void 
GTKFE_DisplayTable (MWContext * context,
                    int iLocation,
                    LO_TableStruct *table_struct)
{
   XP_ASSERT(0);
}

static void 
GTKFE_DisplayCell (MWContext * context,
		    int iLocation,
		    LO_CellStruct *cell_struct)
{
   XP_ASSERT(0);
}

static void 
GTKFE_DisplaySubDoc (MWContext * context,
		      int iLocation,
		      LO_SubDocStruct *subdoc_struct)
{
   XP_ASSERT(0);
}

static void 
GTKFE_DisplayLineFeed (MWContext * context,
			int iLocation ,
			LO_LinefeedStruct *line_feed,
			XP_Bool need_bg)
{
#if 0
  int x, y, width, height;
  fe_Drawable *fe_drawable = CONTEXT_DATA(context)->drawable;
  GdkGCValues gcv;

  x = line_feed->x + line_feed->x_offset;
  y = line_feed->y + line_feed->y_offset;
  width = line_feed->width;
  height = line_feed->height;

  gc = fe_GetGCfromDW(fe_drawable, GDK_GC_FOREGROUND, &gcv);

  gdk_draw_rectangle

  gdk_gc_destroy(gc);
#endif
}

static void 
GTKFE_DisplayHR (MWContext * context,
		  int iLocation ,
		  LO_HorizRuleStruct *HR_struct)
{

  GdkGC *gc;
  fe_Drawable *fe_drawable = CONTEXT_DATA(context)->drawable;
  int x, y, width, height;
  GdkGCValues gcv;

  memset(&gcv.foreground.pixel, 0, sizeof(GdkColor));
  gc = fe_GetGCfromDW(fe_drawable, GDK_GC_FOREGROUND, &gcv);

  x = HR_struct->x + HR_struct->x_offset;
  y = HR_struct->y + HR_struct->y_offset;

  width = HR_struct->width - 1;
  height = HR_struct->height?HR_struct->height:1;

  gdk_draw_rectangle(fe_drawable->gdkdrawable, gc, 
                     TRUE, x, y, width, height);

  gdk_gc_destroy(gc);
}

static void 
GTKFE_DisplayBullet (MWContext *context,
		      int iLocation,
		      LO_BulletStruct *bullet)
{

  GdkGC *gc;
  GdkGCValues gcv;
  int w,h;
  gboolean hollow_p;
  GdkDrawable *drawable;
  fe_Drawable *fe_drawable = CONTEXT_DATA (context)->drawable;
  GtkLayout *layout = GTK_LAYOUT(CONTEXT_DATA(context)->layout);

  long x = bullet->x + bullet->x_offset - layout->xoffset +
      fe_drawable->x_origin;
  long y = bullet->y + bullet->y_offset - layout->yoffset +
      fe_drawable->y_origin;

  LO_COLOR_TO_GDK_COLOR((&bullet->text_attr->fg), gcv.foreground);
  gc = fe_GetGCfromDW(fe_drawable, GDK_GC_FOREGROUND, &gcv);

  drawable = fe_drawable->gdkdrawable;
  w = bullet->width;
  h = bullet->height;
  hollow_p = (bullet->bullet_type != BULLET_BASIC);

  switch (bullet->bullet_type)
    {
    case BULLET_BASIC:
    case BULLET_ROUND:
      /* Subtract 1 to compensate for the behavior of XDrawArc(). */
      w -= 1;
      h -= 1;
      /* Now round up to an even number so that the circles look nice. */
      (w & 1) || w++;
      (y & 1) || h++;
      g_warning("bullet: arc at (%ld, %ld)+%d+%d\n",
                x, y, w, h);
      gdk_draw_arc(drawable, gc, !hollow_p, x, y, w, h, 0, 360*64);
      break;
    case BULLET_SQUARE:
      g_warning("bullet: square at (%ld, %ld)+%d+%d\n",
                x, y, w, h);
      gdk_draw_rectangle(drawable, gc, !hollow_p, x, y, w, h);
      break;
    case BULLET_MQUOTE:
      /*
       * WARNING... [ try drawing a 2 pixel wide filled rectangle ]
       *
       *
       */
      w = 2;
      gdk_draw_rectangle(drawable, gc, TRUE, x, y, w, h);
      g_warning("bullet: mquote at (%ld, %ld)+%d+%d\n",
                x, y, w, h);
      break;

    case BULLET_NONE:
      g_print("BULLET_NONE\n");
      /* Do nothing. */
      break;
      
    default:
      XP_ASSERT(0);
    }

  gdk_gc_destroy(gc);
}

static void 
GTKFE_DisplayFormElement (MWContext * context,
			   int iLocation,
			   LO_FormElementStruct * form_element)
{
   XP_ASSERT(0);
}

static void 
GTKFE_DisplayBorder (MWContext *context,
		      int iLocation,
		      int x,
		      int y,
		      int width,
		      int height,
		      int bw,
		      LO_Color *color,
		      LO_LineStyle style)
{
  GdkGC *gc;
  GdkGCValues gcv;
  int Red, Green, Blue;
  fe_Drawable *fe_drawable = CONTEXT_DATA(context)->drawable;

  if(bw <= 0)
    return;

  if(color){
    Red = color->red; Green = color->green; Blue = color->blue;
  } else
    Red = Green = Blue = 0;

  gcv.foreground.red = Red;
  gcv.foreground.green = Green;
  gcv.foreground.blue = Blue;
  gcv.foreground.pixel = gdk_imlib_best_color_match(&Red, &Green, &Blue);

  if(style == LO_DASH)
    gcv.line_style = GDK_LINE_ON_OFF_DASH;
  else
    gcv.line_style = GDK_LINE_SOLID;

  gcv.line_width = bw;

  gc = fe_GetGCfromDW(fe_drawable, GDK_GC_FOREGROUND|GDK_GC_LINE_STYLE|GDK_GC_LINE_WIDTH,
                      &gcv);

  gdk_draw_rectangle(fe_drawable->gdkdrawable, gc, FALSE, x + (bw / 2), y + (bw / 2),
                     width - bw, height - bw);
  gdk_gc_destroy(gc);
}

static void 
GTKFE_DisplayFeedback (MWContext *context,
			int iLocation,
			LO_Element *element)
{
   XP_ASSERT(0);
}

static void 
GTKFE_ClearView (MWContext * context,
		  int which)
{
   XP_ASSERT(0);
}

static void 
GTKFE_SetDocDimension (MWContext *context,
			int iLocation,
			int32 iWidth,
			int32 iLength)
{
  XP_ASSERT(context);
  g_print("SetDocDimension to %dx%d\n", iWidth, iLength);
  gtk_layout_set_size(GTK_LAYOUT(CONTEXT_DATA(context)->layout),
                      iWidth, iLength);
}

static void
GTKFE_SetDocPosition (MWContext *context, int iLocation, int32 x, int32 y)
{
  GtkLayout *layout = GTK_LAYOUT(CONTEXT_DATA(context)->layout);
  /* This function takes care of setting the scroll policies of a window. */
  gtk_adjustment_set_value(layout->hadjustment, (gfloat)x);
  gtk_adjustment_set_value(layout->vadjustment, (gfloat)y);
}

static void
GTKFE_LayoutNewDocument(MWContext *context,
			 URL_Struct *url,
			 int32 *iWidth,
			 int32 *iHeight,
			 int32 *mWidth,
			 int32 *mHeight)
{
  fprintf (stderr,"LayoutNewDocument(%p,%p,%d,%d,%d,%d)\n",
	    context,url,*iWidth,*iHeight,*mWidth,*mHeight);
  
  CONTEXT_DATA (context)->delayed_images_p = False;

   
  /* The pixmap itself is freed when its IL_Image is destroyed. */
  CONTEXT_DATA (context)->backdrop_pixmap = 0;

  /* Set background after making the backdrop_pixmap 0 as SetBackground
   * will ignore a background setting request if backdrop_pixmap is
   * available.
   */

  /*FIXME: This is a nasty  hack [VH] */

  /* Clear the background since, in the case of grid cells without borders, 
     the grid boundaries don't get cleared */

  *iWidth = CONTEXT_DATA(context)->layout->allocation.width;
  *iHeight = CONTEXT_DATA(context)->layout->allocation.height;
  *mWidth = 4;
  *mHeight = 4;

  /* Get rid of the old title; don't install "Unknown" until we've gotten
     to the end of the document without XFE_SetDocTitle() having been called.
   */
  if (context->title)
    free (context->title);
  context->title = 0;

  /* This is set in fe_resize_cb */
  CONTEXT_DATA (context)->is_resizing = FALSE;

  if (url->address)
    {
      fprintf(stderr, "adding %s to history\n", url->address);
      SHIST_AddDocument (context, SHIST_CreateHistoryEntry (url, ""));
      SetURLString(context, url);
    }

  /* Make sure we clear the string from the previous document */
  if (context->defaultStatus) {
    XP_FREE (context->defaultStatus);
    context->defaultStatus = 0;
  }

  GTKFE_SetDocPosition (context, 0, 0, 0);
}


static void 
GTKFE_GetDocPosition (MWContext *context,
		       int iLocation,
		       int32 *iX,
		       int32 *iY)
{
  GtkLayout *layout = GTK_LAYOUT(CONTEXT_DATA(context)->layout);

  *iX = layout->xoffset;
  *iY = layout->yoffset;
}

static void 
GTKFE_BeginPreSection (MWContext *context)
{
}

static void 
GTKFE_EndPreSection (MWContext *context)
{
}

/*
 * Print a message in the status line. No pointer to the message is preserved.
 */
static void
GTKFE_Progress(MWContext *context, const char *message)
{
#ifdef DEBUG_PROGRESS
   printf("XFE_Progress(%s)\n",message ? message : "BLANK");
#endif

   UpdateStatusText(context,(char *) message);
}                                       


static void 
GTKFE_SetProgressBarPercent (MWContext *context,
                             int32 percent)
{
  gtk_progress_bar_update(GTK_PROGRESS_BAR(CONTEXT_DATA(context)->progressbar),
                          ((gfloat)percent)/100);
}

static void 
GTKFE_SetBackgroundColor (MWContext *context,
                          uint8 red,
                          uint8 green,
                          uint8 blue)
{
  GdkColor c;
  GtkWidget *layout;
  GtkStyle *style;
  gint Red=red, Green=green, Blue=blue;

  DMC();

  layout = CONTEXT_DATA(context)->layout;

  XP_ASSERT(layout);

  style = gtk_style_copy(layout->style);
  gtk_style_ref(style);

  c.red = (gushort)(red << 8);
  c.green = (gushort)(green << 8);
  c.blue = (gushort)(blue << 8);
  c.pixel = gdk_imlib_best_color_match(&Red, &Green, &Blue);

  g_print("setting bgcolor to #%2x%2x%2x [%ld]\n", c.red, c.green, c.blue,
          c.pixel);

  style->bg[GTK_STATE_NORMAL] = c;
  if(style->bg_gc[GTK_STATE_NORMAL])
    gdk_gc_set_background(style->bg_gc[GTK_STATE_NORMAL], &c);
  gtk_widget_set_style(layout, style);
  gtk_style_unref(style);

  DMC();
}

static gint fe_netlib_hungry_p = 0, fe_netlib_idler = -1;

static void
netlib_idler(void)
{
	NET_ProcessNet ((void *)-1, NET_EVERYTIME_TYPE);
#ifdef MOZ_NEO
	/* No idea what NeoAccess is. XFE had it. */
	MSG_OnIdle();
#endif
}

static void 
GTKFE_SetCallNetlibAllTheTime (MWContext * win_id)
{
   if((fe_netlib_hungry_p == 0) && (fe_netlib_idler == -1))
	fe_netlib_idler = gtk_idle_add((GtkFunction)netlib_idler, win_id);
   fe_netlib_hungry_p++;
}

static void 
GTKFE_ClearCallNetlibAllTheTime (MWContext * win_id)
{
   --fe_netlib_hungry_p;
   if((fe_netlib_hungry_p == 0)
      && (fe_netlib_idler != -1))
     {
	gtk_idle_remove(fe_netlib_idler);
	fe_netlib_idler = -1;
     }
   XP_ASSERT(fe_netlib_hungry_p >= 0);
}


/* Inform the user that a document is being transferred.
   URL is the url to which this report relates;
   bytes_received is how many bytes have been read;
   content_length is how large this document is (0 if unknown.)
   This is called from netlib as soon as we know the content length
    (or know that we don't know.)  It is called only once per document.
 */
static void 
GTKFE_GraphProgressInit (MWContext *context_in,
			  URL_Struct *URL_s,
			  int32 content_length)
{
#ifdef DEBUG_PROGRESS
	printf("XFE_GraphProgressInit(%ld)\n",content_length);
#endif

   MWContext * context = XP_GetNonGridContext(context_in);

   if (!CHECK_CONTEXT_AND_DATA(context)) return;
   
   if (!CONTEXT_DATA(context)->thermo_timer_id)
     {
	/* Hey!  How did that get turned off?  Turn it back on. */

	StartProgressGraph (context);
     }
	
   if (content_length == 0) CONTEXT_DATA(context)->thermo_size_unknown_count++;
   else CONTEXT_DATA(context)->thermo_total += content_length;
}

/* Remove --one-- transfer from the progress graph.
 */

static void 
GTKFE_GraphProgressDestroy (MWContext *context_in,
			     URL_Struct *URL_s,
			     int32 content_length,
			     int32 total_bytes_read)
{
   MWContext * context = XP_GetNonGridContext(context_in);

   if (!CHECK_CONTEXT_AND_DATA(context)) return;
   
   if (content_length == 0)
     {
	/* Now that this transfer is done, we finally know how big it was.
	 This means that maybe we can go out of cylon mode and back into
	 thermometer mode. */
	CONTEXT_DATA (context)->thermo_size_unknown_count--;
	CONTEXT_DATA (context)->thermo_total += total_bytes_read;
     }
}

/* Inform the user of current progress, somehow.
   URL is the url to which this report relates;
   bytes_received is how many bytes have been read;
   content_length is how large this document is (0 if unknown.)
   This is called from netlib, and may be called very frequently.
 */
static void 
GTKFE_GraphProgress (MWContext *context_in,
		      URL_Struct *URL_s,
		      int32 bytes_received,
		      int32 bytes_since_last_time,
		      int32 content_length)
{
  MWContext * context = XP_GetNonGridContext(context_in);

#ifdef DEBUG
   /*
     fprintf (stdout,"GTKFE_GraphProgress:\n\t%ld bytes received\n\t%ld bytes since last time\n\t%ld bytes long\n",
	    (long)bytes_received,
	    (long)bytes_since_last_time,
	    (long)content_length);
   */
#endif
   

   if (!CHECK_CONTEXT_AND_DATA(context)) return;
   
   if (CONTEXT_DATA (context)->thermo_data_start_time <= 0)
     /* This is the first chunk of bits to arrive. */
     CONTEXT_DATA (context)->thermo_data_start_time = time ((time_t *) 0);
	
   CONTEXT_DATA (context)->thermo_current += bytes_since_last_time;

   UpdateGraph (context, False, content_length);
}

static XP_Bool 
GTKFE_UseFancyFTP (MWContext * window_id)
{
   XP_ASSERT(0);
   return FALSE;
}

static XP_Bool 
GTKFE_UseFancyNewsgroupListing (MWContext *window_id)
{
   XP_ASSERT(0);
   return FALSE;
}

static int 
GTKFE_FileSortMethod (MWContext * window_id)
{
   XP_ASSERT(0);
   return -1;
}

static XP_Bool 
GTKFE_ShowAllNewsArticles (MWContext *window_id)
{
   XP_ASSERT(0);
   return FALSE;
}

/*
 * Are the functions below
 * needed at all? There are equivalent functions in gtkdlgs.
 * 
 * [VH]
 */

static void
GTKFE_Alert(MWContext *context,
	     const char *msg)
{
   XP_ASSERT(context);
   XP_ASSERT(msg);
   FE_Alert(context,msg);
}

static XP_Bool 
GTKFE_Confirm(MWContext * context,
	       const char * msg)
{
   XP_ASSERT(context);
   XP_ASSERT(msg);
   return FE_Confirm(context,msg);
}

static char* 
GTKFE_Prompt(MWContext * context,
	      const char * msg,
	      const char * dflt)
{
   XP_ASSERT(context);
   XP_ASSERT(msg);
   XP_ASSERT(dflt);
   return FE_Prompt(context,msg,dflt);
}

static XP_Bool 
GTKFE_PromptUsernameAndPassword (MWContext *context,
				  const char * message,
				  char **username,
				  char **password)
{
   XP_ASSERT(context);
   XP_ASSERT(message);
   return FE_PromptUsernameAndPassword (context, message, username, password);
}

static char* 
GTKFE_PromptPassword(MWContext * context,
		      const char * Msg)
{
   XP_ASSERT(context);
   XP_ASSERT(Msg);
   return FE_PromptPassword(context, Msg);
}
/********************/

static char* 
GTKFE_PromptWithCaption(MWContext * context,
			 const char * caption,
			 const char * msg,
			 const char * dflt)
{
#ifdef HAVE_GNOME
  GtkWidget *w;
  char *retval;
  int btn_num;
  w = gnome_message_box_new(msg, GNOME_MESSAGE_BOX_QUESTION,
                            dflt, NULL);
  gtk_window_set_title(GTK_WINDOW(w), caption);
  gnome_dialog_set_default(GNOME_DIALOG(w), 0);
  btn_num = gnome_dialog_run_modal(GNOME_DIALOG(w));
  if(btn_num >= 0)
    retval = (char *)dflt;
  else
    retval = NULL;
  gtk_widget_destroy(w);

  return retval;
#else
  return NULL;
#endif
}

static void
fe_SetCursor(MWContext *context,
             gboolean is_over_link)
{
  fe_ContextData *cd = CONTEXT_DATA(context);

  gdk_window_set_cursor(cd->layout->window,
                        is_over_link?cd->link_cursor:cd->normal_cursor);
}
             

/*  This is the function/define that is called when GTKFE_EnableClicking
 is called. We need to get the #define out of this file. [MW]
 
 Doesn't (*context->funcs->EnableClicking) point to GTKFE_EnableClicking()?
 I smell loop, and a stack overflow, and a seg. fault here if we
 try calling FE_EnableClicking() from GTKFE_EnableClicking()... 
 Ref: Declaration of ContextFuncs further down in this file. [VH]
 
 */
/*
  Before we can make this call we need to build the context->funcs by 
  having a call to FE_GetInitContext() or something like it.
*/

static void 
GTKFE_EnableClicking(MWContext* context)
{
  /* FIXME: Next candidate for implementation... 
     Called at least three times now... */

  MWContext *top = XP_GetNonGridContext (context);
  XP_Bool running_p;

  /*XFE_Frame *f;*/

  /* this is very evil but has been known to happen*/
  if (top == NULL) return;
  
  running_p = XP_IsContextBusy (top);
  
  if (! running_p)
    fe_StopProgressGraph(context);

  if (CONTEXT_DATA (context)->clicking_blocked)
    {
      CONTEXT_DATA (context)->clicking_blocked = False;
      /* #### set to link cursor if over link. */
      fe_SetCursor (context, False);
    }

  /*
   * FIXME this is still pretty broken, but it does do something close to
   * correct.  The menu_items need to be fixed so does the progress bar etc
   */
  
  /* FIXME: Doesn't work with Gnome'ified toolbar code [VH] */
  if (CONTEXT_DATA (top)->show_toolbar_p) {
    if (CONTEXT_DATA (top)->abort_button)
      gtk_widget_set_sensitive (CONTEXT_DATA (top)->abort_button,
                                IsContextStoppable(top));
  }
  if (CONTEXT_DATA (top)->show_menubar_p && CONTEXT_DATA (top)->menubar) {
    /* XXX TODO actually do the abort_menuitem thing */
#ifdef FINISHED
    if (CONTEXT_DATA (top)->abort_menuitem)
      gtk_widget_set_sensitive (CONTEXT_DATA (top)->abort_menuitem,
                                IsContextStoppable(top));
#endif
  }
} 

static void 
GTKFE_AllConnectionsComplete(MWContext * context)
{
  /* FIXME: Called sometime after url_exit... Should do something soon ;) */
  UpdatePercent(context,1, 0);
  gtk_statusbar_push(GTK_STATUSBAR(CONTEXT_DATA(context)->statusbar),
                     1, (gchar *)"Document: done!");
}

static void 
GTKFE_EraseBackground (MWContext * context,
                       int iLocation,
                       int32 x,
                       int32 y,
                       uint32 width,
                       uint32 height,
                       LO_Color *bg)
{
  fe_Drawable *fe_drawable = CONTEXT_DATA(context)->drawable;

  if(bg)
    {
      GdkGC *gc = gdk_gc_new(fe_drawable->gdkdrawable);
      GdkColor color;
      int Red = bg->red, Green = bg->green, Blue = bg->blue;
      color.red = bg->red << 8;
      color.green = bg->green << 8;
      color.blue = bg->blue << 8;
      color.pixel = gdk_imlib_best_color_match(&Red, &Green, &Blue);
      gdk_gc_set_foreground (gc, &color);
      gdk_draw_rectangle (fe_drawable->gdkdrawable, gc, TRUE, x, y, width, height);
    }
  else
    gdk_window_clear_area(fe_drawable->gdkdrawable, x, y, width, height);
}

static void 
GTKFE_SetDrawable (MWContext *context,
                   CL_Drawable *drawable)
{
  XP_ASSERT(context);
  if(context && drawable)
    CONTEXT_DATA(context)->drawable = CL_GetDrawableClientData(drawable);
}

static void 
GTKFE_GetTextFrame (MWContext *context,
                    LO_TextStruct *text,
                    int32 start,
                    int32 end,
                    XP_Rect *frame)
{
   XP_ASSERT(0);
}

/* these functions are to allow dealyed native window applet creation and transparent applet */
static void 
GTKFE_HandleClippingView (MWContext *pContext,
			   struct LJAppletData *appletD,
			   int x,
			   int y,
			   int width,
			   int height)
{
   XP_ASSERT(0);
}

static void 
GTKFE_DrawJavaApp (MWContext *pContext,
		    int iLocation,
		    LO_JavaAppStruct *pJava)
{
   XP_ASSERT(0);
}

/*
  the warnings:../../include/mk_cx_fn.h:75-8: warning: 
  initialization from incompatible pointer type.
  needs to be fixed. [MW]

  <Maybe> this should go into the gtkfe.h?
 
  Nope. It can't. This is a constant... It's used when we
  initialize the MWContext... [VH]
*/

ContextFuncs gtkfe_context_funcs = {
#define FE_DEFINE(func, returns, args) GTKFE_##func,
  #include "mk_cx_fn.h"
};

MWContext*
GTKFE_CreateMWContext(void)
{
  MWContext *context = XP_NewContext();

  g_print("CreateMWContext called!\n");

  XP_ASSERT(context);
  if (!context) return NULL;

  context->funcs = &gtkfe_context_funcs;
  return context;
}

int32
FE_GetContextID(MWContext *window_id)
{
   return (int32)window_id;
}



/*
 * Get a context that represents the background root window. This is used
 * at init time to find a context we can use to prompt for passwords,
 * confirm or alert before any main window contexts get initialized.
 */
MWContext*
FE_GetInitContext()
{
  /* FIXME
   * This seems rather important to me. It is called so that
   * the inital MWContext can be built.  Without it we can't do anything. [MW]
   * 
   * The comments in XFE says it's only used to be able to put up Alert's etc.
   * before the main window is up. Should implement it soon anyways... [VH]
  */

   XP_ASSERT(0);
   return NULL;
}

MWContext*
FE_MakeNewWindow(MWContext *old_context,
		 URL_Struct *url,
		 char *window_name,
		 Chrome *chrome)
{
  return GTKFE_MakeNewWindow(url, window_name, chrome, old_context);
}

void
FE_DestroyWindow(MWContext *context)
{
   XP_ASSERT(0);
}

void
FE_UpdateStopState(MWContext *context)
{
   XP_ASSERT(0);
}

void
FE_UpdateChrome(MWContext *window,
		Chrome *chrome)
{
  fe_ContextData *c;

  if (!chrome || !window || window->type != MWContextBrowser)
    return;

  c = CONTEXT_DATA(window);

#define GTKFE_SHOWHIDE(boolvar, widget) \
  if(chrome->boolvar) \
    gtk_widget_show(c->widget); \
  else \
    gtk_widget_hide(c->widget)

  GTKFE_SHOWHIDE(show_bottom_status_bar, statusbar);
  GTKFE_SHOWHIDE(show_button_bar, toolbar);
  GTKFE_SHOWHIDE(show_url_bar, locationhbox);
  GTKFE_SHOWHIDE(show_scrollbar, hscroll);
  GTKFE_SHOWHIDE(show_scrollbar, vscroll);
  GTKFE_SHOWHIDE(show_menu, menubar);
  /* XXX TODO:
     show_directory_buttons
     show_security_bar
  */

  gdk_window_set_override_redirect(c->main->window, chrome->hide_title_bar?TRUE:FALSE);
  if(c->delete_event_close_id != -1)
      gtk_signal_disconnect(GTK_OBJECT(c->main), c->delete_event_close_id);
  if(c->close_callback_id != -1)
    {
      gtk_signal_disconnect(GTK_OBJECT(c->main), c->close_callback_id);
      c->close_callback_id = -1;
    }

  if(chrome->allow_close)
    {
      struct gtkfe_SigRelayInfo *ri;
      c->close_callback_id = 
        gtk_signal_connect(GTK_OBJECT(c->main),
                           "delete_event",
                           GTK_SIGNAL_FUNC(gtkfe_QuitCallback),
                           c);
      ri = g_new(struct gtkfe_SigRelayInfo, 1);
      ri->callback = chrome->close_callback;
      ri->arg = chrome->close_arg;
      c->delete_event_close_id =
        gtk_signal_connect_interp(GTK_OBJECT(c->main),
                                  "delete_event",
                                  (GtkCallbackMarshal)gtkfe_callback_relay,
                                  ri,
                                  GTKFE_FREE,
                                  GTK_RUN_FIRST);
    }
  else
    {
      c->delete_event_close_id =
        gtk_signal_connect(GTK_OBJECT(c->main),
                           "delete_event",
                           GTK_SIGNAL_FUNC(gtk_false),
                           NULL);
    }

  
}

void
FE_QueryChrome(MWContext *window,
	       Chrome *chrome)
{
  fe_ContextData *fec = CONTEXT_DATA(window);

#define SC(chromevar, cdvar) chrome->chromevar = fec->cdvar

  SC(show_bottom_status_bar, show_bottom_status_bar_p);

  gdk_window_get_origin(fec->main->window, &chrome->l_hint, &chrome->t_hint);
  chrome->w_hint = fec->layout->allocation.width;
  chrome->h_hint = fec->layout->allocation.height;
  chrome->outw_hint = fec->main->allocation.width;
  chrome->outh_hint = fec->main->allocation.height;

  chrome->show_scrollbar = PR_TRUE;
}

void 
FE_BackCommand(MWContext *context)
{
  /* this one taken from xfe/commands.c/fe_back_cb [TP] */
  MWContext *top_context = XP_GetNonGridContext(context);
  URL_Struct *url;
  /* FIXME
  if (FE_IsGridParent(top_context))
    {
      if (LO_BackInGrid(top_context))
        {
          return;
        }
    }
0  */
  url = SHIST_CreateURLStructFromHistoryEntry (top_context,
                                               SHIST_GetPrevious (top_context));
  if (url)
    FE_GetURL (top_context, url);
  else
    FE_Alert (top_context, "No previous url"); /* FIXME */
}

void 
FE_ForwardCommand(MWContext *context)
{
  /* Taken from xfe/command.c/fe_forward_cb [TP] */
  MWContext *top_context = XP_GetNonGridContext(context);
  URL_Struct *url;
  /* FIXME
  if (FE_IsGridParent(top_context))
    {
        if (LO_ForwardInGrid(top_context))
          {
            return;
          }
    }
  */
  url = SHIST_CreateURLStructFromHistoryEntry (top_context,
					       SHIST_GetNext (top_context));
  if (url)
    FE_GetURL (top_context, url);
  else
    FE_Alert (top_context, "No next url"); /* FIXME */
}

void 
FE_HomeCommand(MWContext *context)
{
  URL_Struct *url;
  url = NET_CreateURLStruct ("http://home.netscape.com/", FALSE);
  if (url) FE_GetURL (context, url);
}

void 
FE_PrintCommand(MWContext *context)
{
  XP_ASSERT(0);
}

static void
find_button_pressed(GtkWidget *widget, gint button_number, gpointer user_data)
{
}

XP_Bool
FE_FindCommand(MWContext *context,
	       char *text,
	       XP_Bool case_sensitive,
	       XP_Bool backwards,
	       XP_Bool wrap)
{
  XP_ASSERT(0);
  return PR_FALSE;
}

void 
FE_GetWindowOffset(MWContext *pContext, 
		   int32 *sx, 
		   int32 *sy)
{
  gdk_window_get_origin(CONTEXT_WIDGET(pContext)->window, sx, sy);
}

void 
FE_GetScreenSize(MWContext *pContext, 
		 int32 *sx, 
		 int32 *sy)
{
  *sx = gdk_screen_width();
  *sy = gdk_screen_height();
}

void 
FE_GetAvailScreenRect(MWContext *pContext, 
		      int32 *sx, 
		      int32 *sy, 
		      int32 *left, 
		      int32 *top)
{
  *left = *top = 0;
  FE_GetScreenSize(pContext, sx, sy);
}

void 
FE_GetPixelAndColorDepth(MWContext *pContext, 
			 int32 *pixelDepth, 
			 int32 *colorDepth)
{
  GdkVisual *v = gdk_imlib_get_visual();
  
  *pixelDepth = *colorDepth = v->depth;

  g_warning("We returned pixel & color depth as %d\n", v->depth);
}

void
FE_ShiftImage (MWContext *context,
	       LO_ImageStruct *lo_image)
{
   XP_ASSERT(0);
}

void
FE_ScrollDocTo (MWContext *context,
		int iLocation,
		int32 x,
		int32 y)
{
  GtkLayout *layout = GTK_LAYOUT(CONTEXT_DATA(context)->layout);

  gtk_adjustment_set_value(layout->hadjustment, (gfloat)x);
  gtk_adjustment_set_value(layout->vadjustment, (gfloat)y);
}

void
FE_ScrollDocBy (MWContext *context,
		int iLocation,
		int32 x,
		int32 y)
{
  GtkLayout *layout = GTK_LAYOUT(CONTEXT_DATA(context)->layout);

  gtk_adjustment_set_value(layout->hadjustment,
                           ((gfloat)x) + layout->hadjustment->value);
  gtk_adjustment_set_value(layout->vadjustment,
                           ((gfloat)y) + layout->vadjustment->value);
}



/* Callback hack.... Used below */

/*
 * Cleans up after Mozilla is done retrieving a document
 * 
 * [VH]
 */
void url_exit(URL_Struct *url, int status, MWContext *context) {

   XP_ASSERT(context);
   XP_ASSERT(url);
   
   /* Got this assert from XFE. Is it right? [VH] */
   XP_ASSERT (((status == MK_INTERRUPTED) || status >= 0) ||
	      CONTEXT_DATA (context)->active_url_count > 0);
   
   if(! (((status == MK_INTERRUPTED) || status >= 0) ||
	      CONTEXT_DATA (context)->active_url_count > 0))
     {
       g_print("status = %d, active_url_count = %d\n",
               status, CONTEXT_DATA (context)->active_url_count);
     }
   
   if (CONTEXT_DATA (context)->active_url_count > 0)
     CONTEXT_DATA (context)->active_url_count--;
   
  /* We should be guarenteed that XFE_AllConnectionsComplete() will be called
     right after this, if active_url_count has just gone to 0. */
  if (status < 0 && url->error_msg)
    {
      FE_Alert (context, url->error_msg);
    }

#if defined(EDITOR) && defined(EDITOR_UI)
  /*
   *    Do stuff specific to the editor
   */
  if (context->type == MWContextEditor)
    /*fe_EditorGetUrlExitRoutine(context, url, status);*/
#endif
   if (status != MK_CHANGING_CONTEXT)
     {
	NET_FreeURLStruct (url);
     }


   /* FIXME: This code was in qtfe (slightly C'ified here), but not in XFE it seems...
    * And not documented... Hmm. Needs to find out what it does. [VH]
    */
#if 0   
   if (CONTEXT_DATA(context)->destroyed){
      XP_RemoveContextFromList(context);

      free(CONTEXT_DATA(context);    /* FIXME: Ouch... Smells memory leak and disaster a long way... [VH] */
   }
#endif
}

/* Stuff from qtfe [VH] */

/* This is how the libraries ask the front end to load a URL just
 *  as if the user had typed/clicked it (thermo and busy-cursor
 *  and everything.)  NET_GetURL doesn't do all this stuff.
 */

int
FE_GetURL (MWContext *context,
	   URL_Struct *url)
{
   int32 x,y;
   
   XP_ASSERT(context);
   
   if (url && XP_FindNamedAnchor (context, url, &x, &y)) {

      /* FIXME: Don't know what this does yet... (Yeah, I'm lazy) [VH] */
#if 0
       setupToolbarSignals();
       emit urlChanged( url->address );
#endif
      context->url = strdup(url->address);
      
      /* FIXME: What's this? Yuck.. We're actually getting deep into the FE now. From qtfe */
#if 0      
      setDocPosition( 0, x, y ); //### what is an iLocation ???
#endif
      return 0;
   } else {
      XP_InterruptContext(context);
      
#if 0
      setupToolbarSignals();
      emit urlChanged( url->address );
#endif

      context->url = strdup(url->address);
      return NET_GetURL( url, FO_CACHE_AND_PRESENT, context, url_exit); /* url_exit? Some callback? */
    }
   return 0;
}

/* Code is in the process of being migrated to gtk. [MW] */
void
FE_SetRefreshURLTimer(MWContext *context, 
		      uint32     seconds, 
		      char      *refresh_url)
{
#if 0
   guint interval = (guint)seconds;
   gint timer_id;
#endif

   XP_ASSERT(context); /* I'm fanatic about checking pointers that
			* will be dereferenced ;) [VH] */
   XP_ASSERT(refresh_url);
                        /* If we're told to refresh an URL without
			 * getting an URL, something is fishy [VH] */
   
   if(context->type != MWContextBrowser)
     return;
  
  if (CONTEXT_DATA (context)->refresh_url_timer)
    {
       /* XtRemoveTimeOut (CONTEXT_DATA (context)->refresh_url_timer);
	  We need to add out own gtk code here for timeouts.
	  This code is ran if the refresh_url_timer has already been set.
	  So, we need to get rid of the old timer before setting a new one.
       
	  Something like.
	  gtk_timeout_remove((gint)CONTEXT_DATA (context)->refresh_url_timer); 
	  [MW]
	*/
      
       gtk_timeout_remove((gint)CONTEXT_DATA (context)->refresh_url_timer); 
    }
   
   if (CONTEXT_DATA (context)->refresh_url_timer_url)
     {
	/*
	  free (CONTEXT_DATA (context)->refresh_url_timer_url);
	  
	  Shouldn't this be XP_FREE or some gtk/glib free? 
	 
	  This also needs to be reset before a new one is setup for this
	  context.
         */
      free (CONTEXT_DATA (context)->refresh_url_timer_url);
      
    }

  CONTEXT_DATA (context)->refresh_url_timer = 0;  
  CONTEXT_DATA (context)->refresh_url_timer_secs = seconds;
  CONTEXT_DATA (context)->refresh_url_timer_url = strdup (refresh_url);

  if (seconds <= 0)
    {
      /*
        fe_refresh_url_timer ((XtPointer) context, 0);
        fe_refresh_url_timer does a "force" url update.  
        Mode code that needs to migrate to gtk.
        
        GTKFE_refresh_url_timer((gpointer)context);

        Notice that i don't pass across the "0".  I hope it will still work.
        CONTEXT_DATA(context)->refresh_url_timer should be all we need.
      */

      GTKFE_refresh_url_timer((gpointer)context); /* Call the callback 
                                                     manually. */
      
    }
  else /* it is time to set the url_timer */
    {
      /*
        CONTEXT_DATA (context)->refresh_url_timer =
        XtAppAddTimeOut (fe_XtAppContext, seconds * 1000,
                         fe_refresh_url_timer, (XtPointer) context);
                    
        To migrate this code looks like we will have to assume that 
        XtIntervalId and gint are close enough so the data in "context"
        doesn't get messed up.

        Some thing like this should work:
        CONTEXT_DATA (context)->refresh_url_timer =
          gtk_timeout_add ((guint32) seconds * 1000,
                        GTKFE_refresh_url_timer,
                        (gpointer)context);

        The function fe_refresh_url_timer is like our GTKFE_refresh_url_timer.
      */

      CONTEXT_DATA (context)->refresh_url_timer =
        gtk_timeout_add ((guint32) seconds * 1000,
                         GTKFE_refresh_url_timer,
                         (gpointer)context);
      
    }

}

/*gtk look alike for fe_refresh_url_timer. 
Most of this code is robbed from that function in xfe.[MW]*/ 
gint
GTKFE_refresh_url_timer(gpointer data)
{
   MWContext *context = (MWContext*)data;

   URL_Struct *url;
  
   XP_ASSERT(context); /* Don't trust those who call us... [VH] */
   
   /* Get rid of the timer if one is set. */
   if(CONTEXT_DATA (context)->refresh_url_timer != 0)
     {
	gtk_timeout_remove((gint)CONTEXT_DATA (context)->refresh_url_timer);
     }
  
   CONTEXT_DATA (context)->refresh_url_timer = 0; /* clear timer*/
    
   XP_ASSERT (CONTEXT_DATA (context)->refresh_url_timer_url);

   if (! CONTEXT_DATA (context)->refresh_url_timer_url)
     return FALSE;
   url = NET_CreateURLStruct (CONTEXT_DATA (context)->refresh_url_timer_url,
			      NET_NORMAL_RELOAD);
   
   XP_ASSERT(url);
   
   url->force_reload = NET_NORMAL_RELOAD;
   
   FE_GetURL (context, url);
  
  
   return FALSE;  /*Dont take any chances.*/
}

/* 
 * FIXME the enable/disable funcs should also deal with the menu items right?
 * and I don't know what the returns should be at the moment
 */
int
FE_EnableBackButton(MWContext* context)
{
   XP_ASSERT(context);
   gtk_widget_set_sensitive(CONTEXT_DATA(context)->back_button, TRUE);
   gtk_widget_set_sensitive(CONTEXT_DATA(context)->back_menuitem, TRUE);
   return 0;
}

int
FE_EnableForwardButton(MWContext* context)
{  
   XP_ASSERT(context);
   gtk_widget_set_sensitive (CONTEXT_DATA(context)->forward_button, TRUE);
   gtk_widget_set_sensitive (CONTEXT_DATA(context)->forward_menuitem, TRUE);
   return 0;
}

int
FE_DisableBackButton(MWContext* context)
{
   XP_ASSERT(context);
   gtk_widget_set_sensitive (CONTEXT_DATA(context)->back_button, FALSE);
   gtk_widget_set_sensitive (CONTEXT_DATA(context)->back_menuitem, FALSE);
   return 0;
}

int
FE_DisableForwardButton(MWContext* context)
{
   XP_ASSERT(context);
   gtk_widget_set_sensitive (CONTEXT_DATA(context)->forward_button, FALSE);
   gtk_widget_set_sensitive (CONTEXT_DATA(context)->forward_menuitem, FALSE);
   return 0;
}

MWContext *
FE_MakeBlankWindow(MWContext *old_context,
		   URL_Struct *url,
		   char *window_name)
{
   XP_ASSERT(0);
   return 0;
}

void
FE_SetWindowLoading(MWContext *context,
		    URL_Struct *url,
		    Net_GetUrlExitFunc **exit_func)
{
   XP_ASSERT(0);
}

void
FE_RaiseWindow(MWContext *context)
{
  GtkWidget *widget;
  XP_ASSERT (context);

  widget = CONTEXT_WIDGET (context);
  if (!GTK_WIDGET_NO_WINDOW (widget))
      gdk_window_raise (widget->window);

}

void
FE_ConnectToRemoteHost(MWContext* ctxt,
		       int url_type,
		       char* hostname,
		       char* port,
		       char* username)
{
   XP_ASSERT(0);
}

void*
FE_AboutData(const char* which,
	     char** data_ret,
	     int32* length_ret,
	     char** content_type_ret)
{
   XP_ASSERT(0);
   return 0;
}

void
FE_FreeAboutData(void* data,
		 const char* which)
{
   XP_ASSERT(0);
}



/* Just return false for now, since NetCaster can't possibly be installed... */

XP_Bool
FE_IsNetcasterInstalled()
{
   return 0;
}


ContextFuncs *
GTKFE_BuildDisplayFunctionTable(void)
{
  return &gtkfe_context_funcs;
}

