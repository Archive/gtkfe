#ifndef __GTK_MALLOC_H__
#define __GTK_MALLOC_H__ 1

void *GTKFE_ALLOC( int size );
void GTKFE_FREE( void *pointer );
void *GTKFE_REALLOC( int *pointer, int size );
void *GTKFE_ALLOC_ZAP( int size );
#define GTKFE_NEW(type) ( (type *) GTKFE_ALLOC_ZAP( sizeof( type ) ) )
void *GTKFE_REALLOC_ZAP( int *pointer, int size );

#endif
