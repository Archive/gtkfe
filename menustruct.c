#ifdef HAVE_GNOME
#include <gnome.h>
#else
#include <gtk/gtk.h>
#endif
#include "gtkfe.h"

#if 0
#include "logo.xpm"
#include "tb_help.xpm"
#include "tb_home.xpm"
#include "tb_left_arrow.xpm"
#include "tb_new.xpm"
#include "tb_print.xpm"
#include "tb_right_arrow.xpm"
#include "tb_search.xpm"
#include "tb_stop.xpm"
#endif

/* The menu definitions: File/Exit and Help/About are mandatory.
 * We should probably allow both NetScape compatible and "standard"
 * keysyms to be used? [VH]
 */



/* Gnome UI version... */

/* Callback declarations */
void GTKFE_about_callback (GtkWidget *widget, void *data);

/* navigation callbacks */
void gtkfe_BackCallback(GtkWidget *widget);
void gtkfe_ForwardCallback(GtkWidget *widget);
void gtkfe_HomeCallback(GtkWidget *widget);
void gtkfe_EditCallback(GtkWidget *widget);
void gtkfe_ReloadCallback(GtkWidget *widget);
void gtkfe_FindCallback(GtkWidget *widget);
void gtkfe_StopCallback(GtkWidget *widget);

/* file menu callbacks */
void gtkfe_NewWindowCallback(GtkWidget *widget);
void gtkfe_OpenPageCallback(GtkWidget *widget);
void gtkfe_SaveAsCallback(GtkWidget *widget);
void gtkfe_SaveFrameAsCallback(GtkWidget *widget);
void gtkfe_UploadFileCallback(GtkWidget *widget);
void gtkfe_PrintDocumentCallback(GtkWidget *widget);
void gtkfe_CloseCallback(GtkWidget *widget);
/* edit menu callbacks */
void gtkfe_UndoCallback(GtkWidget *widget);
void gtkfe_RedoCallback(GtkWidget *widget);
void gtkfe_CutCallback(GtkWidget *widget);
void gtkfe_CopyCallback(GtkWidget *widget);
void gtkfe_PasteCallback(GtkWidget *widget);
void gtkfe_FindInDocumentCallback(GtkWidget *widget);
void gtkfe_EditPreferencesCallback(GtkWidget *widget);
/* view menu callbacks */
void gtkfe_ShowNavibarCallback(GtkWidget *widget);
void gtkfe_ShowPersonalToolbarCallback(GtkWidget *widget);
void gtkfe_ShowLocationbarCallback(GtkWidget *widget);
void gtkfe_HideMenubarCallback(GtkWidget *widget);


#ifdef HAVE_GNOME

/* Our item macro for using stock Gnome pixmaps... */

#define GNOMEUIINFO_SITEM(label, tip, cb, xpm) \
{GNOME_APP_UI_ITEM, label, tip, cb, \
                               NULL, NULL, GNOME_APP_PIXMAP_STOCK, xpm, \
                               0, 0, NULL \
}         

/* Our item macro for loading pixmaps on startup (as opposed to compiling them into the app)... */

#define GNOMEUIINFO_FITEM(label, tip, cb, xpm) \
{GNOME_APP_UI_ITEM, label, tip, cb, \
                               NULL, NULL, GNOME_APP_PIXMAP_FILENAME, xpm, \
                               0, 0, NULL \
}         



GnomeUIInfo mozilla_file_menu[] = {
   GNOMEUIINFO_SITEM("New Mozilla window","Open a new Mozilla window",gtkfe_NewWindowCallback,GNOME_STOCK_MENU_NEW),
   GNOMEUIINFO_SITEM("Open page","Open a new page in this window",gtkfe_OpenPageCallback,GNOME_STOCK_MENU_OPEN),
   GNOMEUIINFO_SEPARATOR,
   GNOMEUIINFO_SITEM("Save As...","Save current document as...",gtkfe_SaveAsCallback,GNOME_STOCK_MENU_SAVE_AS),
   GNOMEUIINFO_SITEM("Save Frame As...","Save current document frame as...",gtkfe_SaveFrameAsCallback,GNOME_STOCK_MENU_SAVE_AS),
   GNOMEUIINFO_SEPARATOR,
   GNOMEUIINFO_SITEM("Upload file","Upload a file",gtkfe_UploadFileCallback,0),
   GNOMEUIINFO_SEPARATOR,
   GNOMEUIINFO_SITEM("Print...","Print the current document",gtkfe_PrintDocumentCallback,GNOME_STOCK_MENU_PRINT),
   GNOMEUIINFO_SEPARATOR,
   GNOMEUIINFO_SITEM("Close","Close the current window",gtkfe_CloseCallback,0),
   GNOMEUIINFO_SITEM("Exit","Exit Mozilla/GTK application and close all windows",gtkfe_QuitCallback,GNOME_STOCK_MENU_EXIT),
   GNOMEUIINFO_END
};

GnomeUIInfo mozilla_edit_menu[] = {
   GNOMEUIINFO_SITEM("Undo",0,gtkfe_UndoCallback,GNOME_STOCK_MENU_UNDO),
   GNOMEUIINFO_SITEM("Redo",0,gtkfe_RedoCallback,0),
   GNOMEUIINFO_SEPARATOR,
   GNOMEUIINFO_SITEM("Cut",0,gtkfe_CutCallback,GNOME_STOCK_MENU_CUT),
   GNOMEUIINFO_SITEM("Copy",0,gtkfe_CopyCallback,GNOME_STOCK_MENU_COPY),
   GNOMEUIINFO_SITEM("Paste",0,gtkfe_PasteCallback,GNOME_STOCK_MENU_PASTE),
   GNOMEUIINFO_SEPARATOR,
   GNOMEUIINFO_SITEM("Find in document...",0,gtkfe_FindInDocumentCallback,GNOME_STOCK_MENU_SEARCH),
   GNOMEUIINFO_SITEM("Edit preferences...",0,gtkfe_EditPreferencesCallback,GNOME_STOCK_MENU_PREF),
   GNOMEUIINFO_END
};

GnomeUIInfo mozilla_view_menu[] = {
   GNOMEUIINFO_SITEM("Show Navigation bar",0,gtkfe_ShowNavibarCallback,0),
   GNOMEUIINFO_SITEM("Show Location bar",0,gtkfe_ShowLocationbarCallback,0),
   GNOMEUIINFO_SITEM("Show Personal toolbar",0,gtkfe_ShowPersonalToolbarCallback,0),
   GNOMEUIINFO_SITEM("Hide Menubar",0,gtkfe_HideMenubarCallback,0),
   GNOMEUIINFO_END
};

GnomeUIInfo mozilla_go_menu[] = {
#define MENU_GO_BACK_POS 0
   GNOMEUIINFO_SITEM("Back",0,gtkfe_BackCallback,0),
#define MENU_GO_FORWARD_POS 1
   GNOMEUIINFO_SITEM("Forward",0,gtkfe_ForwardCallback,0),
#define MENU_GO_HOME_POS 2
   GNOMEUIINFO_SITEM("Home",0,gtkfe_HomeCallback,0),
   GNOMEUIINFO_END
};

GnomeUIInfo mozilla_window_menu[] = {
   GNOMEUIINFO_SITEM("Mozilla",0,0,0),
   GNOMEUIINFO_END
};

GnomeUIInfo mozilla_help_menu[] = {
   GNOMEUIINFO_SITEM(N_("About"),N_("About Mozilla/GTK"),GTKFE_about_callback, GNOME_STOCK_MENU_ABOUT),
   GNOMEUIINFO_SEPARATOR,
   GNOMEUIINFO_HELP("mozilla"),
   GNOMEUIINFO_END
};

GnomeUIInfo mozilla_menu[] = {
   GNOMEUIINFO_SUBTREE("File",mozilla_file_menu),
   GNOMEUIINFO_SUBTREE("Edit",mozilla_edit_menu),
   GNOMEUIINFO_SUBTREE("View",mozilla_view_menu),
   GNOMEUIINFO_SUBTREE("Go",mozilla_go_menu),
   GNOMEUIINFO_SUBTREE("Window",mozilla_window_menu),
   GNOMEUIINFO_SUBTREE("Help",mozilla_help_menu),
   GNOMEUIINFO_END
};





GnomeUIInfo mozilla_toolbar[] = {
#define TOOLBAR_BACK_POS 0
   GNOMEUIINFO_FITEM(N_("Back"), 
		    N_("Go to the previous location in the history list"),
		    gtkfe_BackCallback, "tb_left_arrow.xpm"),
#define TOOLBAR_FORWARD_POS 1
   GNOMEUIINFO_FITEM(N_("Forward"),
		    N_("Go to the next location in the history list"),
		    gtkfe_ForwardCallback, "tb_right_arrow.xpm"),
#define TOOLBAR_HOME_POS 2
   GNOMEUIINFO_FITEM(N_("Home"), N_("Go to the preset homepage"), 
		    gtkfe_HomeCallback, "tb_home.xpm"),
   GNOMEUIINFO_SEPARATOR,
   GNOMEUIINFO_FITEM(N_("Edit"), N_("Edit a document"), gtkfe_EditCallback, "tb_new.xpm"),
   GNOMEUIINFO_SEPARATOR,
   GNOMEUIINFO_FITEM(N_("Reload"), N_("Reload the current document"),gtkfe_ReloadCallback,"tb_help.xpm"  ), /* FIXME, reload */ 
   GNOMEUIINFO_FITEM(N_("Find"), N_("Find text in current document"),
		    gtkfe_FindCallback, "tb_search.xpm"),
#define TOOLBAR_ABORT_POS 8
   GNOMEUIINFO_FITEM(N_("Stop"), N_("Stop loading the current document"),
		    gtkfe_StopCallback, "tb_stop.xpm"),
   GNOMEUIINFO_END
};

#endif

void
gtkfe_CreateMenus (MWContext *context, fe_ContextData *cd) {
#ifdef HAVE_GNOME
  gnome_app_create_menus_with_data (GNOME_APP (cd->main), mozilla_menu, context);

  cd->back_menuitem = mozilla_go_menu[MENU_GO_BACK_POS].widget;
  cd->forward_menuitem = mozilla_go_menu[MENU_GO_FORWARD_POS].widget;
  cd->home_menuitem = mozilla_go_menu[MENU_GO_HOME_POS].widget;
#endif /* HAVE_GNOME */
}

void
gtkfe_CreateToolbar(MWContext *context, fe_ContextData *cd) {
#ifdef HAVE_GNOME
  gnome_app_create_toolbar (GNOME_APP (cd->main), mozilla_toolbar);

  cd->back_button = mozilla_toolbar[TOOLBAR_BACK_POS].widget;
  cd->forward_button = mozilla_toolbar[TOOLBAR_FORWARD_POS].widget;
  cd->home_button = mozilla_toolbar[TOOLBAR_HOME_POS].widget;
  cd->abort_button = mozilla_toolbar[TOOLBAR_ABORT_POS].widget;

#else /* HAVE_GNOME */
  /* The gtk case is a mess, the location box is nothing again... :( */
  GdkPixmap *gdkpm=0;
  GdkBitmap *bitmap=0;
  GdkColor c;
  GtkWidget *pmhome=0, *pmback=0, *pmforward=0, *pmedit=0, *pmfind=0, *pmreload=0,*pmstop=0;

   /* vbox for toolbars etc.. */
   cd->toolbarbox = gtk_vbox_new(FALSE,0);
  
#  ifndef HAVE_GDK_IMLIB

  gdkpm=gdk_pixmap_create_from_xpm_d(cd->main->window,
				     &bitmap,
				     &c,
				     tb_left_arrow_xpm);
  if (gdkpm&&bitmap) pmback = gtk_pixmap_new(gdkpm, bitmap);

  gdkpm=gdk_pixmap_create_from_xpm_d(cd->main->window,
				     &bitmap,
				     &c,
				     tb_right_arrow_xpm);
    if (gdkpm&&bitmap) pmforward = gtk_pixmap_new(gdkpm, bitmap);

  gdkpm=gdk_pixmap_create_from_xpm_d(cd->main->window,
				     &bitmap,
				     &c,
				     tb_home_xpm);
  if (gdkpm&&bitmap)   pmhome = gtk_pixmap_new(gdkpm, bitmap);

  gdkpm=gdk_pixmap_create_from_xpm_d(cd->main->window,
				     &bitmap,
				     &c,
				     tb_new_xpm);
  if (gdkpm&&bitmap)   pmedit = gtk_pixmap_new(gdkpm, bitmap);

  gdkpm=gdk_pixmap_create_from_xpm_d(cd->main->window,
				     &bitmap,
				     &c,
				     tb_help_xpm); /* FIX: tb_reload_xpm */
  if (gdkpm&&bitmap)   pmreload = gtk_pixmap_new(gdkpm, bitmap);

  gdkpm=gdk_pixmap_create_from_xpm_d(cd->main->window,
				     &bitmap,
				     &c,
				     tb_search_xpm);
  if (gdkpm&&bitmap)   pmfind = gtk_pixmap_new(gdkpm, bitmap);

  gdkpm=gdk_pixmap_create_from_xpm_d(cd->main->window,
				     &bitmap,
				     &c,
				     tb_stop_xpm);
  if (gdkpm&&bitmap)   pmstop = gtk_pixmap_new(gdkpm, bitmap);

#  else
  gdk_imlib_data_to_pixmap(tb_left_arrow_xpm, &gdkpm, &bitmap);
  if (gdkpm&&bitmap) pmback = gtk_pixmap_new(gdkpm, bitmap);

  gdk_imlib_data_to_pixmap(tb_right_arrow_xpm, &gdkpm, &bitmap);
    if (gdkpm&&bitmap) pmforward = gtk_pixmap_new(gdkpm, bitmap);

  gdk_imlib_data_to_pixmap(tb_home_xpm, &gdkpm, &bitmap);
  if (gdkpm&&bitmap)   pmhome = gtk_pixmap_new(gdkpm, bitmap);

  gdk_imlib_data_to_pixmap(tb_new_xpm, &gdkpm, &bitmap);
  if (gdkpm&&bitmap)   pmedit = gtk_pixmap_new(gdkpm, bitmap);

  gdk_imlib_data_to_pixmap(tb_help_xpm, &gdkpm, &bitmap); /* FIXME: tb_reload_xpm */
  if (gdkpm&&bitmap)   pmreload = gtk_pixmap_new(gdkpm, bitmap);

  gdk_imlib_data_to_pixmap(tb_search_xpm, &gdkpm, &bitmap);
  if (gdkpm&&bitmap)   pmfind = gtk_pixmap_new(gdkpm, bitmap);

  gdk_imlib_data_to_pixmap(tb_stop_xpm, &gdkpm, &bitmap);
  if (gdkpm&&bitmap)   pmstop = gtk_pixmap_new(gdkpm, bitmap);
  

#  endif

  cd->toolbar = gtk_toolbar_new(GTK_ORIENTATION_HORIZONTAL,GTK_TOOLBAR_BOTH);
  /*  
  gtk_toolbar_append_space(GTK_TOOLBAR(cd->toolbar));
  gtk_toolbar_append_space(GTK_TOOLBAR(cd->toolbar));
  */
  gtk_table_attach (GTK_TABLE(cd->contenttable), cd->toolbar, 
		    0, 3, 1, 2, /* left, right, top, bottom attach */
		    GTK_FILL,GTK_FILL, /* x/y options */
		    0,3 /* padding */
		    );
  
  
  cd->back_button = gtk_toolbar_append_item(GTK_TOOLBAR(cd->toolbar),
					    "Back","Previous Document","",
					    pmback,gtkfe_BackCallback,0);
  
  cd->forward_button = gtk_toolbar_append_item(GTK_TOOLBAR(cd->toolbar),
					       "Forward","Next Document","",
					       pmforward,gtkfe_ForwardCallback,0);
  
  cd->home_button = gtk_toolbar_append_item(GTK_TOOLBAR(cd->toolbar),
					    "Home","Go to home page","",
					    pmhome,gtkfe_HomeCallback,0);
  
  gtk_toolbar_append_space(GTK_TOOLBAR(cd->toolbar));
  
  gtk_toolbar_append_item(GTK_TOOLBAR(cd->toolbar),
			  "Edit","Edit the document","",
			  pmedit,gtkfe_EditCallback,0);
  
  gtk_toolbar_append_space(GTK_TOOLBAR(cd->toolbar));
  
  gtk_toolbar_append_item(GTK_TOOLBAR(cd->toolbar),
			  "Reload","Reload the current document","",
			  pmreload,gtkfe_ReloadCallback,0);
  
  gtk_toolbar_append_item(GTK_TOOLBAR(cd->toolbar),
			  "Find","Search string from current document","",
			  pmfind,gtkfe_FindCallback,0);
  
  gtk_toolbar_append_space(GTK_TOOLBAR(cd->toolbar));
  cd->abort_button = gtk_toolbar_append_item(GTK_TOOLBAR(cd->toolbar),
					     "Stop","Interrupt loading","",
					     pmstop,gtkfe_StopCallback,0);
  
  gtk_widget_show(cd->toolbar);

  cd->logo_mask = NULL;
  cd->logo_style = gtk_widget_get_style(cd->main);
  cd->logo_pixmap = gdk_pixmap_create_from_xpm(cd->main->window, &cd->logo_mask,
					       &cd->logo_style->bg[GTK_STATE_NORMAL],
					       "logo.xpm");
  cd->logoimage = gtk_pixmap_new(cd->logo_pixmap, cd->logo_mask);
  
  gtk_table_attach (GTK_TABLE(cd->contenttable), cd->logoimage, 
		    3, 4, 1, 3, /* left, right, top, bottom attach */
		    GTK_FILL,GTK_FILL, /* x/y options */
		    0, 0 /* x/y fill */); 


#endif
}












