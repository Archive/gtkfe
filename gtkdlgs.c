/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public License
 * Version 1.0 (the "NPL"); you may not use this file except in
 * compliance with the NPL.  You may obtain a copy of the NPL at
 * http://www.mozilla.org/NPL/
 *
 * Software distributed under the NPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the NPL
 * for the specific language governing rights and limitations under the
 * NPL.
 *
 * The Initial Developer of this code under the NPL is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation.  All Rights
 * Reserved.
 */

/* 
 * gtkdlgs.c --- gtk fe handling of dialog requests from backend (prompt for
 *                 password, alerts, messages, etc.)
 *
 *  Modified:
 *   1998/04/02 Vidar Hokstad
 *        Added lots of debugging output and default actions to facilitate writing
 *        the rest of the FE. Integrated some very preliminary first
 *        GTK/Gnome stuff from Havoc Pennington.
 *
 *   1998/04/05 Vidar Hokstad
 *        Fixed some prototypes. Fixed FE_Prompt to return a char pointer instead of TRUE.
 *
 *   1998/04/06 Vidar Hokstad
 *        Oops. FE_Confirm() and FE_Prompt() was mixed up... That's why I changed the
 *        pointer above. Changed it back... Must learn to read the comments before
 *        I start messing up the source :)
 * 
 *  FIXME:
 * 
 *    - The Gnome dialog functions doesn't handle multiline texts very gracefully.... At least
 *      not large ones...
 */

#include "xp_core.h"
#include "structs.h"
#include "ntypes.h"
#include "gtkfe.h"

#ifdef HAVE_GNOME

#include <gnome.h>
#include <libgnomeui/gnome-messagebox.h>
#define INVALID_BUTTON -1

static void ButtonClicked(GtkWidget * messagebox, 
                           gint button_number,
                           gpointer store_number_here );

#endif /* HAVE_GNOME */

#undef Bool
#include "fe_proto.h"


/*
** FE_Alert - put up an alert dialog containing the given msg, over
** context.
**
** This method should return immediately, without waiting for user
** input.
*/
void
FE_Alert(MWContext *context,
	 const char *msg)
{
#ifdef HAVE_GNOME
   GtkWidget * dialog;
#endif
   
   XP_ASSERT(msg);

#ifdef HAVE_GNOME
   /* FIXME Do something with the context */

   dialog = gnome_message_box_new( msg, GNOME_MESSAGE_BOX_WARNING,
				  GNOME_STOCK_BUTTON_OK,0 );

   gtk_window_set_title(GTK_WINDOW(dialog), "Mozilla/GNOME");           
   gtk_widget_show(dialog);
#else /* HAVE_GNOME */

   XP_ASSERT(0);
   printf("FE_Alert: '%s'\n",msg);
#endif
}

/*
** FE_Message - put up an information dialog containing the given msg,
** over context.
**
** This method should return immediately, without waiting for user
** input.
*/
void
FE_Message(MWContext *context,
	   const char *msg)
{
#ifdef HAVE_GNOME
   GtkWidget * dialog;
   
   /* FIXME don't ignore the context */
   
   dialog = gnome_message_box_new( msg,
				  GNOME_MESSAGE_BOX_INFO,
				  GNOME_STOCK_BUTTON_OK,0 );
   gtk_window_set_title(GTK_WINDOW(dialog), "Mozilla/GNOME");           
   gtk_widget_show(dialog);
#else /* HAVE_GNOME */
   XP_ASSERT(0);
   /*printf("FE_Message: %s\n",msg);*/
#endif
}


/*
** FE_Confirm - Put up an confirmation dialog (with Yes and No
** buttons) and return TRUE if Yes/Ok was clicked and FALSE if
** No/Cancel was clicked.
**
** This method should not return until the user has clicked on one of
** the buttons.
*/
Bool
FE_Confirm(MWContext *context,
	   const char * msg)
{
#ifdef HAVE_GNOME
   GtkWidget * dialog;
   gint which_button = INVALID_BUTTON;
   
   /* FIXME context */
   
   dialog = gnome_message_box_new( msg, 
				  GNOME_MESSAGE_BOX_QUESTION,
				  GNOME_STOCK_BUTTON_YES,
				  GNOME_STOCK_BUTTON_NO,0 );
   
   gtk_window_set_title(GTK_WINDOW(dialog), "Mozilla/GNOME Confirmation");
   
   gtk_signal_connect( GTK_OBJECT(dialog), "clicked",
		      GTK_SIGNAL_FUNC(ButtonClicked),
		      &which_button );
   
   gtk_widget_show(dialog);
   
   /* FIXME I imagine gtk_main_iteration() won't really
    do, but maybe. */
   while ( which_button == INVALID_BUTTON ) {
      gtk_main_iteration();
   }
   
   /* Yes */
   if ( which_button == 0 ) {
      return TRUE;
   }
   /* No */
   else if ( which_button == 1 ) {
      return FALSE;
   }
   /* Huh? */
   else {
      g_assert_not_reached();
      return FALSE; /* for compiler warnings */
   }
   
#else /* HAVE_GNOME */
   XP_ASSERT(0);
   XP_ASSERT(0);
   printf("FE_Confirm: '%s'\n",msg);
   return 0;
#endif
}

/*
** FE_Prompt - Put up a prompt dialog with the given message, a text
** field (with the value defaulted to dflt).  The user's response
** should be returned.
**
** This method should not return until the user has clicked Ok (the
** return value should not be NULL) or they clicked Cancel (the return
** value should be NULL.)
*/
char *
FE_Prompt(MWContext *context,
	  const char *msg,
	  const char *dflt)
{
   printf("FE_Prompt: '%s' ('%s' default)\n",msg,dflt);
   return 0;
}

/*
** FE_PromptPassword - Put up a prompt dialog with the given message,
** a text field.  The user's response should be returned.
**
** The text field should not show the characters as the user types them.
** Display them as X's, *'s, spaces, etc.
**
** This method should not return until the user has clicked Ok (the
** return value should not be NULL) or they clicked Cancel (the return
** value should be NULL.)
*/
char*
FE_PromptPassword(MWContext *context,
		  const char *msg)
{
#ifdef HAVE_GNOME
  GtkWidget *dialog;
  GtkWidget *vbox;
  GtkWidget *entry;
  gint which_button = INVALID_BUTTON;
   
  /* FIXME context */
  
  dialog = gnome_message_box_new( msg, 
				  GNOME_MESSAGE_BOX_QUESTION,
				  GNOME_STOCK_BUTTON_OK,
				  GNOME_STOCK_BUTTON_CANCEL,0 );
  
  gnome_dialog_set_modal (GNOME_DIALOG (dialog));
   
  gtk_window_set_title(GTK_WINDOW(dialog), "Mozilla/GNOME");           
   
  gtk_signal_connect( GTK_OBJECT(dialog), "clicked",
                      GTK_SIGNAL_FUNC(ButtonClicked),
                      &which_button );
   
  vbox = GNOME_DIALOG (dialog)->vbox;
 
   /* the entry widget */
   entry = gtk_entry_new ();
   gtk_widget_ref (entry);
   gtk_entry_set_visibility (GTK_ENTRY (entry), FALSE);
   
   /*
     This should atually eventually set which_button = 1 etc
     but not now [LE]

     gtk_signal_connect (GTK_OBJECT (entry), "activate",
                       GTK_SIGNAL_FUNC (ButtonClicked),
                       entry);
   */
   gtk_box_pack_end (GTK_BOX (vbox), entry, TRUE, TRUE, 0);

   gtk_widget_show (entry);
   gtk_widget_show(dialog);
   
   
   /* FIXME I imagine gtk_main_iteration() won't really
    do, but maybe. */
   while ( which_button == INVALID_BUTTON ) {
      gtk_main_iteration();
   }
   
   /* Okay */
   if ( which_button == 0 ) {
      char *entry_str = strdup (gtk_entry_get_text (GTK_ENTRY (entry)));
      gtk_widget_unref (entry);
      return entry_str;
   }
   /* Cancel */
   else if ( which_button == 1 ) {
      gtk_widget_unref (entry);
      return NULL;
   }
   /* Huh? */
   else {
      gtk_widget_unref (entry);
      g_assert_not_reached();
      return NULL; /* for compiler warnings */
   }
#else /* HAVE_GNOME */
   XP_ASSERT(0);
   printf("FE_PromptPassword: %s\n",msg);
   return 0;
#endif /* HAVE_GNOME */
}

/*
** FE_PromptMessageSubject - Put up a prompt dialog with the given
** message, a text field.  The user's response should be returned.
**
** The default value in the text field should be "(No Subject)",
** localized to the user's locale.
**
** This method should not return until the user has clicked Ok (the
** return value should not be NULL) or they clicked Cancel (the return
** value should be NULL.)
*/
char*
FE_PromptMessageSubject(MWContext *context)
{
#ifdef HAVE_GNOME
  GtkWidget *dialog;
  GtkWidget *vbox;
  GtkWidget *entry;
  gint which_button = INVALID_BUTTON;
   
  /* FIXME context */
  
  dialog = gnome_message_box_new( "FIX_ME", 
				  GNOME_MESSAGE_BOX_QUESTION,
				  GNOME_STOCK_BUTTON_OK,
				  GNOME_STOCK_BUTTON_CANCEL,0 );
  
  gnome_dialog_set_modal (GNOME_DIALOG (dialog));
   
  gtk_window_set_title(GTK_WINDOW(dialog), "Mozilla/GNOME");           
   
  gtk_signal_connect( GTK_OBJECT(dialog), "clicked",
                      GTK_SIGNAL_FUNC(ButtonClicked),
                      &which_button );
   
  vbox = GNOME_DIALOG (dialog)->vbox;

   /* the entry widget */
   entry = gtk_entry_new ();
   gtk_widget_ref (entry);
   /* FIXME localize this */
   gtk_entry_set_text (GTK_ENTRY (entry), "(No Subject)");
   /*
     gtk_signal_connect (GTK_OBJECT (entry), "activate",
                       GTK_SIGNAL_FUNC (text_enter_cb),
                       entry);
   */
   gtk_box_pack_end (GTK_BOX (vbox), entry, TRUE, TRUE, 0);

   gtk_widget_show (entry);
   gtk_widget_show(dialog);
   
   
   /* FIXME I imagine gtk_main_iteration() won't really
    do, but maybe. */
   while ( which_button == INVALID_BUTTON ) {
      gtk_main_iteration();
   }
   
   /* Okay */
   if ( which_button == 0 ) {
      char *entry_str = strdup (gtk_entry_get_text (GTK_ENTRY (entry)));
      gtk_widget_unref (entry);
      return entry_str;
   }
   /* Cancel */
   else if ( which_button == 1 ) {
      gtk_widget_unref (entry);
      return NULL;
   }
   /* Huh? */
   else {
      gtk_widget_unref (entry);
      g_assert_not_reached();
      return NULL; /* for compiler warnings */
   }
#else  /* HAVE_GNOME */
   XP_ASSERT(0);
   printf ("FE_PromptMessageSubject()\n");
   return 0;
#endif /* HAVE_GNOME */
}

/*
** FE_PromptUsernameAndPassword - Put up a prompt dialog with the given
** message, a two text fields.  It should return TRUE if Ok was clicked
** and FALSE if Cancel was clicked.
**
** The password text field should not show the characters as the user
** types them.  Display them as X's, *'s, spaces, etc.
**
** This method should not return until the user has clicked Ok or Cancel.
*/
Bool
FE_PromptUsernameAndPassword(MWContext *context,
			     const char *message,
			     char **username,
			     char **password)
{
#ifdef HAVE_GNOME
  GtkWidget *dialog;
  GtkWidget *vbox;
  GtkWidget *password_entry;
  GtkWidget *username_entry;
  gint which_button = INVALID_BUTTON;
   
  /* FIXME context */
  
  dialog = gnome_message_box_new( message, 
				  GNOME_MESSAGE_BOX_QUESTION,
				  GNOME_STOCK_BUTTON_OK,
				  GNOME_STOCK_BUTTON_CANCEL,0 );
  
  gnome_dialog_set_modal (GNOME_DIALOG (dialog));
   
  gtk_window_set_title(GTK_WINDOW(dialog), "Mozilla/GNOME");           
   
  gtk_signal_connect( GTK_OBJECT(dialog), "clicked",
                      GTK_SIGNAL_FUNC(ButtonClicked),
                      &which_button );
   
  vbox = GNOME_DIALOG (dialog)->vbox;
 
   /* the entry widget */
   username_entry = gtk_entry_new ();
   gtk_widget_ref (username_entry);
   gtk_box_pack_start (GTK_BOX (vbox), username_entry, TRUE, TRUE, 0);
   gtk_widget_show (username_entry);

   password_entry = gtk_entry_new ();
   gtk_widget_ref (password_entry);
   gtk_entry_set_visibility (GTK_ENTRY (password_entry), FALSE);
   
   /*
     This should atually eventually set which_button = 1 etc
     but not now [LE]

     gtk_signal_connect (GTK_OBJECT (entry), "activate",
                       GTK_SIGNAL_FUNC (button_clicked_cb),
                       entry);
   */
   gtk_box_pack_start (GTK_BOX (vbox), password_entry, TRUE, TRUE, 0);
   gtk_widget_show (password_entry);
   
   gtk_widget_show(dialog);
   
   
   /* FIXME I imagine gtk_main_iteration() won't really
    do, but maybe. */
   while ( which_button == INVALID_BUTTON ) {
      gtk_main_iteration();
   }
   
   /* Okay */
   if ( which_button == 0 ) {
      *password = strdup (gtk_entry_get_text (GTK_ENTRY (password_entry))); 
      *username = strdup (gtk_entry_get_text (GTK_ENTRY (username_entry)));
      gtk_widget_unref (password_entry);
      gtk_widget_unref (username_entry);
      return TRUE;
   }
   /* Cancel */
   else if ( which_button == 1 ) {
      gtk_widget_unref (password_entry);
      gtk_widget_unref (username_entry);
      return FALSE;
   }
   /* Huh? */
   else {
      gtk_widget_unref (password_entry);
      gtk_widget_unref (username_entry);
      g_assert_not_reached();
      return FALSE; /* for compiler warnings */
   }
#else /* HAVE_GNOME */
   XP_ASSERT (0);
   printf("FE_PrompUserNameAndPassword: '%s'\n",message);
   return 0;
#endif /* HAVE_GNOME */
}


/*
** FE_PromptForFileNam - Put up a file selection dialog with the given
** prompt string, 
**
** the file selection box should open up viewing default_path.
**
** if file_must_exist_p, the user should not be allowed to close the
** dialog with an invalid path selected.
**
** if directories_allowed_p, directories can be selected.
**
** After the user has clicked ok or cancel or given some other gesture
** to bring down the filesb, the ReadFileNameCallbackFunction should
** be called with the context, the textual path name, and the closure, as in
** 'fn(context, path, closure);'
**
** Lastly, the function should return 0 if the path was acceptable and -1 if it
** was not.
**
** This method should not return until the user has clicked Ok or Cancel.
*/
int
FE_PromptForFileName(MWContext *context,
		     const char *prompt_string,
		     const char *default_path,
		     XP_Bool file_must_exist_p,
		     XP_Bool directories_allowed_p,
		     ReadFileNameCallbackFunction fn,
		     void *closure)
{
   XP_ASSERT(0);
   printf("FE_PromptForFileName: '%s' (default path '%s')\n",prompt_string,default_path);
   return 0;
}

/*
** FE_SaveDialog - Put up a dialog that basically says "I'm saving
** <foo> right now", cancel?
**
** This function should not block, but should return after putting up the dialog.
**
** If the user clicks cancel, the callback should call EDT_SaveCancel.
**
** Note: This function has been overloaded for use in publishing as well.  There
** are three instances where this function will be called:
**   1) Saving remote files to disk.
**   2) Preparing to publish files remotely.
**   3) Publishing files to a remote server.
*/
void
FE_SaveDialogCreate(MWContext *context,
		    int file_count,
		    ED_SaveDialogType save_type)
{
   XP_ASSERT(0);
   printf("FE_SaveDialogCreate()\n");
   /* Should maybe call EDT_SaveCancel here until something is implemented...  */
}

/*
** FE_SaveDialogSetFilename - for a save dialog that has been put up above the given
** context, set the filename being saved/published.
*/
void
FE_SaveDialogSetFilename(MWContext *context,
			 char *filename)
{
   XP_ASSERT(0);
   printf("FE_SaveDialogSetFilename: %s\n",filename);
}

/*
** FE_SaveDialogDestroy - the backend calls this function to let us
** know that the save/publish operation has completed.  We should
** destroy the save dialog that has been used above the given context.
*/
void
FE_SaveDialogDestroy(MWContext *context,
		     int status,
		     char *filename)
{
   XP_ASSERT(0);
   printf("FE_SaveDialogDestroy()\n");
}

/*
** FE_SaveFileExistsDialog - put up the standard dialog saying:
** "<foo> exists - overwrite?"  Yes to All, Yes, No, No to All.
**
** return ED_SAVE_OVERWRITE_THIS if the user clicks Yes.
** return ED_SAVE_OVERWRITE_ALL if the user clicks Yes to All.
** return ED_SAVE_DONT_OVERWRITE_THIS if the user clicks No.
** return ED_SAVE_DONT_OVERWRITE_ALL if the user clicks No to All.
*/
ED_SaveOption
FE_SaveFileExistsDialog(MWContext *context,
			char *filename)
{
   XP_ASSERT(0);
   printf("FE_SaveFileExistsDialog()\n");
   return ED_SAVE_DONT_OVERWRITE_ALL;
}

/*
** FE_SaveErrorContinueDialog - put up a dialog that gives some
** textual represenation of the error status, and allow the user
** to decide if they want to continue or not.
**
** Return TRUE if we should continue, and FALSE if we shouldn't.
*/
Bool
FE_SaveErrorContinueDialog(MWContext *context,
			   char *filename,
			   ED_FileError error)
{
   XP_ASSERT(0);
   printf("FE_SaveErrorContinueDialog()\n");
   return 0;
}




/***********************************************************
 Gnome Callbacks 
 *******************************************************/

#ifdef HAVE_GNOME
static void
ButtonClicked(GtkWidget * messagebox, 
              gint button_number,
              gpointer store_number_here )
{
   /* Do we use glib or XP for these things? */
   g_assert( *((gint *)store_number_here) == INVALID_BUTTON );
   XP_ASSERT( *((gint *)store_number_here) == INVALID_BUTTON );
   
   *((gint *)store_number_here) = button_number;
   
}

#endif /* HAVE_GNOME */


static void OpenPageDialog_file_sel_ok_callback( GtkWidget *widget, gpointer data )
{
  gtk_entry_set_text( GTK_ENTRY( data ),
                      gtk_file_selection_get_filename( GTK_FILE_SELECTION( gtk_widget_get_toplevel( widget ) ) ) );
  gtk_widget_destroy( gtk_widget_get_toplevel( widget ) );
}

static void OpenPageDialog_file_sel_cancel_callback( GtkWidget *widget, gpointer data )
{
  gtk_widget_destroy( gtk_widget_get_toplevel( widget ) );
}

static void OpenPageDialog_choose_callback( GtkWidget *widget, gpointer data )
{
  GtkWidget *entry = gtk_object_get_data( GTK_OBJECT( gtk_widget_get_toplevel( widget ) ), "entry" );
  GtkWidget *selection = gtk_file_selection_new( "Choose a file" );
  gtk_signal_connect( GTK_OBJECT( GTK_FILE_SELECTION( selection )->ok_button ), "clicked", 
                      GTK_SIGNAL_FUNC( OpenPageDialog_file_sel_ok_callback ),
                      entry );
  gtk_signal_connect( GTK_OBJECT( GTK_FILE_SELECTION( selection )->cancel_button ), "clicked", 
                      GTK_SIGNAL_FUNC( OpenPageDialog_file_sel_cancel_callback ),
                      entry );
  gtk_widget_show( selection );
}

static void OpenPageDialog_navigator_callback( GtkWidget *widget, gpointer data )
{
  GtkWidget *dialog = gtk_widget_get_toplevel( widget );
  GtkWidget *entry = gtk_object_get_data( GTK_OBJECT( dialog ), "entry" );
  URL_Struct *url;
  
  url = NET_CreateURLStruct( gtk_entry_get_text( GTK_ENTRY( entry ) ), NET_NORMAL_RELOAD );
  FE_GetURL( (MWContext *) data, url );
  gtk_widget_destroy( dialog );
}

static void OpenPageDialog_composer_callback( GtkWidget *widget, gpointer data )
{
  XP_ASSERT(0);
}

static void OpenPageDialog_clear_callback( GtkWidget *widget, gpointer data )
{
  gtk_entry_set_text( GTK_ENTRY( data ), "" );
}

static void OpenPageDialog_cancel_callback( GtkWidget *widget, gpointer data )
{
  gtk_widget_destroy( GTK_WIDGET( data ) );
}

void
gtkfe_OpenPageDialog(MWContext *context,
                     char *defaulttext)
{
  GtkWidget *dialog;
  GtkWidget *label;
  GtkWidget *hbox;
  GtkWidget *vbox;
  GtkWidget *entry;
  GtkWidget *button;

  dialog = gtk_dialog_new();

  vbox = gtk_vbox_new( FALSE, 0 );
  gtk_container_border_width( GTK_CONTAINER( vbox ), 10 );
  gtk_box_pack_start( GTK_BOX( GTK_DIALOG( dialog )->vbox ), vbox, TRUE, TRUE, 0 );

  label = gtk_label_new( "Enter the World Wide Web location (URL) or specify" );
  gtk_label_set_justify( GTK_LABEL( label ), GTK_JUSTIFY_LEFT );
  gtk_box_pack_start( GTK_BOX( vbox ), label, FALSE, FALSE, 0 );
  label = gtk_label_new( "the local file you would like to open:" );
  gtk_label_set_justify( GTK_LABEL( label ), GTK_JUSTIFY_LEFT );
  gtk_box_pack_start( GTK_BOX( vbox ), label, FALSE, FALSE, 0 );

  hbox = gtk_hbox_new( FALSE, 10 );
  gtk_box_pack_start( GTK_BOX( vbox ), hbox, FALSE, FALSE, 0 );

  entry = gtk_entry_new();
  gtk_entry_set_text( GTK_ENTRY( entry ), defaulttext );
  gtk_box_pack_start( GTK_BOX( hbox ), entry, TRUE, TRUE, 0 );
  gtk_object_set_data( GTK_OBJECT( dialog ), "entry", entry );

  button = gtk_button_new_with_label( "Choose File..." );
  gtk_signal_connect( GTK_OBJECT( button ), "clicked", 
                      GTK_SIGNAL_FUNC( OpenPageDialog_choose_callback ), context );
  gtk_box_pack_start( GTK_BOX( hbox ), button, FALSE, FALSE, 0 );

  button = gtk_button_new_with_label( "Open in Navigator" );
  gtk_signal_connect( GTK_OBJECT( button ), "clicked", 
                      GTK_SIGNAL_FUNC( OpenPageDialog_navigator_callback ), context );
  gtk_box_pack_start( GTK_BOX( GTK_DIALOG( dialog )->action_area ), button, FALSE, TRUE, 0 );
  GTK_WIDGET_SET_FLAGS( GTK_WIDGET( button ), GTK_CAN_DEFAULT );
  gtk_window_set_default( GTK_WINDOW( dialog ), button );

  button = gtk_button_new_with_label( "Open in Composer" );
  gtk_signal_connect( GTK_OBJECT( button ), "clicked", 
                      GTK_SIGNAL_FUNC( OpenPageDialog_composer_callback ), context );
  gtk_box_pack_start( GTK_BOX( GTK_DIALOG( dialog )->action_area ), button, FALSE, TRUE, 0 );
  GTK_WIDGET_SET_FLAGS( GTK_WIDGET( button ), GTK_CAN_DEFAULT );

  button = gtk_button_new_with_label( "Clear" );
  gtk_signal_connect( GTK_OBJECT( button ), "clicked", 
                      GTK_SIGNAL_FUNC( OpenPageDialog_clear_callback ), entry );
  gtk_box_pack_start( GTK_BOX( GTK_DIALOG( dialog )->action_area ), button, FALSE, TRUE, 0 );
  GTK_WIDGET_SET_FLAGS( GTK_WIDGET( button ), GTK_CAN_DEFAULT );

  button = gtk_button_new_with_label( "Cancel" );
  gtk_signal_connect( GTK_OBJECT( button ), "clicked", 
                      GTK_SIGNAL_FUNC( OpenPageDialog_cancel_callback ), dialog );
  gtk_box_pack_start( GTK_BOX( GTK_DIALOG( dialog )->action_area ), button, FALSE, TRUE, 0 );
  GTK_WIDGET_SET_FLAGS( GTK_WIDGET( button ), GTK_CAN_DEFAULT );

  gtk_widget_show_all( dialog );
}
