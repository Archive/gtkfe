/*
 * The contents of this file are subject to the Netscape Public License
 * Version 1.0 (the "NPL"); you may not use this file except in
 * compliance with the NPL.  You may obtain a copy of the NPL at
 * http://www.mozilla.org/NPL/
 *
 * Software distributed under the NPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the NPL
 * for the specific language governing rights and limitations under the
 * NPL.
 *
 * The Initial Developer of this code under the NPL is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation.
 *
 * Modified:
 *    1998/04/07    Vidar Hokstad
 *        Created. Inspired a lot by QtBrowserContext.cpp from the qtfe port,
 *        and from XFE, but mostly written from scratch.
 *
 *    1998/04/09    Vidar Hokstad
 *        Added scroller window and space for a status bar...
 * 
 *    1998/04/09    Mike Shaver <shaver@mozilla.org>
 *        Prepared for non-Gnome compile [VH]
 *
 *    1998/04/09    Vidar Hokstad
 *        Added some more plain GTK support.
 *
 *    1998/04/10    Tero Pulkkinen [TP]
 *        Added toolbar code, modified browser window widget packings alittle
 *
 *    1998/04/10    Tero Pulkkinen
 *        Added locationbar. Added pixmaps for buttons in toolbar. 
 *        Remember to add Tuomas Kuosmanen a.k.a. tigert to credits for 
 *        the cool icons. (from http://tigert.gimp.org/gnome/gnome-stock/)
 *    
 *    1998/04/10    Larry Ewing [LE]
 *        initialized some of the context data for toolbar and misc.
 *
 *    1994/04/11    Vidar Hokstad
 *        Gnome'ified the toolbar stuff (and broke some of Larry Ewings
 *	   code in the process). Both GTK and Gnome version now uses
 *         a handlebox for the Location: bar.
 */

#ifdef HAVE_GNOME
#include <gnome.h>

extern GnomeUIInfo mozilla_menu;
extern GnomeUIInfo mozilla_toolbar;
#endif


#include "gtkfe.h"
#include "layout.h"
#include "layers.h"
#include <glib.h>
#include <gdk/gdkx.h>
#include <gtk/gtk.h>
#include "gtklayout.h"
#ifdef DO_CHECKMEM
#include "checkmem.h"
#else
#define DMC()
#endif

/* Callback declarations */
CL_Compositor * gtkfe_CreateCompositor(MWContext *context);
void gtkfe_CreateToolbar(MWContext *context, fe_ContextData *cd);
void gtkfe_CreateMenus(MWContext *context, fe_ContextData *cd);

/* other forward declarations */
static void CreateLocationbar(MWContext *context, fe_ContextData *cd);
static void SetDrawableOrigin(CL_Drawable *drawable,
			      int32 x_origin,
			      int32 y_origin);
static void SetDrawableClip(CL_Drawable *drawable, FE_Region clip_region);
static void RestoreDrawableClip(CL_Drawable *drawable);
static void CopyPixels(CL_Drawable *drawable_src, 
		       CL_Drawable *drawable_dst, 
		       FE_Region region);
static PRBool LockDrawable(CL_Drawable *drawable, CL_DrawableState new_state);
static void RelinquishDrawable(CL_Drawable *drawable);
static void SetDrawableDimensions(CL_Drawable *drawable, uint32 width, uint32 height);
static void ExposeHandler(GtkWidget *widget, GdkEventExpose *event);
extern ContextFuncs gtkfe_context_funcs;

static
CL_DrawableVTable window_drawable_vtable = {
    NULL,
    NULL,
    NULL,
    NULL,
    SetDrawableOrigin,
    NULL,
    SetDrawableClip,
    RestoreDrawableClip,
    CopyPixels,
    NULL
};


static
CL_DrawableVTable backing_store_drawable_vtable = {
    LockDrawable,
    NULL,
    RelinquishDrawable,
    NULL,
    SetDrawableOrigin,
    NULL,
    SetDrawableClip,
    RestoreDrawableClip,
    CopyPixels,
    SetDrawableDimensions
};

/* There is only one backing store pixmap shared among all windows */
static GdkPixmap *fe_backing_store_pixmap = NULL;

/* We use a serial number to compare pixmaps rather than the X Pixmap
   handle itself, in case the server allocates a pixmap with the same
   handle as a pixmap that we've deallocated.  */
static int fe_backing_store_pixmap_serial_num = 0;

/* Current lock owner for backing store */
static fe_Drawable *backing_store_owner = NULL;
static int backing_store_width = 0;
static int backing_store_height = 0;
static int backing_store_refcount = 0;
static int backing_store_depth;


/***** FIXME: VIDARS HUGE UGLY HACK [VH] ****************/
static void
SetDrawableOrigin(CL_Drawable *drawable,
		  int32 x_origin,
		  int32 y_origin)
{
  fe_Drawable *fe_drawable = CL_GetDrawableClientData(drawable);
  fe_drawable->x_origin = x_origin;
  fe_drawable->y_origin = x_origin;
}

static void
SetDrawableClip(CL_Drawable *drawable, FE_Region clip_region)
{
  fe_Drawable *fe_drawable = CL_GetDrawableClientData(drawable);
  fe_drawable->clip_region = clip_region;
}

static void
RestoreDrawableClip(CL_Drawable *drawable) {
  fe_Drawable *fe_drawable = CL_GetDrawableClientData(drawable);
  fe_drawable->clip_region = NULL;
}

static void
CopyPixels(CL_Drawable *drawable_src, 
	   CL_Drawable *drawable_dst, 
	   FE_Region region)
{
  XP_Rect bbox;

  GdkGC *gc;
  GdkDrawable *src, *dst;
  GdkGCValues gcv;

  fe_Drawable *fe_drawable_dst = 
    (fe_Drawable*)CL_GetDrawableClientData(drawable_dst);
  fe_Drawable *fe_drawable_src = 
    (fe_Drawable*)CL_GetDrawableClientData(drawable_src);

  dst = fe_drawable_dst->gdkdrawable;
  src = fe_drawable_src->gdkdrawable;

  FE_GetRegionBoundingBox(region, &bbox);

  gcv.function = GDK_COPY;
  gc = gdk_gc_new_with_values(fe_drawable_dst->gdkdrawable, &gcv,
			      GDK_GC_FUNCTION);
  if(fe_drawable_dst->clip_region)
    {
      gdk_gc_set_clip_origin(gc,
			     fe_drawable_dst->x_origin,
			     fe_drawable_dst->y_origin);
      gdk_gc_set_clip_region(gc, GDK_REGION(fe_drawable_dst->clip_region));
    }
  g_warning("copying area from %p to %p (%d, %d) - (%d, %d)\n",
	    fe_drawable_src, fe_drawable_dst,
	    bbox.left, bbox.top,
	    bbox.right - bbox.left,
	    bbox.bottom - bbox.top);
  gdk_window_copy_area(dst, gc, bbox.left, bbox.top,
		       src, bbox.left, bbox.top,
		       bbox.right - bbox.left,
		       bbox.bottom - bbox.top);
  gdk_gc_destroy(gc);
}

static PRBool
LockDrawable(CL_Drawable *drawable, CL_DrawableState new_state)
{
  fe_Drawable *prior_backing_store_owner;
  fe_Drawable *fe_drawable = (fe_Drawable *)CL_GetDrawableClientData(drawable);

  if (new_state == CL_UNLOCK_DRAWABLE)
    return PR_TRUE;
    
  XP_ASSERT(backing_store_refcount > 0);

  if (!fe_backing_store_pixmap)
      return PR_FALSE;

  gdk_pixmap_ref(fe_backing_store_pixmap);

  prior_backing_store_owner = backing_store_owner;

  /* Check to see if we're the last one to use this drawable.
     If not, someone else might have modified the bits, since the
     last time we wrote to them using this drawable. */
  if (new_state & CL_LOCK_DRAWABLE_FOR_READ) {
    if (prior_backing_store_owner != fe_drawable)
        return PR_FALSE;

    /* The pixmap could have changed since the last time this
       drawable was used due to a resize of the backing store, even
       though no one else has drawn to it.  */
    if (fe_drawable->gdk_drawable_serial_num !=
        fe_backing_store_pixmap_serial_num) {
        return PR_FALSE;
    }
  }

  backing_store_owner = fe_drawable;

  fe_drawable->gdkdrawable = fe_backing_store_pixmap;
  fe_drawable->gdk_drawable_serial_num = fe_backing_store_pixmap_serial_num;

  return PR_TRUE;
}

static void
RelinquishDrawable(CL_Drawable *drawable) {
  if(((GdkWindowPrivate *)fe_backing_store_pixmap)->ref_count == 1)
    {
      gdk_pixmap_unref(fe_backing_store_pixmap);      
      backing_store_owner = NULL;
      fe_backing_store_pixmap = NULL;
      backing_store_width = 0;
      backing_store_height = 0;
    }
  else
    gdk_pixmap_unref(fe_backing_store_pixmap);
}

static void
SetDrawableDimensions(CL_Drawable *drawable, uint32 width, uint32 height) {
  fe_Drawable *fe_drawable = CL_GetDrawableClientData(drawable);

  g_print("Set drawable dimensions to %dx%d\n",
	  width, height);
  if((width > backing_store_width) || (height > backing_store_height))
    {
      if(fe_backing_store_pixmap)
	{
	  while(((GdkWindowPrivate *)fe_backing_store_pixmap)->ref_count)
	    gdk_pixmap_unref(fe_backing_store_pixmap);
	}

      fe_backing_store_pixmap_serial_num++;
      fe_backing_store_pixmap = gdk_pixmap_new(GDK_ROOT_PARENT(),
					       width,
					       height,
					       backing_store_depth);
      gdk_pixmap_ref(fe_backing_store_pixmap);
      backing_store_width = width;
      backing_store_height = height;
    }
  fe_drawable->gdkdrawable = fe_backing_store_pixmap;
}

/********* END OF VIDARS HUGE UGLY HACK ********************/



/* This is for making minimum size of browser window to 50,50 instead
   of the default side -- will be called by gtk_idle_add [TP] */
static gint
SetMinUsize(void *data) {
   gtk_widget_set_usize(GTK_WIDGET(data), 50, 50);
   return FALSE;
}

static void
UpdateRect(MWContext *context, int x, int y, int w, int h) {
  XP_Rect rect;

  XP_ASSERT(context->compositor);
  
  fprintf(stderr, "Update RECT %d %d %d %d \n", x, y, w, h);
  CL_CompositeNow(context->compositor);
  rect.left = x + GTK_LAYOUT(CONTEXT_DATA(context)->layout)->xoffset;
  rect.top = y + GTK_LAYOUT(CONTEXT_DATA(context)->layout)->yoffset;
  rect.right = rect.left + w;
  rect.bottom = rect.top+h;
  CL_UpdateDocumentRect((context)->compositor, &rect, PR_TRUE);
}

static void
ExposeHandler(GtkWidget *widget, GdkEventExpose *event)
{
    MWContext *context = gtkfe_GetContext(widget);

    #define BORDER_WIDTH 0

    gdk_window_clear_area(GTK_LAYOUT(widget)->bin_window,
			  event->area.x - BORDER_WIDTH,
			  event->area.y - BORDER_WIDTH,
			  event->area.width + BORDER_WIDTH,
			  event->area.height + BORDER_WIDTH);

    UpdateRect(context, event->area.x - GTK_LAYOUT(widget)->scroll_x,
	       event->area.y - GTK_LAYOUT(widget)->scroll_y,
	       event->area.width, event->area.height);
    gdk_flush();
}

MWContext *lastctx;

static fe_ContextData *
NewBrowserContext(MWContext *context,
		  Chrome *decor,
		  GtkWidget *parent,
		  const char *name)
{
   /* FIXME -- Need to decide on memory allocation methods... */
   fe_ContextData *cd;

   DMC();

   cd = GTKFE_ALLOC(sizeof(struct fe_ContextData));

   cd->last_status_ctx = -1;

   DMC();

   lastctx = context;
   
   if (!cd) {
      /* FIXME -- We should use XP_GetString() for this [VH] */
      FE_Alert(context,"Out of memory when creating new browser context");
      return 0;
   }

   context->fe.data = cd;
   
   /* 
    * FIXME initialize some vars, if these are set elsewhere fine, then
    * remove them :)
    */
   cd->show_toolbar_p = 1;

   /* The stuff below is stolen from gnome_hello of all places :-) But
    * is it the right one to use when we'll open multiple windows?
    */

#ifdef HAVE_GNOME
   cd->main = gnome_app_new("mozilla", (char *)name);
#else
   cd->main = gtk_window_new (GTK_WINDOW_TOPLEVEL);
   gtk_window_set_title(GTK_WINDOW(cd->main), (char *)name);
#endif
   gtk_object_set_data(GTK_OBJECT(cd->main), "gtkfe-parent", parent);
   gtk_object_set_data(GTK_OBJECT(cd->main), "MWContext", context);

   if (!cd->main) {
      fprintf(stderr,"Unable to instantiate Mozilla/GNOME application window. Exiting\n");
      exit(1);
   }
   
   gtk_widget_realize(cd->main); /* We DO need this...
				    pixmap routines use it [TP] */

   /* This should go to a "close" callback, to close the current window, not all of Mozilla. [VH] */
   cd->delete_event_close_id =
     gtk_signal_connect (GTK_OBJECT (cd->main), "delete_event",
			 GTK_SIGNAL_FUNC (gtkfe_QuitCallback),
			 cd);
   cd->close_callback_id = -1;

   gtkfe_CreateMenus (context, cd);
   cd->bookmark_menu = gtk_menu_new();

   /* Table for the contents */

   cd->contenttable = gtk_table_new(4,7, FALSE);
   XP_ASSERT(cd->contenttable);
   
   /* Toolbars */
   
   gtkfe_CreateToolbar (context, cd);
   CreateLocationbar (context, cd);

   cd->separator=gtk_hseparator_new();
   gtk_table_attach (GTK_TABLE(cd->contenttable), cd->separator, 
		     0, 4, 4, 5, /* left, right, top, bottom attach */
		     GTK_FILL,GTK_FILL, /* x/y options */
		     0,0 /* padding */
		     );
   gtk_widget_show(cd->separator);
   
#ifdef HAVE_GNOME
   gnome_app_set_contents(GNOME_APP(cd->main),cd->contenttable);
#else
   gtk_container_add (GTK_CONTAINER (cd->main), cd->contenttable);
#endif
   
   /* Scrollwindow */

   cd->scrolltable = gtk_table_new (2, 2, FALSE);
   gtk_table_attach_defaults (GTK_TABLE(cd->contenttable), cd->scrolltable, 
			      0, 4, 5, 6);
   gtk_widget_show (cd->scrolltable);
  
   cd->layout = gtk_layout_new (NULL, NULL);
   gtk_object_set_data(GTK_OBJECT(cd->layout), "MWContext", context);
   g_print("Created a new layout %p for cd %p\n",
	   cd->layout, cd);
   gtk_table_attach (GTK_TABLE (cd->scrolltable), cd->layout,
		     0, 1, 0, 1, 
		     GTK_FILL | GTK_EXPAND,
		     GTK_FILL | GTK_EXPAND,
		     0, 0);

   gtk_widget_set_events (cd->layout, GDK_EXPOSURE_MASK);
   gtk_signal_connect (GTK_OBJECT (cd->layout), "expose_event",
		       GTK_SIGNAL_FUNC (ExposeHandler), NULL);
   gtk_widget_show (cd->layout);
   
   cd->hscroll = gtk_hscrollbar_new (gtk_layout_get_hadjustment (GTK_LAYOUT (cd->layout)));
   gtk_table_attach (GTK_TABLE (cd->scrolltable), cd->hscroll,
		     0, 1, 1, 2, 
		     GTK_FILL | GTK_EXPAND,
		     0,
		     0, 0);
   gtk_widget_show (cd->hscroll);
   
   cd->vscroll = gtk_vscrollbar_new (gtk_layout_get_vadjustment (GTK_LAYOUT (cd->layout)));
   gtk_table_attach (GTK_TABLE (cd->scrolltable), cd->vscroll,
		     1, 2, 0, 1, 
		     0,
		     GTK_FILL | GTK_EXPAND,
		     0, 0);
   gtk_widget_show (cd->vscroll);
   
   /* if you change cd->layout to something here, remember to
      change it elsewhere too.. (like from idle_add's..) */
   gtk_widget_set_usize(cd->layout, 500, 500);
   
   /* Progressbar */
   
   cd->progressbar = gtk_progress_bar_new ();
   XP_ASSERT(cd->progressbar); /* FIXME: Asserting legal error conditions is a no-no [VH] */
   gtk_widget_set_usize(cd->progressbar, 100, 0);
   gtk_table_attach (GTK_TABLE(cd->contenttable), cd->progressbar, 
		     0, 2, 6, 7, /* left, right, top, bottom attach */
		     GTK_FILL,GTK_FILL, /* x/y options */
		     0,0 /* padding */
		     );
   
   /* Statusbar */

   /*
    * How well does GTK status bars work with Mozilla? Don't think the
    * Mozilla interface supports the stack based status bar concept of
    * GTK very well. [VH]
    *
    * The stack-based thing will work well for URL highlighting and
    * JS mouseOver'd window.status and window.defaultStatus things, I think.
    * [shaver]
    */
   
   cd->statusbar = gtk_statusbar_new ();
   XP_ASSERT(cd->statusbar); /* FIXME: Asserting legal error conditions is a no-no [VH] */
   gtk_table_attach(GTK_TABLE(cd->contenttable), cd->statusbar, 
		    2, 4, 6, 7,
		    GTK_FILL, GTK_FILL,
		    0,0
		    );
   gtk_widget_show(cd->statusbar);
 
   gtk_widget_show(cd->contenttable);
   gtk_widget_show(cd->scrolltable);
   gtk_widget_show(cd->progressbar);
#ifndef HAVE_GNOME
   gtk_widget_show(cd->logoimage);
#endif
   gtk_widget_show(cd->main); /* show this last! */

   /* these are for setting the minimum size of some widgets to small.. */
   gtk_idle_add(SetMinUsize, cd->main);
   gtk_idle_add(SetMinUsize, cd->layout);

   cd->thermo_timer_id = -1;

   /* Cursors */
   cd->normal_cursor = gdk_cursor_new(GDK_ARROW);
   cd->link_cursor = gdk_cursor_new(GDK_HAND1);
   cd->busy_cursor = gdk_cursor_new(GDK_WATCH);

   return cd;
}

/* getcontext from the widget */

MWContext *gtkfe_GetContext(GtkWidget *w) {
  return gtk_object_get_data(GTK_OBJECT(gtk_widget_get_toplevel(w)), "MWContext");
}

fe_ContextData *gtkfe_GetContextData(GtkWidget *w) {
  return gtkfe_GetContext(w)->fe.data;
}


MWContext *
GTKFE_MakeNewWindow(URL_Struct *url,
		    char       *window_name,
		    Chrome     *chrome,
		    MWContext  *parent)
{
   MWContext* context = GTKFE_CreateMWContext();
   GtkWidget* parentw = 0;
   fe_ContextData * cd = 0;
   
   context->type = chrome ? chrome->type
     : parent ? parent->type
     : MWContextBrowser;

   context->funcs = &gtkfe_context_funcs;

   DMC();
   
   XP_AddContextToList (context);

   DMC();

   g_print("XP_AddContextToList done\n");

   DMC();

   switch (context->type) {
    case MWContextBrowser:
      cd = NewBrowserContext(context, chrome, parentw, window_name);
      break;
    default:
      XP_ASSERT(0);
      g_warning("gtkfe doesn't currently support any other contexts than MWContextBrowser\n");
   }
   context->fe.data = cd;

   g_print("creating compositor done\n");
   
   context->compositor = gtkfe_CreateCompositor(context);

   g_print("starting geturl:\n");
   
   if (url)
     FE_GetURL(context, url);
   g_print("done\n");

   return context;
}

static void
layout_pos_changed(GtkAdjustment *adjustment,
		   CL_Compositor *user_data)
{
  XP_Rect rect;
  GtkLayout *layout = gtk_object_get_data(GTK_OBJECT(adjustment), "GtkLayout");
  GtkWidget *layoutw = GTK_WIDGET(layout);
  g_print("Scrolling to (%d, %d)\n",
	  layout->xoffset, layout->yoffset);
  CL_ScrollCompositorWindow(user_data, layout->xoffset, layout->yoffset);
  CL_CompositeNow(user_data);
  rect.left = 0;
  rect.top = 0;
  rect.right = layoutw->allocation.width;
  rect.bottom = layoutw->allocation.height;
  CL_RefreshWindowRect(user_data, &rect);
}

static void
layout_size_changed(GtkWidget *widget,
		    GtkAllocation *allocation,
		    CL_Compositor *user_data)
{
  XP_Rect rect;

  CL_ResizeCompositorWindow(user_data, allocation->width, allocation->height);
  LO_RelayoutOnResize(gtkfe_GetContext(widget),
		      allocation->width, allocation->height, 4, 4);
}

CL_Compositor *
gtkfe_CreateCompositor(MWContext *context)
{
  int32 comp_width, comp_height;
  CL_Drawable *cl_window_drawable, *cl_backing_store_drawable;
  CL_Compositor *compositor;
  fe_Drawable *window_drawable, *backing_store_drawable;
  GdkVisual *v;

  GdkWindow *window;

  XP_ASSERT(CONTEXT_DATA(context)->layout);
  
  window = GTK_LAYOUT(CONTEXT_DATA(context)->layout)->bin_window;
  v = gdk_imlib_get_visual();
  backing_store_depth = v->depth;
  g_warning("Backing store depth is %d\n", backing_store_depth);
   
  /* Create a new compositor and its default layers */
  comp_width = MAX(CONTEXT_DATA(context)->layout->allocation.width, 500);
  comp_height = MAX(CONTEXT_DATA(context)->layout->allocation.height, 500);

  window_drawable = XP_NEW_ZAP(fe_Drawable);
  if (!window_drawable)
    return NULL;

  window_drawable->gdkdrawable = window;
  
  /* Create backing store drawable, but don't create pixmap
     until SetDrawableDimensions() is called from the
     compositor */
  backing_store_drawable = XP_NEW_ZAP(fe_Drawable);
  if (!backing_store_drawable)
    return NULL;

  if(window_drawable && backing_store_drawable)
    g_warning("new window has window_drawable->gdkdrawable = %p, backing_store_drawable->gdkdrawable = %p\n",
	      window_drawable->gdkdrawable, backing_store_drawable->gdkdrawable);
  
  /* Create XP handle to window's HTML view for compositor */
  cl_window_drawable = CL_NewDrawable(comp_width, comp_height, 
				      CL_WINDOW, &window_drawable_vtable,
				      (void*)window_drawable);

  /* Create XP handle to backing store for compositor */
  cl_backing_store_drawable = CL_NewDrawable(comp_width, comp_height, 
					     CL_BACKING_STORE,
					     &backing_store_drawable_vtable,
					     (void*)backing_store_drawable);
  
  compositor = CL_NewCompositor(cl_window_drawable, cl_backing_store_drawable,
				0, 0, comp_width, comp_height, 30);
  
  /* Set reasonable default drawable */
  CONTEXT_DATA (context)->drawable = window_drawable;

  gtk_signal_connect(GTK_OBJECT(CONTEXT_DATA (context)->layout),
		     "size_allocate",
		     GTK_SIGNAL_FUNC(layout_size_changed),
		     compositor);
  gtk_signal_connect(GTK_OBJECT(GTK_LAYOUT(CONTEXT_DATA(context)->layout)->hadjustment),
		     "value_changed",
		     GTK_SIGNAL_FUNC(layout_pos_changed),
		     compositor);
  gtk_signal_connect(GTK_OBJECT(GTK_LAYOUT(CONTEXT_DATA(context)->layout)->vadjustment),
		     "value_changed",
		     GTK_SIGNAL_FUNC(layout_pos_changed),
		     compositor);
  return compositor;
}

static void
LocationEntryChanged(GtkWidget *entry, gpointer *data) {
    MWContext *context = (MWContext *)data;
    URL_Struct *url;
    gchar *url_str = gtk_entry_get_text(GTK_ENTRY(entry));
    url = NET_CreateURLStruct(url_str, NET_DONT_RELOAD);
    FE_GetURL(context, url);
}

static void
CreateLocationbar(MWContext *context, fe_ContextData *cd) {

   GtkWidget *hbox, *label, *entry;

   cd->locationhbox = gtk_handle_box_new();
 
   gtk_table_attach (GTK_TABLE(cd->contenttable), cd->locationhbox,           
		     0, 3, 2, 3, /* left, right, top, bottom attach */
		     GTK_FILL,GTK_FILL, /* x/y options */
		     0,3 /* padding */
		     );

   gtk_widget_show(cd->locationhbox);
   
   hbox = gtk_hbox_new(FALSE,0);
   gtk_widget_show(hbox);
   
   label = gtk_label_new(" Location: ");
   gtk_widget_show(label);
   
#ifdef HAVE_GNOME
   entry = gnome_entry_new(NULL);
#else
   entry = NULL;
#endif   
   cd->locationentry = entry;
   gtk_widget_show(entry);
   
#if HAVE_GNOME
   gtk_signal_connect(GTK_OBJECT(GTK_COMBO(entry)->entry),
		      "activate", (GtkSignalFunc)LocationEntryChanged,
		      context);
#endif
   
   gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
   gtk_box_pack_start(GTK_BOX(hbox), entry, -1, -1, 0);
   gtk_container_add(GTK_CONTAINER(cd->locationhbox), hbox);
}







